package Base;


public class General extends Exception {

    private String code;
    private String errorOrigen;

    public General(String message, String code, String errorOrigen) {
        super(message);
        setCode(code);
        setErrorOrigen(errorOrigen);
    }

    public General(String message, String code, String errorOrigen, Throwable cause) {
        super(message, cause);
        setCode(code);
        setErrorOrigen(errorOrigen);
    }

    @Override
    public String toString() {
        return getMessage() + " - " + getCode() + " - " + getErrorOrigen();
    }

    /**
     * @return the errorOrigen
     */
    public String getErrorOrigen() {
        return errorOrigen;
    }

    /**
     * @param errorOrigen the errorOrigen to set
     */
    public void setErrorOrigen(String errorOrigen) {
        this.errorOrigen = errorOrigen;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }
}
