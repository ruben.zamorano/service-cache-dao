package Base;

public class GlobalEnum {
    public enum enumPlazos {
        PLAZOS_36 ("4"),
        PLAZOS_30 ("3"),
        PLAZOS_24 ("2"),
        PLAZOS_18 ("1"),
        PLAZOS_12 ("0");

        private final String plazos;

        enumPlazos (String tipo) {
            this.plazos = tipo;
        }
        public String getPlazo() { return plazos; }
    }
}
