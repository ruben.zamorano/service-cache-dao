package Base;


//import com.coppel.config.DataBaseConfig;
//import com.coppel.config.DataBaseEnum;
//import com.coppel.ecommerce.calculopagoinicial.dto.Clases;
//import com.coppel.ecommerce.calculopagoinicial.dto.PagoInicialConfig;
//import com.coppel.ecommerce.configuracion.LecturaConexionBaseXML;
//import com.coppel.ecommerce.txmanager.TxManager;

import Config.DataBaseConfig;
import Config.DataBaseEnum;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.util.ArrayList;
import java.util.List;

public class SingletonRS {

    static private SingletonRS _instance = null;
    static public String WSSituacionesEspeciales = null;
    static public String WSCCuentaPort2 = null;
    static public String wsConsultaPlazosMuebles = null;
    static public String WsValidarReglasDeCredito = null;
    static public String WsCalcularPagoInicialTienda = null;
    static public String WsCalcularPagoInicialTiendaVentaContado = null;
    static public String ApiConsultarBuroCredito = null;
    public String apiConsultartasaInteres = null;
    static public PagoInicialConfig piConfig = null;

    private LecturaConexionBaseXML conexionBaseXML = new LecturaConexionBaseXML();

    protected SingletonRS() throws Exception {

        try {
            List<DataBaseConfig> configs = new ArrayList<DataBaseConfig>();
            configs.add(conexionBaseXML.GetConexionByName(DataBaseEnum.TIENDA_VIRTUAL.name()));
            configs.add(conexionBaseXML.GetConexionByName(DataBaseEnum.TIENDA_800.name()));
            configs.add(conexionBaseXML.GetConexionByName(DataBaseEnum.CARTERA.name()));

            TxManager.init(configs);

            WSSituacionesEspeciales = this.conexionBaseXML.GetWsUrlConexion("SituacionesEspeciales");
            WsCalcularPagoInicialTiendaVentaContado = this.conexionBaseXML.GetWsUrlConexion("WsCalcularPagoInicialTiendaValidarVentaContado");
            ApiConsultarBuroCredito = this.conexionBaseXML.GetWsUrlConexion("apiConsultarBuroCredito");


            WSCCuentaPort2 = this.conexionBaseXML.GetWsUrlConexion("WSCCuentaPort2");
            wsConsultaPlazosMuebles = this.conexionBaseXML.GetWsUrlConexion("wsConsultaPlazosMuebles");
            WsValidarReglasDeCredito = this.conexionBaseXML.GetWsUrlConexion("WsValidarReglasDeCredito");
            WsCalcularPagoInicialTienda = this.conexionBaseXML
                    .GetWsUrlConexion("WsCalcularPagoInicialTienda");
            apiConsultartasaInteres = this.conexionBaseXML.GetWsUrlConexion("apiConsultartasainteres");

            Node t = conexionBaseXML.GetClass("CALCULOPAGOINICIAL");
            JAXBContext jc = JAXBContext.newInstance(Clases.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();

            Clases clases = (Clases) unmarshaller.unmarshal(t);
            piConfig = clases.getPagoInicialConfig();

        } catch (Exception e) {
            throw e;
        }
    }

    static public SingletonRS instance() throws Exception {
        if (_instance == null) {
            _instance = new SingletonRS();
        }
        return _instance;
    }
}
