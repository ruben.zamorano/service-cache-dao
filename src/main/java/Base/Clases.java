package Base;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Clases {


    PagoInicialConfig piConfig;

    @XmlElement (name = "piConfig")
    public PagoInicialConfig getPagoInicialConfig() {
        return piConfig;
    }

    public void setPagoInicialConfig(PagoInicialConfig piConfig) {
        this.piConfig = piConfig;
    }

}
