package Base;


import Config.DataBaseConfig;
import Config.DataBaseEnum;
import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;


public class TxManager {
    private static final Logger LOG = Logger.getLogger(TxManager.class);

    private static ThreadLocal<Map<DataBaseEnum, Connection>> connectionHolder = new ThreadLocal<Map<DataBaseEnum, Connection>>();
    private static Map<DataBaseEnum, DataSource> dataSources;

    public static void init(List<DataBaseConfig> configs) throws NamingException, SQLException {

//        InitialContext cxt = new InitialContext();

        Properties env = new Properties( );
        env.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
        env.put("java.naming.provider.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
        env.put("java.naming.provider.url", "jnp://myhost:1099");
//        env.put(Context.PROVIDER_URL, "t3://localhost:7001");

//        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.rmi.registry.RegistryContextFactory");
//        env.put(Context.PROVIDER_URL, "rmi://server:1099");

        Hashtable env2 = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://localhost:389/o=jnditutorial");
        env.put("java.naming.factory.initial", "org.apache.camel.util.jndi.CamelInitialContextFactory");
        InitialContext cxt = new InitialContext(env2);



//        SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
//        builder.bind("jdbc/Oracle", ods);
//        builder.activate();


        dataSources = new HashMap<DataBaseEnum, DataSource>();
        DataSource datasource;
        for (DataBaseConfig databaseConfig : configs) {
            LOG.debug("Conectando a " + databaseConfig.getDatabase() + " : " + databaseConfig.getJndi());
            System.out.println("Conectando a " + databaseConfig.getDatabase() + " : " + databaseConfig.getJndi());

            datasource = (DataSource) cxt.lookup(databaseConfig.getJndi());
            LOG.debug("Conexi�n exitosa a " + databaseConfig.getDatabase() + " : " + databaseConfig.getJndi());
            dataSources.put(databaseConfig.getDatabase(), datasource);
        }
    }

    public static Connection beginTx(DataBaseEnum db) {
        initConnectionHolder();

        Connection conn = null;
        try {
            conn = dataSources.get(db).getConnection();
            conn.setAutoCommit(false);
            conn.setReadOnly(false);

            Map<DataBaseEnum, Connection> map = connectionHolder.get();
            map.put(db, conn);
        } catch (Exception ex) {
            LOG.error("Couldn't get a connection", ex);
            throw new RuntimeException(ex);
        }
        return conn;
    }

    public static Connection beginQuery(DataBaseEnum db) {
        initConnectionHolder();

        Connection conn = beginTx(db);
        try {
            conn.setReadOnly(true);
        } catch (Exception ex) {
            LOG.error("Couldn't set connection to read-only mode.", ex);
            throw new RuntimeException(ex);
        }
        return conn;
    }

    public static Connection getConnection(DataBaseEnum db) {
        Connection conn = connectionHolder.get().get(db);
        if (conn == null) {
            String errorMsg = "Couldn't get hold of a connection for this thread";
            LOG.error(errorMsg);
            throw new RuntimeException(errorMsg);
        }
        return conn;
    }

    public static void commit(DataBaseEnum db) {
        try {
            Connection conn = getConnection(db);
            conn.commit();
            safeClose(conn);
        } catch (Exception ex) {
            LOG.error("Couldn't commit the transaction", ex);
            throw new RuntimeException(ex);
        } finally {
            connectionHolder.get().put(db, null);
        }
    }

    public static void rollback(DataBaseEnum db) {
        try {
            Connection conn = getConnection(db);
            conn.rollback();
            safeClose(conn);
        } catch (Exception ex) {
            LOG.error("Couldn't rollback the transaction", ex);
            throw new RuntimeException(ex);
        } finally {
            connectionHolder.get().put(db, null);
        }
    }

    public static void closeQueryConnection(DataBaseEnum db) {
        try {
            Connection conn = getConnection(db);
            safeClose(conn);
        } catch (Exception ex) {
            LOG.error("Couldn't rollback the transaction", ex);
            throw new RuntimeException(ex);
        } finally {
            connectionHolder.get().put(db, null);
        }
    }

    private static void safeClose(Connection conn) {
        try {
            conn.close();
        } catch (Exception ex) {
            LOG.error("Unable to close connection", ex);
        }
    }

    private static void initConnectionHolder() {
        if (connectionHolder.get() == null) {
            connectionHolder.set(new HashMap<DataBaseEnum, Connection>());
        }
    }
}
