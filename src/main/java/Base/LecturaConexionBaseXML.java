package Base;


import Config.DataBaseConfig;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;


public class LecturaConexionBaseXML {

    private static final Logger LOG = Logger.getLogger(LecturaConexionBaseXML.class);

    private static String PATH_APACHETOMCAT = System.getProperty("catalina.base");

    /**
     *
     * Obtiene la conexion a la que se desee de acuerdo al nombre de BD enviado
     *
     * @param nameBD
//     * @param conexion
     * @throws Exception
     */
    public DataBaseConfig GetConexionByName(String nameBD) throws Exception {
        DataBaseConfig dbconfig = null;
        String jdbc = "";
        String pathTomcatOld = PATH_APACHETOMCAT + File.separator + "conf" + File.separator
                + "XML_BASE.xml";

        String pathTomcat = "/Users/danielabojorquez/ruben/BuenFin2021/refactordao/src/main/java/Base/XML_BASE.xml";
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        File file = new File(pathTomcat);
        boolean existe = false;
        try {
            if (file.exists()) {
                LOG.debug("file.exists(): " + file.exists());
                Document doc = db.parse(file);
                Element docEle = doc.getDocumentElement();
                NodeList conexionList = docEle.getElementsByTagName("conexion");

                if (conexionList != null) {
                    for (int i = 0; i < conexionList.getLength(); i++) {
                        String name = conexionList.item(i).getAttributes().getNamedItem("name").getNodeValue();
                        LOG.debug("name: " + name);
                        Node node = conexionList.item(i);
                        if (name.equals(nameBD)) {
                            if (node.getNodeType() == Node.ELEMENT_NODE) {
                                Element e = (Element) node;
                                NodeList nodeList = null;

                                nodeList = e.getElementsByTagName("jdbc");
                                jdbc = nodeList.item(0).getChildNodes().item(0).getNodeValue();
                                LOG.debug("jdbc: " + jdbc);

                                dbconfig = new DataBaseConfig(nameBD, jdbc);
                                existe = true;
                                break;
                            }
                        }
                    }

                    if (!existe)
                        throw new General("Error no existe una conexion para " + nameBD, "-3",
                                "Error al leer archivo de configuracion");
                } else
                    throw new General("Error no existen conexiones", "-3",
                            "Error al leer archivo de configuracion");

            }
        } catch (Exception e) {
            throw new General("Error al obtener la conexion a la base de datos", "-3",
                    "GetConexionByName", e);
        }

        return dbconfig;
    }

    /**
     * Obtiene la url de conexion del WS deseado
     *
     * @param nameBD
     * @return String
     * @throws Exception
     */
    public String GetWsUrlConexion(String nameBD) throws General, Exception {
        String url = "";
        String pathTomcat = PATH_APACHETOMCAT + File.separator + "conf" + File.separator
                + "XML_BASE.xml";
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        File file = new File(pathTomcat);
        boolean existe = false;
        try {
            if (file.exists()) {
                Document doc = db.parse(file);
                Element docEle = doc.getDocumentElement();
                NodeList conexionList = docEle.getElementsByTagName("websevice");

                if (conexionList != null) {
                    for (int i = 0; i < conexionList.getLength(); i++) {
                        String name = conexionList.item(i).getAttributes().getNamedItem("name").getNodeValue();
                        LOG.debug("name: " + name);
                        Node node = conexionList.item(i);
                        if (name.equals(nameBD)) {
                            if (node.getNodeType() == Node.ELEMENT_NODE) {
                                Element e = (Element) node;
                                NodeList nodeList = e.getElementsByTagName("host");
                                url = nodeList.item(0).getChildNodes().item(0).getNodeValue();
                                LOG.debug("host ws: " + url);
                                existe = true;
                                break;
                            }
                        }
                    }

                    if (!existe) {
                        throw new General("Error no existe un ws " + nameBD, "ERROR",
                                "Error al leer archivo de configuracion");
                    }
                } else {
                    throw new General("Error al leer archivo de configuracion", "ERROR",
                            "Error al leer archivo de configuracion");
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return url;
    }

    public Node GetClass(String clase) throws Exception {

        String pathTomcat = PATH_APACHETOMCAT + File.separator + "conf" + File.separator
                + "XML_BASE.xml";
        Node t = null;

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        File file = new File(pathTomcat);
        boolean existe = false;
        try {
            if (file.exists()) {
                Document doc = db.parse(file);
                Element docEle = doc.getDocumentElement();
                NodeList conexionList = docEle.getElementsByTagName("clases");

                if (conexionList != null) {
                    for (int i = 0; i < conexionList.getLength(); i++) {
                        String name = conexionList.item(i).getAttributes().getNamedItem("name").getNodeValue();
                        LOG.debug("name: " + name);
                        Node node = conexionList.item(i);
                        if (name.equals(clase)) {
                            if (node.getNodeType() == Node.ELEMENT_NODE) {
                                t = node;
                                existe = true;
                                break;
                            }
                        }
                    }

                    if (!existe) {
                        throw new General("Error no existe una clase " + clase, "ERROR",
                                "Error al leer archivo de configuracion");
                    }
                } else {
                    throw new General("Error al leer archivo de configuracion", "ERROR",
                            "Error al leer archivo de configuracion");
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return t;
    }

}