package Base;


import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "version", "versionRopa", "versionMuebles", "versionRopaLealtad","versionMueblesLealtad","buroCredito","conectionTimeoutWSTienda", "requestTimeoutWSTienda" })
public class PagoInicialConfig
{
    private String version;
    private String versionRopa;
    private String versionMuebles;
    private String versionRopaLealtad;
    private String versionMueblesLealtad;
    private String buroCredito;
    public String getBuroCredito() {
        return buroCredito;
    }

    public void setBuroCredito(String buroCredito) {
        this.buroCredito = buroCredito;
    }

    public String getVersionRopaLealtad() {
        return versionRopaLealtad;
    }

    public void setVersionRopaLealtad(String versionRopaLealtad) {
        this.versionRopaLealtad = versionRopaLealtad;
    }

    public String getVersionMueblesLealtad() {
        return versionMueblesLealtad;
    }

    public void setVersionMueblesLealtad(String versionMueblesLealtad) {
        this.versionMueblesLealtad = versionMueblesLealtad;
    }

    private int conectionTimeoutWSTienda = 5000;
    private int requestTimeoutWSTienda = 5000;

    public String getVersionRopa()
    {
        return versionRopa;
    }

    public void setVersionRopa(String versionRopa)
    {
        this.versionRopa = versionRopa;
    }

    public String getVersionMuebles()
    {
        return versionMuebles;
    }

    public void setVersionMuebles(String versionMuebles)
    {
        this.versionMuebles = versionMuebles;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public int getConectionTimeoutWSTienda()
    {
        return conectionTimeoutWSTienda;
    }

    public void setConectionTimeoutWSTienda(int conectionTimeoutWSTienda)
    {
        this.conectionTimeoutWSTienda = conectionTimeoutWSTienda;
    }

    public int getRequestTimeoutWSTienda()
    {
        return requestTimeoutWSTienda;
    }

    public void setRequestTimeoutWSTienda(int requestTimeoutWSTienda)
    {
        this.requestTimeoutWSTienda = requestTimeoutWSTienda;
    }

    @Override
    public String toString() {
        return "PagoInicialConfig [version=" + version + ", versionRopa=" + versionRopa + ", versionMuebles="
                + versionMuebles + ", versionRopaLealtad=" + versionRopaLealtad + ", versionMueblesLealtad="
                + versionMueblesLealtad + ", buroCredito=" + buroCredito + ", conectionTimeoutWSTienda="
                + conectionTimeoutWSTienda + ", requestTimeoutWSTienda=" + requestTimeoutWSTienda + "]";
    }
}
