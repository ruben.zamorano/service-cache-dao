package helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.*;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class HelperCache {

    public static final String CACHE_HOST = "http://localhost:8089/api.servicecache";
    public static final String CACHE_TTL = "30";
    public static final int CACHE_TIME_OUT_MS_GET = 500;
    public static final int CACHE_TIME_OUT_MS_POST = 500;
    public static final Gson gson = getGson();


    private HelperCache()
    {

    }

    public static String getCacheService(String collectionName, String query)
    {
        try {
            String key = query.hashCode() + "";
            OkHttpClient client = new OkHttpClient()
                    .newBuilder()
                    .readTimeout(CACHE_TIME_OUT_MS_GET, TimeUnit.MILLISECONDS)
                    .connectTimeout(CACHE_TIME_OUT_MS_GET, TimeUnit.MILLISECONDS)
                    .build();
            Request request = new Request.Builder()
                    .url(CACHE_HOST  + "/" + collectionName + "/" + key)
                    .method("GET", null)
                    .build();

            ResponseHelperCache responseCache = getResponseCache( getBodyRequestCache(client, request));


            return getDataCacheResponse(responseCache);
        }
        catch (Exception e)
        {
            return null;
        }
    }


    public static void setCacheService(String collectionName, String query, String data)
    {
        String key = query.hashCode() + "";
        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .readTimeout(CACHE_TIME_OUT_MS_POST, TimeUnit.MILLISECONDS)
                .connectTimeout(CACHE_TIME_OUT_MS_POST, TimeUnit.MILLISECONDS)
                .build();

        BodyGrabarCache bodyGrabarCache = new BodyGrabarCache();
        bodyGrabarCache.setKeyName(key);
        bodyGrabarCache.setKeyValue(data);
        bodyGrabarCache.setCollection(collectionName);
        bodyGrabarCache.setTtl(CACHE_TTL);

        RequestBody body = getRequestBody(bodyGrabarCache);
        Request request = new Request.Builder()
                .url(CACHE_HOST +  "/grabar")
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .build();
        ResponseBody response = getBodyRequestCache(client, request);
        response.close();
    }

    public static Gson getGson()
    {
        return new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
    }

    public static ResponseBody getBodyRequestCache(OkHttpClient client, Request request)
    {

        try
        {
            return  client.newCall(request).execute().body();
        }
        catch (Exception e)
        {
            System.out.println("getBodyRequestCache: " + e.getMessage());
            return null;
        }

    }

    public static ResponseHelperCache getResponseCache(ResponseBody data)
    {
        try
        {
            if (data == null)
            {
                throw getException();
            }
            else
            {
                data.close();
                return gson.fromJson(data.string(), ResponseHelperCache.class);
            }

        }
        catch (Exception e)
        {
            return null;
        }

    }


    public static String getDataCacheResponse(ResponseHelperCache responseHelperCache)
    {
        try {
            return responseHelperCache.getData().getKeyValue();
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public static RequestBody getRequestBody(BodyGrabarCache body)
    {
        try {

            if (body == null)
            {
                throw getException();
            }
            else
            {
                MediaType mediaType = MediaType.parse("application/json");
                return RequestBody.create(mediaType, gson.toJson(body));
            }

        }
        catch (Exception e)
        {
            return null;
        }
    }

    private static Exception getException()
    {
        return new Exception("Data null");
    }

}
