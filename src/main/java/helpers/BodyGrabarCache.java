package helpers;

public class BodyGrabarCache {

    private String keyName;
    private String keyValue;
    private String collection;
    private String ttl;

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public BodyGrabarCache() {

    }
}
