package helpers;

public class CacheDTO {

    private String keyValue;

    public CacheDTO(String keyValue) {
        this.keyValue = keyValue;
    }

    public String getKeyValue() {
        return keyValue;
    }
}