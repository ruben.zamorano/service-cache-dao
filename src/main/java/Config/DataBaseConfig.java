package Config;

public class DataBaseConfig {

    private final String PREFIX_JNDI = "java:/comp/env/";

    private DataBaseEnum database;

    private String jndi;

    public DataBaseConfig(String database, String jndi) {
        this.database = DataBaseEnum.valueOf(database);
        this.jndi = PREFIX_JNDI + jndi;
    }

    public DataBaseEnum getDatabase() {
        return database;
    }

    public String getJndi() {
        return jndi;
    }
}
