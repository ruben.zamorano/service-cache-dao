package DTO;


public class RespuestaCalculoTotalesPagoInicial {

    private long lPagoDineroETotal;
    private int iTotalContadoSinAjuste;
    private int iVtaAccesoriosCel;
    private int iEngancheIlustrativo;
    private int iNumDepartamento;
    private int iImporte;
    private String sListaSku;
    private String sListaDcf;
    private String sImporteArt;
    private int iSobrePrecioSimulado12;
    private int iSobrePrecioSimulado18;
    private int iSobrePrecioSimulado24;
    private int iSobrePrecioSimuladoFinal;
    private int iFlagCMA;
    private int iResConstantePagoInicialSimuladoCMA;

    public RespuestaCalculoTotalesPagoInicial(){

    }

    /**
     * @param lPagoDineroETotal
     * @param iTotalContadoSinAjuste
     * @param iVtaAccesoriosCel
     * @param iEngancheIlustrativo
     * @param iNumDepartamento
     * @param iImporte
     * @param sListaSku
     * @param sListaDcf
     * @param sImporteArt
     * @param iSobrePrecioSimulado12
     * @param iSobrePrecioSimulado18
     * @param iSobrePrecioSimulado24
     * @param iSobrePrecioSimuladoFinal
     * @param iFlagCMA
     * @param iResConstantePagoInicialSimuladoCMA
     */
    public RespuestaCalculoTotalesPagoInicial(long lPagoDineroETotal, int iTotalContadoSinAjuste, int iVtaAccesoriosCel,
                                              int iEngancheIlustrativo, int iNumDepartamento, int iImporte, String sListaSku, String sListaDcf,
                                              String sImporteArt, int iSobrePrecioSimulado12, int iSobrePrecioSimulado18, int iSobrePrecioSimulado24,
                                              int iSobrePrecioSimuladoFinal, int iFlagCMA, int iResConstantePagoInicialSimuladoCMA) {
        this.lPagoDineroETotal = lPagoDineroETotal;
        this.iTotalContadoSinAjuste = iTotalContadoSinAjuste;
        this.iVtaAccesoriosCel = iVtaAccesoriosCel;
        this.iEngancheIlustrativo = iEngancheIlustrativo;
        this.iNumDepartamento = iNumDepartamento;
        this.iImporte = iImporte;
        this.sListaSku = sListaSku;
        this.sListaDcf = sListaDcf;
        this.sImporteArt = sImporteArt;
        this.iSobrePrecioSimulado12 = iSobrePrecioSimulado12;
        this.iSobrePrecioSimulado18 = iSobrePrecioSimulado18;
        this.iSobrePrecioSimulado24 = iSobrePrecioSimulado24;
        this.iSobrePrecioSimuladoFinal = iSobrePrecioSimuladoFinal;
        this.iFlagCMA = iFlagCMA;
        this.iResConstantePagoInicialSimuladoCMA = iResConstantePagoInicialSimuladoCMA;
    }

    /**
     * @return the lPagoDineroETotal
     */
    public long getlPagoDineroETotal() {
        return lPagoDineroETotal;
    }

    /**
     * @param lPagoDineroETotal the lPagoDineroETotal to set
     */
    public void setlPagoDineroETotal(long lPagoDineroETotal) {
        this.lPagoDineroETotal = lPagoDineroETotal;
    }

    /**
     * @return the iTotalContadoSinAjuste
     */
    public int getiTotalContadoSinAjuste() {
        return iTotalContadoSinAjuste;
    }

    /**
     * @param iTotalContadoSinAjuste the iTotalContadoSinAjuste to set
     */
    public void setiTotalContadoSinAjuste(int iTotalContadoSinAjuste) {
        this.iTotalContadoSinAjuste = iTotalContadoSinAjuste;
    }

    /**
     * @return the iVtaAccesoriosCel
     */
    public int getiVtaAccesoriosCel() {
        return iVtaAccesoriosCel;
    }

    /**
     * @param iVtaAccesoriosCel the iVtaAccesoriosCel to set
     */
    public void setiVtaAccesoriosCel(int iVtaAccesoriosCel) {
        this.iVtaAccesoriosCel = iVtaAccesoriosCel;
    }

    /**
     * @return the iEngancheIlustrativo
     */
    public int getiEngancheIlustrativo() {
        return iEngancheIlustrativo;
    }

    /**
     * @param iEngancheIlustrativo the iEngancheIlustrativo to set
     */
    public void setiEngancheIlustrativo(int iEngancheIlustrativo) {
        this.iEngancheIlustrativo = iEngancheIlustrativo;
    }

    /**
     * @return the iNumDepartamento
     */
    public int getiNumDepartamento() {
        return iNumDepartamento;
    }

    /**
     * @param iNumDepartamento the iNumDepartamento to set
     */
    public void setiNumDepartamento(int iNumDepartamento) {
        this.iNumDepartamento = iNumDepartamento;
    }

    /**
     * @return the iImporte
     */
    public int getiImporte() {
        return iImporte;
    }

    /**
     * @param iImporte the iImporte to set
     */
    public void setiImporte(int iImporte) {
        this.iImporte = iImporte;
    }

    /**
     * @return the sListaSku
     */
    public String getsListaSku() {
        return sListaSku;
    }

    /**
     * @param sListaSku the sListaSku to set
     */
    public void setsListaSku(String sListaSku) {
        this.sListaSku = sListaSku;
    }

    /**
     * @return the sListaDcf
     */
    public String getsListaDcf() {
        return sListaDcf;
    }

    /**
     * @param sListaDcf the sListaDcf to set
     */
    public void setsListaDcf(String sListaDcf) {
        this.sListaDcf = sListaDcf;
    }

    /**
     * @return the sImporteArt
     */
    public String getsImporteArt() {
        return sImporteArt;
    }

    /**
     * @param sImporteArt the sImporteArt to set
     */
    public void setsImporteArt(String sImporteArt) {
        this.sImporteArt = sImporteArt;
    }

    /**
     * @return the iSobrePrecioSimulado12
     */
    public int getiSobrePrecioSimulado12() {
        return iSobrePrecioSimulado12;
    }

    /**
     * @param iSobrePrecioSimulado12 the iSobrePrecioSimulado12 to set
     */
    public void setiSobrePrecioSimulado12(int iSobrePrecioSimulado12) {
        this.iSobrePrecioSimulado12 = iSobrePrecioSimulado12;
    }

    /**
     * @return the iSobrePrecioSimulado18
     */
    public int getiSobrePrecioSimulado18() {
        return iSobrePrecioSimulado18;
    }

    /**
     * @param iSobrePrecioSimulado18 the iSobrePrecioSimulado18 to set
     */
    public void setiSobrePrecioSimulado18(int iSobrePrecioSimulado18) {
        this.iSobrePrecioSimulado18 = iSobrePrecioSimulado18;
    }

    /**
     * @return the iSobrePrecioSimulado24
     */
    public int getiSobrePrecioSimulado24() {
        return iSobrePrecioSimulado24;
    }

    /**
     * @param iSobrePrecioSimulado24 the iSobrePrecioSimulado24 to set
     */
    public void setiSobrePrecioSimulado24(int iSobrePrecioSimulado24) {
        this.iSobrePrecioSimulado24 = iSobrePrecioSimulado24;
    }

    /**
     * @return the iSobrePrecioSimuladoFinal
     */
    public int getiSobrePrecioSimuladoFinal() {
        return iSobrePrecioSimuladoFinal;
    }

    /**
     * @param iSobrePrecioSimuladoFinal the iSobrePrecioSimuladoFinal to set
     */
    public void setiSobrePrecioSimuladoFinal(int iSobrePrecioSimuladoFinal) {
        this.iSobrePrecioSimuladoFinal = iSobrePrecioSimuladoFinal;
    }

    /**
     * @return the iFlagCMA
     */
    public int getiFlagCMA() {
        return iFlagCMA;
    }

    /**
     * @param iFlagCMA the iFlagCMA to set
     */
    public void setiFlagCMA(int iFlagCMA) {
        this.iFlagCMA = iFlagCMA;
    }

    /**
     * @return the iResConstantePagoInicialSimuladoCMA
     */
    public int getiResConstantePagoInicialSimuladoCMA() {
        return iResConstantePagoInicialSimuladoCMA;
    }

    /**
     * @param iResConstantePagoInicialSimuladoCMA the iResConstantePagoInicialSimuladoCMA to set
     */
    public void setiResConstantePagoInicialSimuladoCMA(int iResConstantePagoInicialSimuladoCMA) {
        this.iResConstantePagoInicialSimuladoCMA = iResConstantePagoInicialSimuladoCMA;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "RespuestaCalculoTotalesPagoInicial [lPagoDineroETotal=" + lPagoDineroETotal
                + ", iTotalContadoSinAjuste=" + iTotalContadoSinAjuste + ", iVtaAccesoriosCel=" + iVtaAccesoriosCel
                + ", iEngancheIlustrativo=" + iEngancheIlustrativo + ", iNumDepartamento=" + iNumDepartamento
                + ", iImporte=" + iImporte + ", sListaSku=" + sListaSku + ", sListaDcf=" + sListaDcf + ", sImporteArt="
                + sImporteArt + ", iSobrePrecioSimulado12=" + iSobrePrecioSimulado12 + ", iSobrePrecioSimulado18="
                + iSobrePrecioSimulado18 + ", iSobrePrecioSimulado24=" + iSobrePrecioSimulado24
                + ", iSobrePrecioSimuladoFinal=" + iSobrePrecioSimuladoFinal + ", iFlagCMA=" + iFlagCMA
                + ", iResConstantePagoInicialSimuladoCMA=" + iResConstantePagoInicialSimuladoCMA + "]";
    }
}
