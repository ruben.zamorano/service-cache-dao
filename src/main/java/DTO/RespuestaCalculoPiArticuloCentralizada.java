package DTO;


public class RespuestaCalculoPiArticuloCentralizada {
    private int iTotalContado;
    private int iPrecioCredito12;
    private int iPrecioCredito18;
    private int iPrecioCredito24;
    private int iSobrePrecio12;
    private int iSobrePrecio18;
    private int iSobrePrecio24;
    private int Vporcentajeinteres;
    private int iPrecioCredito;
    private int iSobrePrecio;

    public int getiPrecioCredito() {
        return iPrecioCredito;
    }

    public void setiPrecioCredito(int iPrecioCredito) {
        this.iPrecioCredito = iPrecioCredito;
    }

    public int getiSobrePrecio() {
        return iSobrePrecio;
    }

    public void setiSobrePrecio(int iSobrePrecio) {
        this.iSobrePrecio = iSobrePrecio;
    }

    public RespuestaCalculoPiArticuloCentralizada() {

    }

    /**
     * @param iTotalContado
     * @param iPrecioCredito12
     * @param iPrecioCredito18
     * @param iPrecioCredito24
     * @param iSobrePrecio12
     * @param iSobrePrecio18
     * @param iSobrePrecio24
     * @param vporcentajeinteres
     */
    public RespuestaCalculoPiArticuloCentralizada(int iTotalContado, int iPrecioCredito12, int iPrecioCredito18,
                                                  int iPrecioCredito24, int iSobrePrecio12, int iSobrePrecio18, int iSobrePrecio24, int vporcentajeinteres) {
        this.iTotalContado = iTotalContado;
        this.iPrecioCredito12 = iPrecioCredito12;
        this.iPrecioCredito18 = iPrecioCredito18;
        this.iPrecioCredito24 = iPrecioCredito24;
        this.iSobrePrecio12 = iSobrePrecio12;
        this.iSobrePrecio18 = iSobrePrecio18;
        this.iSobrePrecio24 = iSobrePrecio24;
        Vporcentajeinteres = vporcentajeinteres;
    }

    /**
     * @param iTotalContado
     * @param iPrecioCredito
     * @param iSobrePrecio
     * @param vporcentajeinteres
     */
    public RespuestaCalculoPiArticuloCentralizada(int iTotalContado, int iPrecioCredito, int iSobrePrecio, int vporcentajeinteres) {
        this.iTotalContado = iTotalContado;
        this.iPrecioCredito = iPrecioCredito;
        this.iSobrePrecio = iSobrePrecio;
        Vporcentajeinteres = vporcentajeinteres;
    }

    /**
     * @return the iTotalContado
     */
    public int getiTotalContado() {
        return iTotalContado;
    }

    /**
     * @param iTotalContado
     *            the iTotalContado to set
     */
    public void setiTotalContado(int iTotalContado) {
        this.iTotalContado = iTotalContado;
    }

    /**
     * @return the iPrecioCredito12
     */
    public int getiPrecioCredito12() {
        return iPrecioCredito12;
    }

    /**
     * @param iPrecioCredito12
     *            the iPrecioCredito12 to set
     */
    public void setiPrecioCredito12(int iPrecioCredito12) {
        this.iPrecioCredito12 = iPrecioCredito12;
    }

    /**
     * @return the iPrecioCredito18
     */
    public int getiPrecioCredito18() {
        return iPrecioCredito18;
    }

    /**
     * @param iPrecioCredito18
     *            the iPrecioCredito18 to set
     */
    public void setiPrecioCredito18(int iPrecioCredito18) {
        this.iPrecioCredito18 = iPrecioCredito18;
    }

    /**
     * @return the iPrecioCredito24
     */
    public int getiPrecioCredito24() {
        return iPrecioCredito24;
    }

    /**
     * @param iPrecioCredito24
     *            the iPrecioCredito24 to set
     */
    public void setiPrecioCredito24(int iPrecioCredito24) {
        this.iPrecioCredito24 = iPrecioCredito24;
    }

    /**
     * @return the iSobrePrecio12
     */
    public int getiSobrePrecio12() {
        return iSobrePrecio12;
    }

    /**
     * @param iSobrePrecio12
     *            the iSobrePrecio12 to set
     */
    public void setiSobrePrecio12(int iSobrePrecio12) {
        this.iSobrePrecio12 = iSobrePrecio12;
    }

    /**
     * @return the iSobrePrecio18
     */
    public int getiSobrePrecio18() {
        return iSobrePrecio18;
    }

    /**
     * @param iSobrePrecio18
     *            the iSobrePrecio18 to set
     */
    public void setiSobrePrecio18(int iSobrePrecio18) {
        this.iSobrePrecio18 = iSobrePrecio18;
    }

    /**
     * @return the iSobrePrecio24
     */
    public int getiSobrePrecio24() {
        return iSobrePrecio24;
    }

    /**
     * @param iSobrePrecio24
     *            the iSobrePrecio24 to set
     */
    public void setiSobrePrecio24(int iSobrePrecio24) {
        this.iSobrePrecio24 = iSobrePrecio24;
    }

    /**
     * @return the vporcentajeinteres
     */
    public int getVporcentajeinteres() {
        return Vporcentajeinteres;
    }

    /**
     * @param vporcentajeinteres
     *            the vporcentajeinteres to set
     */
    public void setVporcentajeinteres(int vporcentajeinteres) {
        Vporcentajeinteres = vporcentajeinteres;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "RespuestaCalculoPiArticuloCentralizada [iTotalContado=" + iTotalContado + ", iPrecioCredito12="
                + iPrecioCredito12 + ", iPrecioCredito18=" + iPrecioCredito18 + ", iPrecioCredito24=" + iPrecioCredito24
                + ", iSobrePrecio12=" + iSobrePrecio12 + ", iSobrePrecio18=" + iSobrePrecio18 + ", iSobrePrecio24="
                + iSobrePrecio24 + ", Vporcentajeinteres=" + Vporcentajeinteres + "]";
    }
}
