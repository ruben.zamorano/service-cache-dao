package DTO;


/**
 * Clase / Objeto para almacenar la informaci�n de cr�dito del �rea de Muebles
 * @author Marco Antonio Olivas Olmeda
 * @version 1.0
 * @created 02-ene-2015 11:30:15 a.m.
 */
public class CreditoMuebles {

    /**
     * Importe total de contado de la venta despues de realizar el c�lculo del pago
     * inicial para el �rea de Muebles
     */
    private int iTotalContado;
    /**
     * Importe total de cr�dito de la venta despues de realizar los c�lculos del pago
     * inicial para el �rea de Muebles
     */
    private int iTotalCredito;
    /**
     * Importe del sobreprecio calculado a 12 meses de la venta
     */
    private int iSobreprecio12;
    /**
     * Importe del sobreprecio calculado a 18 meses
     */
    private int iSobreprecio18;
    /**
     * Importe del pago inicial final que debe cubrir el cliente Coppel para realizar
     * la compra
     */
    private int iSobreprecioFinal;
    /**
     * Importe del pago inicial m�nimo que debe cubrir el cliente Coppel para realizar
     * la compra
     */
    private int iEnganchePropuesto;
    /**
     * Importe del enganche propuesto del calculo del pago inicial final. Este dato es
     * el que regresara el servicio
     */
    private int iEnganchePropuestoFinal;
    /**
     * Valor del puntaje final del param�trico de celulares despues de realizar los
     * c�lculos del pago inicial para el �rea de Muebles
     */
    private int iPuntosFinalCelulares;
    /**
     * Valor del puntaje final del param�trico de alto riesgo despues de realizar los
     * c�lculos del pago inicial para el �rea de Muebles
     */
    private int iPuntosFinalAltoRiesgo;
    /**
     * Bandera para indicar si se debe tramitar la compra despues de realizar el
     * calculo del pago inicial en el �rea de Muebles
     */
    private int iTramitarCredito;
    /**
     * Bandera para indicar si el cr�dito del cliente Coppel esta catalogado como
     * cliente joven
     */
    private int iFlagClienteJoven;
    /**
     * Importe del abono calculado a 12 meses para la compra
     */
    private int iAbono12;
    /**
     * Importe del abono calculado a 18 meses para la compra
     */
    private int iAbono18;
    /**
     * Bandera para indicar si se puede realizar la venta a cr�dito
     */
    private int iFlagVentaCredito;
    /**
     * Importe del sobreprecio que aplica para el c�lculo del pago inicial.
     */
    private int iSobreprecioInicial;
    /**
     * Importe del abono calculado que aplica para la orden de compra
     */
    private int iAbono = 0;
    /**
     * Porcentaje de saturaci�n de compra del cliente.
     */
    private int iPorcentajeSaturacion;
    /**
     * Importe calculado del % de pagos de pagos en los �ltimos 12 meses vs el nuevo
     * saldo del cliente.
     */
    private int iNuevoSaldo;
    /**
     * Pago realizados por el cliente en los �ltimos doce meses.
     */
    private int iPagoUltimosDoceMeses;
    /**
     * Representa el sobreprecio al plazo de 24 meses.
     */
    private int sobrePrecio24;
    /**
     *  Representa el abono mensual calculado a 24 meses.
     */
    private int abono24;
    /**
     *   Representa el pago inicial propuesto al plazo de 18 meses.
     */
    private int enganchePropuestoFinal18;
    /**
     *   Representa el pago inicial propuesto al plazo de 24 meses.
     */
    private int enganchePropuestoFinal24;
    private int iSobrePrecio;
    private int iAbonoMensual;
    private int iCategoria;
    private int iEstado;

    public int getiEstado() {
        return iEstado;
    }

    public void setiEstado(int iEstado) {
        this.iEstado = iEstado;
    }

    private String cuentasSaldadas;

    public String getCuentasSaldadas() {
        return cuentasSaldadas;
    }

    public void setCuentasSaldadas(String cuentasSaldadas) {
        this.cuentasSaldadas = cuentasSaldadas;
    }


    public int getiCategoria() {
        return iCategoria;
    }

    public void setiCategoria(int iCategoria) {
        this.iCategoria = iCategoria;
    }

    public int getiAbonoMensual() {
        return iAbonoMensual;
    }

    public void setiAbonoMensual(int iAbonoMensual) {
        this.iAbonoMensual = iAbonoMensual;
    }

    public int getiSobrePrecio() {
        return iSobrePrecio;
    }

    public void setiSobrePrecio(int iSobrePrecio) {
        this.iSobrePrecio = iSobrePrecio;
    }

    public int getSobrePrecio24() {
        return sobrePrecio24;
    }

    public void setSobrePrecio24(int sobrePrecio24) {
        this.sobrePrecio24 = sobrePrecio24;
    }

    public int getAbono24() {
        return abono24;
    }

    public void setAbono24(int abono24) {
        this.abono24 = abono24;
    }

    public int getEnganchePropuestoFinal18() {
        return enganchePropuestoFinal18;
    }

    public void setEnganchePropuestoFinal18(int enganchePropuestoFinal18) {
        this.enganchePropuestoFinal18 = enganchePropuestoFinal18;
    }

    public int getEnganchePropuestoFinal24() {
        return enganchePropuestoFinal24;
    }

    public void setEnganchePropuestoFinal24(int enganchePropuestoFinal24) {
        this.enganchePropuestoFinal24 = enganchePropuestoFinal24;
    }

    public CreditoMuebles(){

    }

    /**
     * @return the iAbono
     */
    public int getiAbono() {
        return iAbono;
    }

    /**
     * @return the iAbono12
     */
    public int getiAbono12() {
        return iAbono12;
    }

    /**
     * @return the iAbono18
     */
    public int getiAbono18() {
        return iAbono18;
    }

    /**
     * @return the iEnganchePropuesto
     */
    public int getiEnganchePropuesto() {
        return iEnganchePropuesto;
    }

    /**
     * @return the iEnganchePropuestoFinal
     */
    public int getiEnganchePropuestoFinal() {
        return iEnganchePropuestoFinal;
    }

    /**
     * @return the iFlagClienteJoven
     */
    public int getiFlagClienteJoven() {
        return iFlagClienteJoven;
    }

    /**
     * @return the iFlagVentaCredito
     */
    public int getiFlagVentaCredito() {
        return iFlagVentaCredito;
    }

    /**
     * @return the iNuevoSaldo
     */
    public int getiNuevoSaldo() {
        return iNuevoSaldo;
    }

    /**
     * @return the iPagoUltimosDoceMeses
     */
    public int getiPagoUltimosDoceMeses() {
        return iPagoUltimosDoceMeses;
    }

    /**
     * @return the iPorcentajeSaturacion
     */
    public int getiPorcentajeSaturacion() {
        return iPorcentajeSaturacion;
    }

    /**
     * @return the iPuntosFinalAltoRiesgo
     */
    public int getiPuntosFinalAltoRiesgo() {
        return iPuntosFinalAltoRiesgo;
    }

    /**
     * @return the iPuntosFinalCelulares
     */
    public int getiPuntosFinalCelulares() {
        return iPuntosFinalCelulares;
    }

    /**
     * @return the iSobreprecioInicial
     */
    public int getiSobreprecioInicial() {
        return iSobreprecioInicial;
    }

    /**
     * @return the iSobreprecio12
     */
    public int getiSobreprecio12() {
        return iSobreprecio12;
    }

    /**
     * @return the iSobreprecio18
     */
    public int getiSobreprecio18() {
        return iSobreprecio18;
    }

    /**
     * @return the iTotalContado
     */
    public int getiTotalContado() {
        return iTotalContado;
    }

    /**
     * @return the iTotalCredito
     */
    public int getiTotalCredito() {
        return iTotalCredito;
    }

    /**
     * @return the iTramitarCredito
     */
    public int getiTramitarCredito() {
        return iTramitarCredito;
    }

    /**
     * @param iAbono the iAbono to set
     */
    public void setiAbono(int iAbono) {
        this.iAbono = iAbono;
    }

    /**
     * @param iAbono12 the iAbono12 to set
     */
    public void setiAbono12(int iAbono12) {
        this.iAbono12 = iAbono12;
    }

    /**
     * @param iAbono18 the iAbono18 to set
     */
    public void setiAbono18(int iAbono18) {
        this.iAbono18 = iAbono18;
    }

    /**
     * @param iEnganchePropuesto the iEnganchePropuesto to set
     */
    public void setiEnganchePropuesto(int iEnganchePropuesto) {
        this.iEnganchePropuesto = iEnganchePropuesto;
    }

    /**
     * @param iEnganchePropuestoFinal the iEnganchePropuestoFinal to set
     */
    public void setiEnganchePropuestoFinal(int iEnganchePropuestoFinal) {
        this.iEnganchePropuestoFinal = iEnganchePropuestoFinal;
    }

    /**
     * @param iFlagClienteJoven the iFlagClienteJoven to set
     */
    public void setiFlagClienteJoven(int iFlagClienteJoven) {
        this.iFlagClienteJoven = iFlagClienteJoven;
    }

    /**
     * @param iFlagVentaCredito the iFlagVentaCredito to set
     */
    public void setiFlagVentaCredito(int iFlagVentaCredito) {
        this.iFlagVentaCredito = iFlagVentaCredito;
    }

    /**
     * @param iNuevoSaldo the iNuevoSaldo to set
     */
    public void setiNuevoSaldo(int iNuevoSaldo) {
        this.iNuevoSaldo = iNuevoSaldo;
    }

    /**
     * @param iPagoUltimosDoceMeses the iPagoUltimosDoceMeses to set
     */
    public void setiPagoUltimosDoceMeses(int iPagoUltimosDoceMeses) {
        this.iPagoUltimosDoceMeses = iPagoUltimosDoceMeses;
    }

    /**
     * @param iPorcentajeSaturacion the iPorcentajeSaturacion to set
     */
    public void setiPorcentajeSaturacion(int iPorcentajeSaturacion) {
        this.iPorcentajeSaturacion = iPorcentajeSaturacion;
    }

    /**
     * @param iPuntosFinalAltoRiesgo the iPuntosFinalAltoRiesgo to set
     */
    public void setiPuntosFinalAltoRiesgo(int iPuntosFinalAltoRiesgo) {
        this.iPuntosFinalAltoRiesgo = iPuntosFinalAltoRiesgo;
    }

    /**
     * @param iPuntosFinalCelulares the iPuntosFinalCelulares to set
     */
    public void setiPuntosFinalCelulares(int iPuntosFinalCelulares) {
        this.iPuntosFinalCelulares = iPuntosFinalCelulares;
    }

    /**
     * @param iSobreprecioInicial the iSobreprecioInicial to set
     */
    public void setiSobreprecioInicial(int iSobreprecioInicial) {
        this.iSobreprecioInicial = iSobreprecioInicial;
    }

    /**
     * @param iSobreprecio12 the iSobreprecio12 to set
     */
    public void setiSobreprecio12(int iSobreprecio12) {
        this.iSobreprecio12 = iSobreprecio12;
    }

    /**
     * @param iSobreprecio18 the iSobreprecio18 to set
     */
    public void setiSobreprecio18(int iSobreprecio18) {
        this.iSobreprecio18 = iSobreprecio18;
    }

    /**
     * @param iTotalContado the iTotalContado to set
     */
    public void setiTotalContado(int iTotalContado) {
        this.iTotalContado = iTotalContado;
    }

    /**
     * @param iTotalCredito the iTotalCredito to set
     */
    public void setiTotalCredito(int iTotalCredito) {
        this.iTotalCredito = iTotalCredito;
    }

    /**
     * @param iTramitarCredito the iTramitarCredito to set
     */
    public void setiTramitarCredito(int iTramitarCredito) {
        this.iTramitarCredito = iTramitarCredito;
    }

    /**
     * @return the iSobreprecioFinal
     */
    public int getiSobreprecioFinal() {
        return iSobreprecioFinal;
    }

    /**
     * @param iSobreprecioFinal the iSobreprecioFinal to set
     */
    public void setiSobreprecioFinal(int iSobreprecioFinal) {
        this.iSobreprecioFinal = iSobreprecioFinal;
    }

    @Override
    public String toString() {
        return "CreditoMuebles [iTotalContado=" + iTotalContado + ", iTotalCredito=" + iTotalCredito
                + ", iSobreprecio12=" + iSobreprecio12 + ", iSobreprecio18=" + iSobreprecio18 + ", iSobreprecioFinal="
                + iSobreprecioFinal + ", iEnganchePropuesto=" + iEnganchePropuesto + ", iEnganchePropuestoFinal="
                + iEnganchePropuestoFinal + ", iPuntosFinalCelulares=" + iPuntosFinalCelulares
                + ", iPuntosFinalAltoRiesgo=" + iPuntosFinalAltoRiesgo + ", iTramitarCredito=" + iTramitarCredito
                + ", iFlagClienteJoven=" + iFlagClienteJoven + ", iAbono12=" + iAbono12 + ", iAbono18=" + iAbono18
                + ", iFlagVentaCredito=" + iFlagVentaCredito + ", iSobreprecioInicial=" + iSobreprecioInicial
                + ", iAbono=" + iAbono + ", iPorcentajeSaturacion=" + iPorcentajeSaturacion + ", iNuevoSaldo="
                + iNuevoSaldo + ", iPagoUltimosDoceMeses=" + iPagoUltimosDoceMeses + ", sobrePrecio24=" + sobrePrecio24
                + ", abono24=" + abono24 + ", enganchePropuestoFinal18=" + enganchePropuestoFinal18
                + ", enganchePropuestoFinal24=" + enganchePropuestoFinal24 + ", iSobrePrecio=" + iSobrePrecio + "]";
    }


}