package DTO;


import java.util.Date;

/**
 * Clase / Objeto para almacenar la informaci�n de la cuenta del cliente Coppel
 * @author Marco Antonio Olivas Olmeda
 * @version 1.0
 * @created 02-ene-2015 12:29:12 p.m.
 */
public class Cuenta {

    /**
     * N�mero de cliente Coppel
     */
    private int iCliente;
    /**
     * N�mero de cuentas activas que tiene en su cr�dito el cliente Coppel
     * <b>El dato se obtiene de la respuesta de la operaci�n
     * "leerCantidadCuentasActivas()" del nuevo ccuenta en el campo
     * "cuentasactivas"</b>
     */
    private int iCuentasActivas;
    /**
     * Puntualidad que tiene el cr�dito del cliente Coppel
     * <b>El dato se obtiene en la respuesta de la operaci�n "leerDatosCliente()" del
     * nuevo ccuenta al enviar el par�metro tipoconsulta = 5, es el dato
     * "puntualidad"</b>
     */
    private String sPuntualidad;
    /**
     * Valor del limite de cr�dito que tiene la cuenta del cliente Coppel
     * <b>El dato se obtiene en la respuesta de la operaci�n "leerDatosCliente()" del
     * nuevo ccuenta al enviar el par�metro tipoconsulta = 5, es el dato
     * "limitecredito"</b>
     */
    private int iLimiteDeCredito;
    /**
     * Valor de la causa de la Situaci�n Especial que presenta la cuenta del cliente
     * Coppel
     * <b>El dato se obtiene en la respuesta de la operaci�n "leerDatosCliente()" del
     * nuevo ccuenta al enviar el par�metro tipoconsulta = 5, es el dato
     * "situacionespecial"</b>
     */
    private String sSituacionEspecial;
    /**
     * Valor de la causa de la situaci�n especial que presenta la cuenta del cliente
     * Coppel
     * <b>El dato se obtiene en la respuesta de la operaci�n "leerDatosCliente()" del
     * nuevo ccuenta al enviar el par�metro tipoconsulta = 5, es el dato
     * "causasitesp"</b>
     */
    private int iCausaSituacionEspecial;
    /**
     * Importe del saldo en la cuenta de Ropa que tiene el cliente Coppel
     * <b>El dato se obtiene en la respuesta de operaci�n "leerCuentasCliente()" del
     * nuevo ccuenta, el dato es "saldo" cuando "tipocuenta = 0".</b>
     * <b>NOTA: </b>Si no el cuenta no tiene tipocuenta = 0 quiere decir que no tiene
     * saldo pendiente en la cuenta de Ropa
     */
    private int iSaldoRopa;
    /**
     * Valor de pesos unidad cr�dito para el c�lculo del pago inicial
     * <b>Este dato se obtiene de la respuesta de la funci�n PostgreSql
     * "func_gnconsultardatosgndominio()" en el campo "pesosunidadcredito"</b>
     */
    private int iPesosUnidadCredito;
    /**
     * Valor de la tasa de interes del �rea de Ropa que se utiliza para el c�lculo del
     * pago inicial
     *
     * <b>Este dato se obtiene de la respuesta de la funci�n PostgreSql
     * "func_gnconsultardatosgndominio()" en el campo "tasainteresropa"</b>
     */
    private int iTasaInteresRopa;
    /**
     * Valor del plazo al cual se debe calcular la compra a cr�dito en el �rea de
     * Ropa
     * <b>Este dato se puede obtener de dos partes.</b>
     * <ol>
     * 	<li><b>De la respuesta de la operaci�n "leerCuentasCliente()" en el campo
     * "plazo" cuando "tipocuenta = 0"</b></li>
     * 	<li><b>De la respuesta de la funci�n PostgreSql "fun_crobtenerdatoscliente()"
     * en el campo "plazoropa"</b></li>
     * </ol>
     * <b>NOTA: </b>Primero se debe consultar el ccuenta nuevo, si no tiene cuenta en
     * ropa (tipocuenta = 0) se debe consultar la funci�n postgresql
     * "fun_crobtenerdatoscliente()"
     */
    private int iPlazoRopa;
    /**
     * Importe total del vencido que tiene el cliente Coppel
     * <b>Este dato se obtiene en la respuesta de la operaci�n "leerCuentasCliente"
     * del nuevo ccuenta cuando num_tipodecuenta = 999, el dato es "imp_vencido"</b>
     */
    private int iTotalVencido;
    /**
     * Importe del abono base que tiene el cliente Coppel
     * <b>Este dato se obtiene en la respuesta de la operaci�n "leerCuentasCliente"
     * del nuevo ccuenta cuando num_tipodecuenta = 999, el dato es "imp_abonobase"</b>
     */
    private int iTotalAbonoBase;
    /**
     * Importe total del interes adicional que tiene el cliente Coppel
     * <b>Este dato se obtiene en la respuesta de la operaci�n "leerCuentasCliente"
     * del nuevo ccuenta cuando num_tipodecuenta = 999, el dato es
     * "imp_interesadicional"</b>
     */
    private int iTotalInteresAdicional;
    /**
     * Valor que presenta el cr�dito del cliente Coppel para realizar el c�lculo del
     * pago inicial
     * <b>Este dato se obtiene en la respuesta de la operaci�n
     * "leerCantidadCuentasActivas()" del nuevo ccuenta, es el dato
     * "cuentasactivasmuebles30"</b>
     */
    private int iCuentas30Dias3000;
    /**
     * Fecha de la primer compra registrada por el cliente Coppel
     * <b>Este dato se obtiene en la operaci�n "leerDatosCliente" del nuevo ccuenta
     * cuando se env�a tipoconsulta = 4, el dato es "fechaprimercompra"</b>
     */
    private Date dFechaPrimerCompra;
    /**
     * D�a de la primer compra registrada por el cliente Coppel
     * <b>Este dato se obtiene de la fechaprimercompra. Es el d�a que se encuentra en
     * ese dato</b>
     */
    private int iDiaPrimerCompra;
    /**
     * Mes de la primer compra registrada del cliente Coppel
     * <b>Este dato se obtiene del mes que tenga el dato fechaprimercompra</b>
     */
    private int iMesPrimerCompra;
    /**
     * A�o de la primer compra registrada por el cliente Coppel
     * <b>Este dato se obtiene del mes que tenga el dato fechaprimercompra</b>
     */
    private int iAnioPrimerCompra;
    /**
     * Valor de los puntos del par�metrico de alto riesgo para el c�lculo del pago
     * inicial
     * <b>Este dato se obtiene en la operaci�n "leerDatosCliente" del nuevo ccuenta
     * cuando se env�a tipoconsulta = 4, el dato es "paraltoriesgo"</b>
     */
    private int iPuntosAltoRiesgo;
    /**
     * Valor de los puntos del par�metrico de celulares para el c�lculo del pago
     * inicial
     * <b>Este dato se obtiene en la operaci�n "leerDatosCliente" del nuevo ccuenta
     * cuando se env�a tipoconsulta = 4, el dato es "parcelulares"</b>
     */
    private int iPuntosCelulares;
    /**
     * Valor del param�trico del modelo de celulares para el c�lculo del pago inicial
     * <b>Este dato se obtiene en la operaci�n "leerDatosCliente" del nuevo ccuenta
     * cuando se env�a tipoconsulta = 4, el dato es "modelocelulares"</b>
     */
    private String sParametricoModeloCelulares;
    /**
     * Bandera para indicar si los c�lculos para el pago inicial de muebles se deben
     * realizar a <b>18</b> meses. Para coppel.com actualmente se utiliza solo <b>12
     * meses </b>(<b>0</b>)
     * <b>2 = 24 meses</b>
     * <b>1 = 18 meses</b>
     * <b>0 = 12 meses</b>
     */
    private int iFlagPlazo;
    /**
     * N�mero de cuentas activas de Muebles
     * <b>Este dato se obtiene en la respuesta de la operaci�n
     * "leerCantidadCuentasActivas()" del nuevo ccuenta, es el dato
     * "cuentasactivasmuebles"</b>
     */
    private int iCuentasActivasMuebles;
    /**
     * Valor de la linea real de cr�dito que tiene el cliente Coppel en su cuenta
     * <b>Este dato se obtiene de la respuesta de la operaci�n "" del nuevo ccuenta</b>
     */
    private int iLineaRealDeCredito;
    /**
     * Pago de los ultimos doce meses del cliente.
     * dato imp_pagoultimosdocemeses del metodo leerdatosCliente consulta 3
     */
    private int iPagoUltimosDoceMeses;
    /**
     * Prepuntaje asignado en cartera al cliente.
     */
    private int iPrepuntaje;
    /**
     * Indicador de linea de credito que indica si es especial. Dato
     * flagLineaCreditoEsp del metodo leermargencredito.
     */
    private int iFlagLineaCreditoEspecial;
    /**
     * Saldo del cliente multiplicado por el factor de ajuste. Este dato se obtiene
     * del valor dato saldocliente del metodo leerMargenCredito
     */
    private int iSaldoClienteFactorizado;
    /**
     * Fecha de alto del cliente Coppel
     */
    private Date dFechAlta;
    /**
     * Fecha de alto del cliente Coppel (String)
     */
    private String sFechAlta;
    /**
     * Importe del saldo que presenta el cliente del resultado del parametrico
     */
    private int iSaldoClienteSinAjuste;
    /**
     * Importe del saldo de las cuentas de Muebles del cliente Coppel
     */
    private int iSaldoMuebles;
    /**
     * Importe del saldo en la cuenta de Tiempo Aire del cliente Coppel
     */
    private int iSaldoTiempoAire;
    /**
     * Importe del saldo en la cuenta de Prestamos del cliente Coppel
     */
    private int iSaldoPrestamos;
    /**
     * Importe del saldo en la cuenta de Bancoppel del cliente Coppel
     */
    private int iSaldoBancoppel;
    /**
     * Importe del saldo en la cuenta Revolvente del cliente Coppel
     */
    private int iSaldoRevolvente;
    /**
     * Importe del saldo de Dinero electr�nico que tiene el cliente Coppel en su
     * cr�dito
     */
    private int iSaldoDineroElectronico;

    private String sNombre;

    private String sFechaPrimeraCompra;

    private int iFlagDescuentoEspecial;

    private int margenCredito;

    private int lineaCreditoReal;

    /**
     * Total con el que salda el cliente
     */
    private int iSaldaCon;

    /**
     * Suma del Abono Base solamente de Ropa
     */
    private int iAbonoBaseRopa;

    /**
     * Suma del vencido de Ropa.
     */
    private int iVencidoRopa;

    /**
     * Suma del importe con el cual salda ropa.
     */
    private int iSaldaConRopa;

    private String sApPaterno;
    private String sApMaterno;

    public String getsApPaterno() {
        return sApPaterno;
    }

    public void setsApPaterno(String sApPaterno) {
        this.sApPaterno = sApPaterno;
    }

    public String getsApMaterno() {
        return sApMaterno;
    }

    public void setsApMaterno(String sApMaterno) {
        this.sApMaterno = sApMaterno;
    }

    public Cuenta(){

    }

    /**
     * @return the dFechAlta
     */
    public Date getdFechAlta() {
        return dFechAlta;
    }

    /**
     * @return the dFechaPrimerCompra
     */
    public Date getdFechaPrimerCompra() {
        return dFechaPrimerCompra;
    }

    /**
     * @return the iAnioPrimerCompra
     */
    public int getiAnioPrimerCompra() {
        return iAnioPrimerCompra;
    }

    /**
     * @return the iCausaSituacionEspecial
     */
    public int getiCausaSituacionEspecial() {
        return iCausaSituacionEspecial;
    }

    /**
     * @return the iCliente
     */
    public int getiCliente() {
        return iCliente;
    }

    /**
     * @return the iCuentas30Dias3000
     */
    public int getiCuentas30Dias3000() {
        return iCuentas30Dias3000;
    }

    /**
     * @return the iCuentasActivas
     */
    public int getiCuentasActivas() {
        return iCuentasActivas;
    }

    /**
     * @return the iCuentasActivasMuebles
     */
    public int getiCuentasActivasMuebles() {
        return iCuentasActivasMuebles;
    }

    /**
     * @return the iDiaPrimerCompra
     */
    public int getiDiaPrimerCompra() {
        return iDiaPrimerCompra;
    }

    /**
     * @return the iFlagLineaCreditoEspecial
     */
    public int getiFlagLineaCreditoEspecial() {
        return iFlagLineaCreditoEspecial;
    }

    /**
     * @return the iFlagPlazo
     */
    public int getiFlagPlazo() {
        return iFlagPlazo;
    }

    /**
     * @return the iLimiteDeCredito
     */
    public int getiLimiteDeCredito() {
        return iLimiteDeCredito;
    }

    /**
     * @return the iLineaRealDeCredito
     */
    public int getiLineaRealDeCredito() {
        return iLineaRealDeCredito;
    }

    /**
     * @return the iMesPrimerCompra
     */
    public int getiMesPrimerCompra() {
        return iMesPrimerCompra;
    }

    /**
     * @return the iPagoUltimosDoceMeses
     */
    public int getiPagoUltimosDoceMeses() {
        return iPagoUltimosDoceMeses;
    }

    /**
     * @return the iPesosUnidadCredito
     */
    public int getiPesosUnidadCredito() {
        return iPesosUnidadCredito;
    }

    /**
     * @return the iPlazoRopa
     */
    public int getiPlazoRopa() {
        return iPlazoRopa;
    }

    /**
     * @return the iPrepuntaje
     */
    public int getiPrepuntaje() {
        return iPrepuntaje;
    }

    /**
     * @return the iPuntosAltoRiesgo
     */
    public int getiPuntosAltoRiesgo() {
        return iPuntosAltoRiesgo;
    }

    /**
     * @return the iPuntosCelulares
     */
    public int getiPuntosCelulares() {
        return iPuntosCelulares;
    }

    /**
     * @return the iSaldoBancoppel
     */
    public int getiSaldoBancoppel() {
        return iSaldoBancoppel;
    }

    /**
     * @return the iSaldoClienteFactorizado
     */
    public int getiSaldoClienteFactorizado() {
        return iSaldoClienteFactorizado;
    }

    /**
     * @return the iSaldoClienteSinAjuste
     */
    public int getiSaldoClienteSinAjuste() {
        return iSaldoClienteSinAjuste;
    }

    /**
     * @return the iSaldoDineroElectronico
     */
    public int getiSaldoDineroElectronico() {
        return iSaldoDineroElectronico;
    }

    /**
     * @return the iSaldoMuebles
     */
    public int getiSaldoMuebles() {
        return iSaldoMuebles;
    }

    /**
     * @return the iSaldoPrestamos
     */
    public int getiSaldoPrestamos() {
        return iSaldoPrestamos;
    }

    /**
     * @return the iSaldoRevolvente
     */
    public int getiSaldoRevolvente() {
        return iSaldoRevolvente;
    }

    /**
     * @return the iSaldoRopa
     */
    public int getiSaldoRopa() {
        return iSaldoRopa;
    }

    /**
     * @return the iSaldoTiempoAire
     */
    public int getiSaldoTiempoAire() {
        return iSaldoTiempoAire;
    }

    /**
     * @return the iTasaInteresRopa
     */
    public int getiTasaInteresRopa() {
        return iTasaInteresRopa;
    }

    /**
     * @return the iTotalAbonoBase
     */
    public int getiTotalAbonoBase() {
        return iTotalAbonoBase;
    }

    /**
     * @return the iTotalInteresAdicional
     */
    public int getiTotalInteresAdicional() {
        return iTotalInteresAdicional;
    }

    /**
     * @return the iTotalVencido
     */
    public int getiTotalVencido() {
        return iTotalVencido;
    }

    /**
     * @return the sParametricoModeloCelulares
     */
    public String getsParametricoModeloCelulares() {
        return sParametricoModeloCelulares;
    }

    /**
     * @return the sPuntualidad
     */
    public String getsPuntualidad() {
        return sPuntualidad;
    }

    /**
     * @return the sSituacionEspecial
     */
    public String getsSituacionEspecial() {
        return sSituacionEspecial;
    }

    /**
     * @param dFechAlta the dFechAlta to set
     */
    public void setdFechAlta(Date dFechAlta) {
        this.dFechAlta = dFechAlta;
    }

    /**
     * @param dFechaPrimerCompra the dFechaPrimerCompra to set
     */
    public void setdFechaPrimerCompra(Date dFechaPrimerCompra) {
        this.dFechaPrimerCompra = dFechaPrimerCompra;
    }

    /**
     * @param iAnioPrimerCompra the iAnioPrimerCompra to set
     */
    public void setiAnioPrimerCompra(int iAnioPrimerCompra) {
        this.iAnioPrimerCompra = iAnioPrimerCompra;
    }

    /**
     * @param iCausaSituacionEspecial the iCausaSituacionEspecial to set
     */
    public void setiCausaSituacionEspecial(int iCausaSituacionEspecial) {
        this.iCausaSituacionEspecial = iCausaSituacionEspecial;
    }

    /**
     * @param iCliente the iCliente to set
     */
    public void setiCliente(int iCliente) {
        this.iCliente = iCliente;
    }

    /**
     * @param iCuentas30Dias3000 the iCuentas30Dias3000 to set
     */
    public void setiCuentas30Dias3000(int iCuentas30Dias3000) {
        this.iCuentas30Dias3000 = iCuentas30Dias3000;
    }

    /**
     * @param iCuentasActivas the iCuentasActivas to set
     */
    public void setiCuentasActivas(int iCuentasActivas) {
        this.iCuentasActivas = iCuentasActivas;
    }

    /**
     * @param iCuentasActivasMuebles the iCuentasActivasMuebles to set
     */
    public void setiCuentasActivasMuebles(int iCuentasActivasMuebles) {
        this.iCuentasActivasMuebles = iCuentasActivasMuebles;
    }

    /**
     * @param iDiaPrimerCompra the iDiaPrimerCompra to set
     */
    public void setiDiaPrimerCompra(int iDiaPrimerCompra) {
        this.iDiaPrimerCompra = iDiaPrimerCompra;
    }

    /**
     * @param iFlagLineaCreditoEspecial the iFlagLineaCreditoEspecial to set
     */
    public void setiFlagLineaCreditoEspecial(int iFlagLineaCreditoEspecial) {
        this.iFlagLineaCreditoEspecial = iFlagLineaCreditoEspecial;
    }

    /**
     * @param iFlagPlazo the iFlagPlazo18 to set
     */
    public void setiFlagPlazo(int iFlagPlazo) {
        this.iFlagPlazo = iFlagPlazo;
    }

    /**
     * @param iLimiteDeCredito the iLimiteDeCredito to set
     */
    public void setiLimiteDeCredito(int iLimiteDeCredito) {
        this.iLimiteDeCredito = iLimiteDeCredito;
    }

    /**
     * @param iLineaRealDeCredito the iLineaRealDeCredito to set
     */
    public void setiLineaRealDeCredito(int iLineaRealDeCredito) {
        this.iLineaRealDeCredito = iLineaRealDeCredito;
    }

    /**
     * @param iMesPrimerCompra the iMesPrimerCompra to set
     */
    public void setiMesPrimerCompra(int iMesPrimerCompra) {
        this.iMesPrimerCompra = iMesPrimerCompra;
    }

    /**
     * @param iPagoUltimosDoceMeses the iPagoUltimosDoceMeses to set
     */
    public void setiPagoUltimosDoceMeses(int iPagoUltimosDoceMeses) {
        this.iPagoUltimosDoceMeses = iPagoUltimosDoceMeses;
    }

    /**
     * @param iPesosUnidadCredito the iPesosUnidadCredito to set
     */
    public void setiPesosUnidadCredito(int iPesosUnidadCredito) {
        this.iPesosUnidadCredito = iPesosUnidadCredito;
    }

    /**
     * @param iPlazoRopa the iPlazoRopa to set
     */
    public void setiPlazoRopa(int iPlazoRopa) {
        this.iPlazoRopa = iPlazoRopa;
    }

    /**
     * @param iPrepuntaje the iPrepuntaje to set
     */
    public void setiPrepuntaje(int iPrepuntaje) {
        this.iPrepuntaje = iPrepuntaje;
    }

    /**
     * @param iPuntosAltoRiesgo the iPuntosAltoRiesgo to set
     */
    public void setiPuntosAltoRiesgo(int iPuntosAltoRiesgo) {
        this.iPuntosAltoRiesgo = iPuntosAltoRiesgo;
    }

    /**
     * @param iPuntosCelulares the iPuntosCelulares to set
     */
    public void setiPuntosCelulares(int iPuntosCelulares) {
        this.iPuntosCelulares = iPuntosCelulares;
    }

    /**
     * @param iSaldoBancoppel the iSaldoBancoppel to set
     */
    public void setiSaldoBancoppel(int iSaldoBancoppel) {
        this.iSaldoBancoppel = iSaldoBancoppel;
    }

    /**
     * @param iSaldoClienteFactorizado the iSaldoClienteFactorizado to set
     */
    public void setiSaldoClienteFactorizado(int iSaldoClienteFactorizado) {
        this.iSaldoClienteFactorizado = iSaldoClienteFactorizado;
    }

    /**
     * @param iSaldoClienteSinAjuste the iSaldoClienteSinAjuste to set
     */
    public void setiSaldoClienteSinAjuste(int iSaldoClienteSinAjuste) {
        this.iSaldoClienteSinAjuste = iSaldoClienteSinAjuste;
    }

    /**
     * @param iSaldoDineroElectronico the iSaldoDineroElectronico to set
     */
    public void setiSaldoDineroElectronico(int iSaldoDineroElectronico) {
        this.iSaldoDineroElectronico = iSaldoDineroElectronico;
    }

    /**
     * @param iSaldoMuebles the iSaldoMuebles to set
     */
    public void setiSaldoMuebles(int iSaldoMuebles) {
        this.iSaldoMuebles = iSaldoMuebles;
    }

    /**
     * @param iSaldoPrestamos the iSaldoPrestamos to set
     */
    public void setiSaldoPrestamos(int iSaldoPrestamos) {
        this.iSaldoPrestamos = iSaldoPrestamos;
    }

    /**
     * @param iSaldoRevolvente the iSaldoRevolvente to set
     */
    public void setiSaldoRevolvente(int iSaldoRevolvente) {
        this.iSaldoRevolvente = iSaldoRevolvente;
    }

    /**
     * @param iSaldoRopa the iSaldoRopa to set
     */
    public void setiSaldoRopa(int iSaldoRopa) {
        this.iSaldoRopa = iSaldoRopa;
    }

    /**
     * @param iSaldoTiempoAire the iSaldoTiempoAire to set
     */
    public void setiSaldoTiempoAire(int iSaldoTiempoAire) {
        this.iSaldoTiempoAire = iSaldoTiempoAire;
    }

    /**
     * @param iTasaInteresRopa the iTasaInteresRopa to set
     */
    public void setiTasaInteresRopa(int iTasaInteresRopa) {
        this.iTasaInteresRopa = iTasaInteresRopa;
    }

    /**
     * @param iTotalAbonoBase the iTotalAbonoBase to set
     */
    public void setiTotalAbonoBase(int iTotalAbonoBase) {
        this.iTotalAbonoBase = iTotalAbonoBase;
    }

    /**
     * @param iTotalInteresAdicional the iTotalInteresAdicional to set
     */
    public void setiTotalInteresAdicional(int iTotalInteresAdicional) {
        this.iTotalInteresAdicional = iTotalInteresAdicional;
    }

    /**
     * @param iTotalVencido the iTotalVencido to set
     */
    public void setiTotalVencido(int iTotalVencido) {
        this.iTotalVencido = iTotalVencido;
    }

    /**
     * @param sParametricoModeloCelulares the sParametricoModeloCelulares to set
     */
    public void setsParametricoModeloCelulares(String sParametricoModeloCelulares) {
        this.sParametricoModeloCelulares = sParametricoModeloCelulares;
    }

    /**
     * @param sPuntualidad the sPuntualidad to set
     */
    public void setsPuntualidad(String sPuntualidad) {
        this.sPuntualidad = sPuntualidad;
    }

    /**
     * @param sSituacionEspecial the sSituacionEspecial to set
     */
    public void setsSituacionEspecial(String sSituacionEspecial) {
        this.sSituacionEspecial = sSituacionEspecial;
    }

    public String getsNombre() {
        return sNombre;
    }

    public void setsNombre(String sNombre) {
        this.sNombre = sNombre;
    }

    public String getsFechaPrimeraCompra() {
        return sFechaPrimeraCompra;
    }

    public void setsFechaPrimeraCompra(String sFechaPrimeraCompra) {
        this.sFechaPrimeraCompra = sFechaPrimeraCompra;
    }

    public String getsFechAlta() {
        return sFechAlta;
    }

    public void setsFechAlta(String sFechAlta) {
        this.sFechAlta = sFechAlta;
    }

    public int getiFlagDescuentoEspecial() {
        return iFlagDescuentoEspecial;
    }

    public void setiFlagDescuentoEspecial(int iFlagDescuentoEspecial) {
        this.iFlagDescuentoEspecial = iFlagDescuentoEspecial;
    }

    public int getMargenCredito() {
        return margenCredito;
    }

    public void setMargenCredito(int margenCredito) {
        this.margenCredito = margenCredito;
    }

    public int getLineaCreditoReal() {
        return lineaCreditoReal;
    }

    public void setLineaCreditoReal(int lineaRealCreditoPesos) {
        this.lineaCreditoReal = lineaRealCreditoPesos;
    }

    /**
     * @return the iSaldaCon
     */
    public int getiSaldaCon() {
        return iSaldaCon;
    }

    /**
     * @param iSaldaCon the iSaldaCon to set
     */
    public void setiSaldaCon(int iSaldaCon) {
        this.iSaldaCon = iSaldaCon;
    }

    /**
     * @return the iAbonoBaseRopa
     */
    public int getiAbonoBaseRopa() {
        return iAbonoBaseRopa;
    }

    /**
     * @param iAbonoBaseRopa the iAbonoBaseRopa to set
     */
    public void setiAbonoBaseRopa(int iAbonoBaseRopa) {
        this.iAbonoBaseRopa = iAbonoBaseRopa;
    }

    /**
     * @return the iVencidoRopa
     */
    public int getiVencidoRopa() {
        return iVencidoRopa;
    }

    /**
     * @param iVencidoRopa the iVencidoRopa to set
     */
    public void setiVencidoRopa(int iVencidoRopa) {
        this.iVencidoRopa = iVencidoRopa;
    }

    /**
     * @return the iSaldaConRopa
     */
    public int getiSaldaConRopa() {
        return iSaldaConRopa;
    }

    /**
     * @param iSaldaConRopa the iSaldaConRopa to set
     */
    public void setiSaldaConRopa(int iSaldaConRopa) {
        this.iSaldaConRopa = iSaldaConRopa;
    }
}