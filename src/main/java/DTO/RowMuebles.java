package DTO;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Clase que representa el objeto de salida de Muebles del C?lculo Pago Inicial
 * General
 *
 * @author Ramses Santos
 *
 */

@XmlRootElement(name = "row", namespace = "")
@XmlAccessorType(XmlAccessType.FIELD)
public class RowMuebles {
    private String respagoinicialpropuesto12;
    private String respagoinicialminimo12;
    private String respagoinicialpropuesto18;
    private String respagoinicialminimo18;
    private String respagoinicialpropuesto24;
    private String respagoinicialminimo24;
    private String respagoinicialpropuesto30;
    private String respagoinicialminimo30;
    private String respagoinicialpropuesto36;
    private String respagoinicialminimo36;
    private String respuntajefinal12;
    private String respuntajefinal18;
    private String respuntajefinal24;
    private String resnuevosaldo;
    private String restipopagoinicial;
    private String resporcentajepagoinicial12;
    private String resporcentajepagominimo12;
    private String resporcentajepagoinicial18;
    private String resporcentajepagominimo18;
    private String resporcentajepagoinicial24;
    private String resporcentajepagominimo24;
    private String resporcentajepagoinicial30;
    private String resporcentajepagominimo30;
    private String resporcentajepagoinicial36;
    private String resporcentajepagominimo36;
    private String itotalfactura;
    private String vpagoinicial;
    private String vpagoinicialminimo;
    private String vporcentajepagofinal;
    private String viflagprimeracompra;
    private String vipuntajeprimeracompra;
    private String vporcentajesaturacionfinal;
    private String vipuntosporcentajesaturacion;
    private String vipuntosporcentajesaturacionfinal;
    private String viporcpagosdocemesesvsnvosaldo;
    private String vipuntospagosdocemesesvsnvosaldo;
    private String vipuntosdepartamento;
    private String vimesesantiguedadcte;
    private String vipuntosantiguedadcte;
    private String vipuntajeinicial;
    private String vexceso;
    private String ipuntajealtoriesgo;
    private String inum_puntosparametricosclientesnuevos;
    private String isumapuntosvariables;
    private String inumpuntosvariablemodelotiendafinal;
    private String respuntajefinal;
    private String ffactorlineafinal;
    private String iflagcalculoparametrico;
    private String iporcentajesaturacion;
    private String iporcentajesaturacion18;
    private String iporcentajesaturacion24;
    private String lmargencreditocliente;
    private String lexcesocredito;
    private String ffactorlineanormal;
    private String ffactorlineaespecial;
    private String ffactorlineabasica;
    private String ffactortopemaximo;
    private String iporcentajesaturaciontopemaximo;
    private String iporcentajesaturaciontopemaximo18;
    private String iporcentajesaturaciontopemaximo24;
    private String iporcentajesaturaciontopemaximofinal;
    private String ivsmcduranteventa;
    private String iflagtopefactura;
    private String imontomaximofactura;
    private String iexcesotopemax;
    private String iexcesotopemaxfinal;
    private String iflagpagocompleto;
    private String ipagocomparativotope;
    private String irescma;
    private String iresfactorcmafinal;
    private String iresabononuevacomprasimulado;
    private String iresabononuevacomprasimuladofinal;
    private String iresporcentajepicmasimulado;
    private String iresporcentajepicmasimuladofinal;
    private String irespicmasimulado;
    private String irespicmasimuladofinal;
    private String iresporcentajepicmareal;
    private String iresporcentajepicmarealfinal;
    private String iresabonodisponibledelcliente;
    private String iresventatotalcreditocompleta;
    private String iresabonodisponiblesimulado;
    private String iresporclimitesuperior;
    private String iresporcsaturacioncmafinal;
    private String iresabonobasedespcomprafinal;
    private String iresporcpicmafinalmaximofinal;
    private String iresporccmafinalmaximofinal;
    private String iresporcpicmafinalminimofinal;
    private String iresporccmafinalminimofinal;
    private String irespicmafinalmaximofinal;
    private String irespicmafinalminimofinal;
    private String irespioperacionmaximofinal;
    private String irespioperacionminimofinal;
    private String irespicmarealfinal;
    private String iresconstanteabonobasemuebles;
    private String iressobrepreciosimuladofinal;
    private String iresflaglineacreditofinal;
    private String iresplazosalidapi;
    PlazoPagoInicial plazospagoinicial;
    private String crlv_clientehit;
    private String irnum_cuentasactivas;
    private String irnum_cuentassaldadasperiodo;
    private String irnum_puntoscuentasactivas;
    private String irnum_puntoscuentasactivasysaldadas;
    private String irnum_puntoscuentassaldadas;
    private String irmp_pago12meses;
    private String irnum_estado;
    private String irnum_puntosestado;
    private String crlv_categoria;
    private String irnum_puntoscategoria;
    private String irnum_diasprimeracompra;
    private String irnum_puntosdiasprimeracompra;
    private String irnum_nuevosaldofactorizado;
    private String irnum_nuevosaldo;
    private String irburo_scoresc01;
    private String irnum_puntosscore01;
    private String irburo_limcreditors22;
    private String irnum_puntoslimcreditors22;
    private String irburo_prccreditors26;
    private String irnum_puntosprccreditors26;
    private String irburo_cuentasmorosidadrs13;
    private String irburo_totalvencidosrs24;
    private String irburo_idtramautilizada;
    private String drburo_fecconsultatramautilizada;
    private String irnum_puntossaldodespuesdelaventa;
    private String irnum_puntostotalvencidosrs24ymorosidadrs13;



    public String getIrnum_puntostotalvencidosrs24ymorosidadrs13() {
        return irnum_puntostotalvencidosrs24ymorosidadrs13;
    }

    public void setIrnum_puntostotalvencidosrs24ymorosidadrs13(String irnum_puntostotalvencidosrs24ymorosidadrs13) {
        this.irnum_puntostotalvencidosrs24ymorosidadrs13 = irnum_puntostotalvencidosrs24ymorosidadrs13;
    }

    public String getIrnum_cuentasactivas() {
        return irnum_cuentasactivas;
    }

    public void setIrnum_cuentasactivas(String irnum_cuentasactivas) {
        this.irnum_cuentasactivas = irnum_cuentasactivas;
    }

    public String getDrburo_fecconsultatramautilizada() {
        return drburo_fecconsultatramautilizada;
    }

    public void setDrburo_fecconsultatramautilizada(String drburo_fecconsultatramautilizada) {
        this.drburo_fecconsultatramautilizada = drburo_fecconsultatramautilizada;
    }

    public String getIrnum_puntossaldodespuesdelaventa() {
        return irnum_puntossaldodespuesdelaventa;
    }

    public void setIrnum_puntossaldodespuesdelaventa(String irnum_puntossaldodespuesdelaventa) {
        this.irnum_puntossaldodespuesdelaventa = irnum_puntossaldodespuesdelaventa;
    }

    public String getIrnum_cuentassaldadasperiodo() {
        return irnum_cuentassaldadasperiodo;
    }

    public String getCrlv_clientehit() {
        return crlv_clientehit;
    }

    public void setCrlv_clientehit(String crlv_clientehit) {
        this.crlv_clientehit = crlv_clientehit;
    }

    public String getInum_cuentasactivas() {
        return irnum_cuentasactivas;
    }

    public void setInum_cuentasactivas(String inum_cuentasactivas) {
        this.irnum_cuentasactivas = inum_cuentasactivas;
    }

    public String getrInum_cuentassaldadasperiodo() {
        return irnum_cuentassaldadasperiodo;
    }

    public void setIrnum_cuentassaldadasperiodo(String inum_cuentassaldadasperiodo) {
        this.irnum_cuentassaldadasperiodo = inum_cuentassaldadasperiodo;
    }

    public String getIrnum_puntoscuentasactivas() {
        return irnum_puntoscuentasactivas;
    }

    public void setIrnum_puntoscuentasactivas(String irnum_puntoscuentasactivas) {
        this.irnum_puntoscuentasactivas = irnum_puntoscuentasactivas;
    }

    public String getIrnum_puntoscuentasactivasysaldadas() {
        return irnum_puntoscuentasactivasysaldadas;
    }

    public void setIrnum_puntoscuentasactivasysaldadas(String irnum_puntoscuentasactivasysaldadas) {
        this.irnum_puntoscuentasactivasysaldadas = irnum_puntoscuentasactivasysaldadas;
    }

    public String getIrnum_puntoscuentassaldadas() {
        return irnum_puntoscuentassaldadas;
    }

    public void setIrnum_puntoscuentassaldadas(String irnum_puntoscuentassaldadas) {
        this.irnum_puntoscuentassaldadas = irnum_puntoscuentassaldadas;
    }

    public String getIrmp_pago12meses() {
        return irmp_pago12meses;
    }

    public void setIrmp_pago12meses(String irmp_pago12meses) {
        this.irmp_pago12meses = irmp_pago12meses;
    }

    public String getIrnum_estado() {
        return irnum_estado;
    }

    public void setIrnum_estado(String irnum_estado) {
        this.irnum_estado = irnum_estado;
    }

    public String getIrnum_puntosestado() {
        return irnum_puntosestado;
    }

    public void setIrnum_puntosestado(String irnum_puntosestado) {
        this.irnum_puntosestado = irnum_puntosestado;
    }

    public String getCrlv_categoria() {
        return crlv_categoria;
    }

    public void setCrlv_categoria(String crlv_categoria) {
        this.crlv_categoria = crlv_categoria;
    }

    public String getIrnum_puntoscategoria() {
        return irnum_puntoscategoria;
    }

    public void setIrnum_puntoscategoria(String irnum_puntoscategoria) {
        this.irnum_puntoscategoria = irnum_puntoscategoria;
    }

    public String getIrnum_diasprimeracompra() {
        return irnum_diasprimeracompra;
    }

    public void setIrnum_diasprimeracompra(String irnum_diasprimeracompra) {
        this.irnum_diasprimeracompra = irnum_diasprimeracompra;
    }

    public String getIrnum_puntosdiasprimeracompra() {
        return irnum_puntosdiasprimeracompra;
    }

    public void setIrnum_puntosdiasprimeracompra(String irnum_puntosdiasprimeracompra) {
        this.irnum_puntosdiasprimeracompra = irnum_puntosdiasprimeracompra;
    }

    public String getIrnum_nuevosaldofactorizado() {
        return irnum_nuevosaldofactorizado;
    }

    public void setIrnum_nuevosaldofactorizado(String irnum_nuevosaldofactorizado) {
        this.irnum_nuevosaldofactorizado = irnum_nuevosaldofactorizado;
    }

    public String getIrnum_nuevosaldo() {
        return irnum_nuevosaldo;
    }

    public void setIrnum_nuevosaldo(String irnum_nuevosaldo) {
        this.irnum_nuevosaldo = irnum_nuevosaldo;
    }

    public String getIrburo_scoresc01() {
        return irburo_scoresc01;
    }

    public void setIrburo_scoresc01(String irburo_scoresc01) {
        this.irburo_scoresc01 = irburo_scoresc01;
    }

    public String getIrnum_puntosscore01() {
        return irnum_puntosscore01;
    }

    public void setIrnum_puntosscore01(String irnum_puntosscore01) {
        this.irnum_puntosscore01 = irnum_puntosscore01;
    }

    public String getIrburo_limcreditors22() {
        return irburo_limcreditors22;
    }

    public void setIrburo_limcreditors22(String irburo_limcreditors22) {
        this.irburo_limcreditors22 = irburo_limcreditors22;
    }

    public String getIrnum_puntoslimcreditors22() {
        return irnum_puntoslimcreditors22;
    }

    public void setIrnum_puntoslimcreditors22(String irnum_puntoslimcreditors22) {
        this.irnum_puntoslimcreditors22 = irnum_puntoslimcreditors22;
    }

    public String getIrburo_prccreditors26() {
        return irburo_prccreditors26;
    }

    public void setIrburo_prccreditors26(String irburo_prccreditors26) {
        this.irburo_prccreditors26 = irburo_prccreditors26;
    }

    public String getIrnum_puntosprccreditors26() {
        return irnum_puntosprccreditors26;
    }

    public void setIrnum_puntosprccreditors26(String irnum_puntosprccreditors26) {
        this.irnum_puntosprccreditors26 = irnum_puntosprccreditors26;
    }

    public String getIrburo_cuentasmorosidadrs13() {
        return irburo_cuentasmorosidadrs13;
    }

    public void setIrburo_cuentasmorosidadrs13(String irburo_cuentasmorosidadrs13) {
        this.irburo_cuentasmorosidadrs13 = irburo_cuentasmorosidadrs13;
    }

    public String getIrburo_totalvencidosrs24() {
        return irburo_totalvencidosrs24;
    }

    public void setIrburo_totalvencidosrs24(String irburo_totalvencidosrs24) {
        this.irburo_totalvencidosrs24 = irburo_totalvencidosrs24;
    }

    public String getIrburo_idtramautilizada() {
        return irburo_idtramautilizada;
    }

    public void setIrburo_idtramautilizada(String irburo_idtramautilizada) {
        this.irburo_idtramautilizada = irburo_idtramautilizada;
    }

    public String getIrburo_fecconsultatramautilizada() {
        return drburo_fecconsultatramautilizada;
    }

    public void setIrburo_fecconsultatramautilizada(String irburo_fecconsultatramautilizada) {
        this.drburo_fecconsultatramautilizada = irburo_fecconsultatramautilizada;
    }

    public String getRespagoinicialpropuesto12() {
        return respagoinicialpropuesto12;
    }

    public String getIresporcentajepicmareal() {
        return iresporcentajepicmareal;
    }

    public void setIresporcentajepicmareal(String iresporcentajepicmareal) {
        this.iresporcentajepicmareal = iresporcentajepicmareal;
    }

    public String getIresabononuevacomprasimulado() {
        return iresabononuevacomprasimulado;
    }

    public void setIresabononuevacomprasimulado(String iresabononuevacomprasimulado) {
        this.iresabononuevacomprasimulado = iresabononuevacomprasimulado;
    }

    public String getIresporcpicmafinalminimofinal() {
        return iresporcpicmafinalminimofinal;
    }

    public void setIresporcpicmafinalminimofinal(String iresporcpicmafinalminimofinal) {
        this.iresporcpicmafinalminimofinal = iresporcpicmafinalminimofinal;
    }

    public String getIresporcpicmafinalmaximofinal() {
        return iresporcpicmafinalmaximofinal;
    }

    public void setIresporcpicmafinalmaximofinal(String iresporcpicmafinalmaximofinal) {
        this.iresporcpicmafinalmaximofinal = iresporcpicmafinalmaximofinal;
    }

    public String getIrespicmasimulado() {
        return irespicmasimulado;
    }

    public void setIrespicmasimulado(String irespicmasimulado) {
        this.irespicmasimulado = irespicmasimulado;
    }

    public String getIresporcentajepicmasimulado() {
        return iresporcentajepicmasimulado;
    }

    public void setIresporcentajepicmasimulado(String iresporcentajepicmasimulado) {
        this.iresporcentajepicmasimulado = iresporcentajepicmasimulado;
    }

    public String getIexcesotopemax() {
        return iexcesotopemax;
    }

    public void setIexcesotopemax(String iexcesotopemax) {
        this.iexcesotopemax = iexcesotopemax;
    }

    public String getIsumapuntosvariables() {
        return isumapuntosvariables;
    }

    public void setIsumapuntosvariables(String isumapuntosvariables) {
        this.isumapuntosvariables = isumapuntosvariables;
    }

    public String getVipuntosporcentajesaturacion() {
        return vipuntosporcentajesaturacion;
    }

    public void setVipuntosporcentajesaturacion(String vipuntosporcentajesaturacion) {
        this.vipuntosporcentajesaturacion = vipuntosporcentajesaturacion;
    }

    public PlazoPagoInicial getPlazospagoinicial() {
        return plazospagoinicial;
    }

    public void setPlazospagoinicial(PlazoPagoInicial plazospagoinicial) {
        this.plazospagoinicial = plazospagoinicial;
    }

    public String getRespagoinicialminimo12() {
        return respagoinicialminimo12;
    }

    public String getRespagoinicialpropuesto18() {
        return respagoinicialpropuesto18;
    }

    public String getRespagoinicialminimo18() {
        return respagoinicialminimo18;
    }

    public String getRespagoinicialpropuesto24() {
        return respagoinicialpropuesto24;
    }

    public String getRespagoinicialminimo24() {
        return respagoinicialminimo24;
    }

    public String getRespagoinicialpropuesto30() {
        return respagoinicialpropuesto30;
    }

    public void setRespagoinicialpropuesto30(String respagoinicialpropuesto30) {
        this.respagoinicialpropuesto30 = respagoinicialpropuesto30;
    }

    public String getRespagoinicialminimo30() {
        return respagoinicialminimo30;
    }

    public void setRespagoinicialminimo30(String respagoinicialminimo30) {
        this.respagoinicialminimo30 = respagoinicialminimo30;
    }

    public String getRespagoinicialpropuesto36() {
        return respagoinicialpropuesto36;
    }

    public void setRespagoinicialpropuesto36(String respagoinicialpropuesto36) {
        this.respagoinicialpropuesto36 = respagoinicialpropuesto36;
    }

    public String getRespagoinicialminimo36() {
        return respagoinicialminimo36;
    }

    public void setRespagoinicialminimo36(String respagoinicialminimo36) {
        this.respagoinicialminimo36 = respagoinicialminimo36;
    }

    public String getRespuntajefinal12() {
        return respuntajefinal12;
    }

    public String getRespuntajefinal18() {
        return respuntajefinal18;
    }

    public String getRespuntajefinal24() {
        return respuntajefinal24;
    }

    public String getResnuevosaldo() {
        return resnuevosaldo;
    }

    public String getRestipopagoinicial() {
        return restipopagoinicial;
    }

    public String getResporcentajepagoinicial12() {
        return resporcentajepagoinicial12;
    }

    public String getResporcentajepagominimo12() {
        return resporcentajepagominimo12;
    }

    public String getResporcentajepagoinicial18() {
        return resporcentajepagoinicial18;
    }

    public String getResporcentajepagominimo18() {
        return resporcentajepagominimo18;
    }

    public String getResporcentajepagoinicial24() {
        return resporcentajepagoinicial24;
    }

    public String getResporcentajepagominimo24() {
        return resporcentajepagominimo24;
    }

    public String getResporcentajepagoinicial30() {
        return resporcentajepagoinicial30;
    }

    public void setResporcentajepagoinicial30(String resporcentajepagoinicial30) {
        this.resporcentajepagoinicial30 = resporcentajepagoinicial30;
    }

    public String getResporcentajepagominimo30() {
        return resporcentajepagominimo30;
    }

    public void setResporcentajepagominimo30(String resporcentajepagominimo30) {
        this.resporcentajepagominimo30 = resporcentajepagominimo30;
    }

    public String getResporcentajepagoinicial36() {
        return resporcentajepagoinicial36;
    }

    public void setResporcentajepagoinicial36(String resporcentajepagoinicial36) {
        this.resporcentajepagoinicial36 = resporcentajepagoinicial36;
    }

    public String getResporcentajepagominimo36() {
        return resporcentajepagominimo36;
    }

    public void setResporcentajepagominimo36(String resporcentajepagominimo36) {
        this.resporcentajepagominimo36 = resporcentajepagominimo36;
    }

    public String getItotalfactura() {
        return itotalfactura;
    }

    public String getVpagoinicial() {
        return vpagoinicial;
    }

    public String getVpagoinicialminimo() {
        return vpagoinicialminimo;
    }

    public String getVporcentajepagofinal() {
        return vporcentajepagofinal;
    }

    public String getViflagprimeracompra() {
        return viflagprimeracompra;
    }

    public String getVipuntajeprimeracompra() {
        return vipuntajeprimeracompra;
    }

    public String getVporcentajesaturacionfinal() {
        return vporcentajesaturacionfinal;
    }

    public String getVipuntosporcentajesaturacionfinal() {
        return vipuntosporcentajesaturacionfinal;
    }

    public String getViporcpagosdocemesesvsnvosaldo() {
        return viporcpagosdocemesesvsnvosaldo;
    }

    public String getVipuntospagosdocemesesvsnvosaldo() {
        return vipuntospagosdocemesesvsnvosaldo;
    }

    public String getVipuntosdepartamento() {
        return vipuntosdepartamento;
    }

    public String getVimesesantiguedadcte() {
        return vimesesantiguedadcte;
    }

    public String getVipuntosantiguedadcte() {
        return vipuntosantiguedadcte;
    }

    public String getVipuntajeinicial() {
        return vipuntajeinicial;
    }

    public String getVexceso() {
        return vexceso;
    }

    public String getIpuntajealtoriesgo() {
        return ipuntajealtoriesgo;
    }

    public String getInum_puntosparametricosclientesnuevos() {
        return inum_puntosparametricosclientesnuevos;
    }

    public String getInumpuntosvariablemodelotiendafinal() {
        return inumpuntosvariablemodelotiendafinal;
    }

    public String getRespuntajefinal() {
        return respuntajefinal;
    }

    public String getFfactorlineafinal() {
        return ffactorlineafinal;
    }

    public String getIflagcalculoparametrico() {
        return iflagcalculoparametrico;
    }

    public String getIporcentajesaturacion() {
        return iporcentajesaturacion;
    }

    public String getIporcentajesaturacion18() {
        return iporcentajesaturacion18;
    }

    public String getIporcentajesaturacion24() {
        return iporcentajesaturacion24;
    }

    public String getLmargencreditocliente() {
        return lmargencreditocliente;
    }

    public String getLexcesocredito() {
        return lexcesocredito;
    }

    public String getFfactorlineanormal() {
        return ffactorlineanormal;
    }

    public String getFfactorlineaespecial() {
        return ffactorlineaespecial;
    }

    public String getFfactorlineabasica() {
        return ffactorlineabasica;
    }

    public String getFfactortopemaximo() {
        return ffactortopemaximo;
    }

    public String getIporcentajesaturaciontopemaximo() {
        return iporcentajesaturaciontopemaximo;
    }

    public String getIporcentajesaturaciontopemaximo18() {
        return iporcentajesaturaciontopemaximo18;
    }

    public String getIporcentajesaturaciontopemaximo24() {
        return iporcentajesaturaciontopemaximo24;
    }

    public String getIporcentajesaturaciontopemaximofinal() {
        return iporcentajesaturaciontopemaximofinal;
    }

    public String getIvsmcduranteventa() {
        return ivsmcduranteventa;
    }

    public String getIflagtopefactura() {
        return iflagtopefactura;
    }

    public String getImontomaximofactura() {
        return imontomaximofactura;
    }

    public String getIexcesotopemaxfinal() {
        return iexcesotopemaxfinal;
    }

    public String getIflagpagocompleto() {
        return iflagpagocompleto;
    }

    public String getIpagocomparativotope() {
        return ipagocomparativotope;
    }

    public String getIrescma() {
        return irescma;
    }

    public String getIresfactorcmafinal() {
        return iresfactorcmafinal;
    }

    public String getIresabononuevacomprasimuladofinal() {
        return iresabononuevacomprasimuladofinal;
    }

    public String getIresporcentajepicmasimuladofinal() {
        return iresporcentajepicmasimuladofinal;
    }

    public String getIrespicmasimuladofinal() {
        return irespicmasimuladofinal;
    }

    public String getIresporcentajepicmarealfinal() {
        return iresporcentajepicmarealfinal;
    }

    public String getIresabonodisponibledelcliente() {
        return iresabonodisponibledelcliente;
    }

    public String getIresventatotalcreditocompleta() {
        return iresventatotalcreditocompleta;
    }

    public String getIresabonodisponiblesimulado() {
        return iresabonodisponiblesimulado;
    }

    public String getIresporclimitesuperior() {
        return iresporclimitesuperior;
    }

    public String getIresporcsaturacioncmafinal() {
        return iresporcsaturacioncmafinal;
    }

    public String getIresabonobasedespcomprafinal() {
        return iresabonobasedespcomprafinal;
    }

    public String getIresporccmafinalmaximofinal() {
        return iresporccmafinalmaximofinal;
    }

    public String getIresporccmafinalminimofinal() {
        return iresporccmafinalminimofinal;
    }

    public String getIrespicmafinalmaximofinal() {
        return irespicmafinalmaximofinal;
    }

    public String getIrespicmafinalminimofinal() {
        return irespicmafinalminimofinal;
    }

    public String getIrespioperacionmaximofinal() {
        return irespioperacionmaximofinal;
    }

    public String getIrespioperacionminimofinal() {
        return irespioperacionminimofinal;
    }

    public String getIrespicmarealfinal() {
        return irespicmarealfinal;
    }

    public String getIresconstanteabonobasemuebles() {
        return iresconstanteabonobasemuebles;
    }

    public String getIressobrepreciosimuladofinal() {
        return iressobrepreciosimuladofinal;
    }

    public String getIresflaglineacreditofinal() {
        return iresflaglineacreditofinal;
    }

    public String getIresplazosalidapi() {
        return iresplazosalidapi;
    }

    // Setter Methods

    public void setRespagoinicialpropuesto12(String respagoinicialpropuesto12) {
        this.respagoinicialpropuesto12 = respagoinicialpropuesto12;
    }

    public void setRespagoinicialminimo12(String respagoinicialminimo12) {
        this.respagoinicialminimo12 = respagoinicialminimo12;
    }

    public void setRespagoinicialpropuesto18(String respagoinicialpropuesto18) {
        this.respagoinicialpropuesto18 = respagoinicialpropuesto18;
    }

    public void setRespagoinicialminimo18(String respagoinicialminimo18) {
        this.respagoinicialminimo18 = respagoinicialminimo18;
    }

    public void setRespagoinicialpropuesto24(String respagoinicialpropuesto24) {
        this.respagoinicialpropuesto24 = respagoinicialpropuesto24;
    }

    public void setRespagoinicialminimo24(String respagoinicialminimo24) {
        this.respagoinicialminimo24 = respagoinicialminimo24;
    }

    public void setRespuntajefinal12(String respuntajefinal12) {
        this.respuntajefinal12 = respuntajefinal12;
    }

    public void setRespuntajefinal18(String respuntajefinal18) {
        this.respuntajefinal18 = respuntajefinal18;
    }

    public void setRespuntajefinal24(String respuntajefinal24) {
        this.respuntajefinal24 = respuntajefinal24;
    }

    public void setResnuevosaldo(String resnuevosaldo) {
        this.resnuevosaldo = resnuevosaldo;
    }

    public void setRestipopagoinicial(String restipopagoinicial) {
        this.restipopagoinicial = restipopagoinicial;
    }

    public void setResporcentajepagoinicial12(String resporcentajepagoinicial12) {
        this.resporcentajepagoinicial12 = resporcentajepagoinicial12;
    }

    public void setResporcentajepagominimo12(String resporcentajepagominimo12) {
        this.resporcentajepagominimo12 = resporcentajepagominimo12;
    }

    public void setResporcentajepagoinicial18(String resporcentajepagoinicial18) {
        this.resporcentajepagoinicial18 = resporcentajepagoinicial18;
    }

    public void setResporcentajepagominimo18(String resporcentajepagominimo18) {
        this.resporcentajepagominimo18 = resporcentajepagominimo18;
    }

    public void setResporcentajepagoinicial24(String resporcentajepagoinicial24) {
        this.resporcentajepagoinicial24 = resporcentajepagoinicial24;
    }

    public void setResporcentajepagominimo24(String resporcentajepagominimo24) {
        this.resporcentajepagominimo24 = resporcentajepagominimo24;
    }

    public void setItotalfactura(String itotalfactura) {
        this.itotalfactura = itotalfactura;
    }

    public void setVpagoinicial(String vpagoinicial) {
        this.vpagoinicial = vpagoinicial;
    }

    public void setVpagoinicialminimo(String vpagoinicialminimo) {
        this.vpagoinicialminimo = vpagoinicialminimo;
    }

    public void setVporcentajepagofinal(String vporcentajepagofinal) {
        this.vporcentajepagofinal = vporcentajepagofinal;
    }

    public void setViflagprimeracompra(String viflagprimeracompra) {
        this.viflagprimeracompra = viflagprimeracompra;
    }

    public void setVipuntajeprimeracompra(String vipuntajeprimeracompra) {
        this.vipuntajeprimeracompra = vipuntajeprimeracompra;
    }

    public void setVporcentajesaturacionfinal(String vporcentajesaturacionfinal) {
        this.vporcentajesaturacionfinal = vporcentajesaturacionfinal;
    }

    public void setVipuntosporcentajesaturacionfinal(String vipuntosporcentajesaturacionfinal) {
        this.vipuntosporcentajesaturacionfinal = vipuntosporcentajesaturacionfinal;
    }

    public void setViporcpagosdocemesesvsnvosaldo(String viporcpagosdocemesesvsnvosaldo) {
        this.viporcpagosdocemesesvsnvosaldo = viporcpagosdocemesesvsnvosaldo;
    }

    public void setVipuntospagosdocemesesvsnvosaldo(String vipuntospagosdocemesesvsnvosaldo) {
        this.vipuntospagosdocemesesvsnvosaldo = vipuntospagosdocemesesvsnvosaldo;
    }

    public void setVipuntosdepartamento(String vipuntosdepartamento) {
        this.vipuntosdepartamento = vipuntosdepartamento;
    }

    public void setVimesesantiguedadcte(String vimesesantiguedadcte) {
        this.vimesesantiguedadcte = vimesesantiguedadcte;
    }

    public void setVipuntosantiguedadcte(String vipuntosantiguedadcte) {
        this.vipuntosantiguedadcte = vipuntosantiguedadcte;
    }

    public void setVipuntajeinicial(String vipuntajeinicial) {
        this.vipuntajeinicial = vipuntajeinicial;
    }

    public void setVexceso(String vexceso) {
        this.vexceso = vexceso;
    }

    public void setIpuntajealtoriesgo(String ipuntajealtoriesgo) {
        this.ipuntajealtoriesgo = ipuntajealtoriesgo;
    }

    public void setInum_puntosparametricosclientesnuevos(String inum_puntosparametricosclientesnuevos) {
        this.inum_puntosparametricosclientesnuevos = inum_puntosparametricosclientesnuevos;
    }

    public void setInumpuntosvariablemodelotiendafinal(String inumpuntosvariablemodelotiendafinal) {
        this.inumpuntosvariablemodelotiendafinal = inumpuntosvariablemodelotiendafinal;
    }

    public void setRespuntajefinal(String respuntajefinal) {
        this.respuntajefinal = respuntajefinal;
    }

    public void setFfactorlineafinal(String ffactorlineafinal) {
        this.ffactorlineafinal = ffactorlineafinal;
    }

    public void setIflagcalculoparametrico(String iflagcalculoparametrico) {
        this.iflagcalculoparametrico = iflagcalculoparametrico;
    }

    public void setIporcentajesaturacion(String iporcentajesaturacion) {
        this.iporcentajesaturacion = iporcentajesaturacion;
    }

    public void setIporcentajesaturacion18(String iporcentajesaturacion18) {
        this.iporcentajesaturacion18 = iporcentajesaturacion18;
    }

    public void setIporcentajesaturacion24(String iporcentajesaturacion24) {
        this.iporcentajesaturacion24 = iporcentajesaturacion24;
    }

    public void setLmargencreditocliente(String lmargencreditocliente) {
        this.lmargencreditocliente = lmargencreditocliente;
    }

    public void setLexcesocredito(String lexcesocredito) {
        this.lexcesocredito = lexcesocredito;
    }

    public void setFfactorlineanormal(String ffactorlineanormal) {
        this.ffactorlineanormal = ffactorlineanormal;
    }

    public void setFfactorlineaespecial(String ffactorlineaespecial) {
        this.ffactorlineaespecial = ffactorlineaespecial;
    }

    public void setFfactorlineabasica(String ffactorlineabasica) {
        this.ffactorlineabasica = ffactorlineabasica;
    }

    public void setFfactortopemaximo(String ffactortopemaximo) {
        this.ffactortopemaximo = ffactortopemaximo;
    }

    public void setIporcentajesaturaciontopemaximo(String iporcentajesaturaciontopemaximo) {
        this.iporcentajesaturaciontopemaximo = iporcentajesaturaciontopemaximo;
    }

    public void setIporcentajesaturaciontopemaximo18(String iporcentajesaturaciontopemaximo18) {
        this.iporcentajesaturaciontopemaximo18 = iporcentajesaturaciontopemaximo18;
    }

    public void setIporcentajesaturaciontopemaximo24(String iporcentajesaturaciontopemaximo24) {
        this.iporcentajesaturaciontopemaximo24 = iporcentajesaturaciontopemaximo24;
    }

    public void setIporcentajesaturaciontopemaximofinal(String iporcentajesaturaciontopemaximofinal) {
        this.iporcentajesaturaciontopemaximofinal = iporcentajesaturaciontopemaximofinal;
    }

    public void setIvsmcduranteventa(String ivsmcduranteventa) {
        this.ivsmcduranteventa = ivsmcduranteventa;
    }

    public void setIflagtopefactura(String iflagtopefactura) {
        this.iflagtopefactura = iflagtopefactura;
    }

    public void setImontomaximofactura(String imontomaximofactura) {
        this.imontomaximofactura = imontomaximofactura;
    }

    public void setIexcesotopemaxfinal(String iexcesotopemaxfinal) {
        this.iexcesotopemaxfinal = iexcesotopemaxfinal;
    }

    public void setIflagpagocompleto(String iflagpagocompleto) {
        this.iflagpagocompleto = iflagpagocompleto;
    }

    public void setIpagocomparativotope(String ipagocomparativotope) {
        this.ipagocomparativotope = ipagocomparativotope;
    }

    public void setIrescma(String irescma) {
        this.irescma = irescma;
    }

    public void setIresfactorcmafinal(String iresfactorcmafinal) {
        this.iresfactorcmafinal = iresfactorcmafinal;
    }

    public void setIresabononuevacomprasimuladofinal(String iresabononuevacomprasimuladofinal) {
        this.iresabononuevacomprasimuladofinal = iresabononuevacomprasimuladofinal;
    }

    public void setIresporcentajepicmasimuladofinal(String iresporcentajepicmasimuladofinal) {
        this.iresporcentajepicmasimuladofinal = iresporcentajepicmasimuladofinal;
    }

    public void setIrespicmasimuladofinal(String irespicmasimuladofinal) {
        this.irespicmasimuladofinal = irespicmasimuladofinal;
    }

    public void setIresporcentajepicmarealfinal(String iresporcentajepicmarealfinal) {
        this.iresporcentajepicmarealfinal = iresporcentajepicmarealfinal;
    }

    public void setIresabonodisponibledelcliente(String iresabonodisponibledelcliente) {
        this.iresabonodisponibledelcliente = iresabonodisponibledelcliente;
    }

    public void setIresventatotalcreditocompleta(String iresventatotalcreditocompleta) {
        this.iresventatotalcreditocompleta = iresventatotalcreditocompleta;
    }

    public void setIresabonodisponiblesimulado(String iresabonodisponiblesimulado) {
        this.iresabonodisponiblesimulado = iresabonodisponiblesimulado;
    }

    public void setIresporclimitesuperior(String iresporclimitesuperior) {
        this.iresporclimitesuperior = iresporclimitesuperior;
    }

    public void setIresporcsaturacioncmafinal(String iresporcsaturacioncmafinal) {
        this.iresporcsaturacioncmafinal = iresporcsaturacioncmafinal;
    }

    public void setIresabonobasedespcomprafinal(String iresabonobasedespcomprafinal) {
        this.iresabonobasedespcomprafinal = iresabonobasedespcomprafinal;
    }

    public void setIresporccmafinalmaximofinal(String iresporccmafinalmaximofinal) {
        this.iresporccmafinalmaximofinal = iresporccmafinalmaximofinal;
    }

    public void setIresporccmafinalminimofinal(String iresporccmafinalminimofinal) {
        this.iresporccmafinalminimofinal = iresporccmafinalminimofinal;
    }

    public void setIrespicmafinalmaximofinal(String irespicmafinalmaximofinal) {
        this.irespicmafinalmaximofinal = irespicmafinalmaximofinal;
    }

    public void setIrespicmafinalminimofinal(String irespicmafinalminimofinal) {
        this.irespicmafinalminimofinal = irespicmafinalminimofinal;
    }

    public void setIrespioperacionmaximofinal(String irespioperacionmaximofinal) {
        this.irespioperacionmaximofinal = irespioperacionmaximofinal;
    }

    public void setIrespioperacionminimofinal(String irespioperacionminimofinal) {
        this.irespioperacionminimofinal = irespioperacionminimofinal;
    }

    public void setIrespicmarealfinal(String irespicmarealfinal) {
        this.irespicmarealfinal = irespicmarealfinal;
    }

    public void setIresconstanteabonobasemuebles(String iresconstanteabonobasemuebles) {
        this.iresconstanteabonobasemuebles = iresconstanteabonobasemuebles;
    }

    public void setIressobrepreciosimuladofinal(String iressobrepreciosimuladofinal) {
        this.iressobrepreciosimuladofinal = iressobrepreciosimuladofinal;
    }

    public void setIresflaglineacreditofinal(String iresflaglineacreditofinal) {
        this.iresflaglineacreditofinal = iresflaglineacreditofinal;
    }

    public void setIresplazosalidapi(String iresplazosalidapi) {
        this.iresplazosalidapi = iresplazosalidapi;
    }

    @Override
    public String toString() {
        return "RowMuebles [respagoinicialpropuesto12=" + respagoinicialpropuesto12 + ", respagoinicialminimo12="
                + respagoinicialminimo12 + ", respagoinicialpropuesto18=" + respagoinicialpropuesto18
                + ", respagoinicialminimo18=" + respagoinicialminimo18 + ", respagoinicialpropuesto24="
                + respagoinicialpropuesto24 + ", respagoinicialminimo24=" + respagoinicialminimo24
                + ", respagoinicialpropuesto30=" + respagoinicialpropuesto30 + ", respagoinicialminimo30="
                + respagoinicialminimo30 + ", respagoinicialpropuesto36=" + respagoinicialpropuesto36
                + ", respagoinicialminimo36=" + respagoinicialminimo36 + ", respuntajefinal12=" + respuntajefinal12
                + ", respuntajefinal18=" + respuntajefinal18 + ", respuntajefinal24=" + respuntajefinal24
                + ", resnuevosaldo=" + resnuevosaldo + ", restipopagoinicial=" + restipopagoinicial
                + ", resporcentajepagoinicial12=" + resporcentajepagoinicial12 + ", resporcentajepagominimo12="
                + resporcentajepagominimo12 + ", resporcentajepagoinicial18=" + resporcentajepagoinicial18
                + ", resporcentajepagominimo18=" + resporcentajepagominimo18 + ", resporcentajepagoinicial24="
                + resporcentajepagoinicial24 + ", resporcentajepagominimo24=" + resporcentajepagominimo24
                + ", resporcentajepagoinicial30=" + resporcentajepagoinicial30 + ", resporcentajepagominimo30="
                + resporcentajepagominimo30 + ", resporcentajepagoinicial36=" + resporcentajepagoinicial36
                + ", resporcentajepagominimo36=" + resporcentajepagominimo36 + ", itotalfactura=" + itotalfactura
                + ", vpagoinicial=" + vpagoinicial + ", vpagoinicialminimo=" + vpagoinicialminimo
                + ", vporcentajepagofinal=" + vporcentajepagofinal + ", viflagprimeracompra=" + viflagprimeracompra
                + ", vipuntajeprimeracompra=" + vipuntajeprimeracompra + ", vporcentajesaturacionfinal="
                + vporcentajesaturacionfinal + ", vipuntosporcentajesaturacion=" + vipuntosporcentajesaturacion
                + ", vipuntosporcentajesaturacionfinal=" + vipuntosporcentajesaturacionfinal
                + ", viporcpagosdocemesesvsnvosaldo=" + viporcpagosdocemesesvsnvosaldo
                + ", vipuntospagosdocemesesvsnvosaldo=" + vipuntospagosdocemesesvsnvosaldo + ", vipuntosdepartamento="
                + vipuntosdepartamento + ", vimesesantiguedadcte=" + vimesesantiguedadcte + ", vipuntosantiguedadcte="
                + vipuntosantiguedadcte + ", vipuntajeinicial=" + vipuntajeinicial + ", vexceso=" + vexceso
                + ", ipuntajealtoriesgo=" + ipuntajealtoriesgo + ", inum_puntosparametricosclientesnuevos="
                + inum_puntosparametricosclientesnuevos + ", isumapuntosvariables=" + isumapuntosvariables
                + ", inumpuntosvariablemodelotiendafinal=" + inumpuntosvariablemodelotiendafinal + ", respuntajefinal="
                + respuntajefinal + ", ffactorlineafinal=" + ffactorlineafinal + ", iflagcalculoparametrico="
                + iflagcalculoparametrico + ", iporcentajesaturacion=" + iporcentajesaturacion
                + ", iporcentajesaturacion18=" + iporcentajesaturacion18 + ", iporcentajesaturacion24="
                + iporcentajesaturacion24 + ", lmargencreditocliente=" + lmargencreditocliente + ", lexcesocredito="
                + lexcesocredito + ", ffactorlineanormal=" + ffactorlineanormal + ", ffactorlineaespecial="
                + ffactorlineaespecial + ", ffactorlineabasica=" + ffactorlineabasica + ", ffactortopemaximo="
                + ffactortopemaximo + ", iporcentajesaturaciontopemaximo=" + iporcentajesaturaciontopemaximo
                + ", iporcentajesaturaciontopemaximo18=" + iporcentajesaturaciontopemaximo18
                + ", iporcentajesaturaciontopemaximo24=" + iporcentajesaturaciontopemaximo24
                + ", iporcentajesaturaciontopemaximofinal=" + iporcentajesaturaciontopemaximofinal + ", ivsmcduranteventa="
                + ivsmcduranteventa + ", iflagtopefactura=" + iflagtopefactura + ", imontomaximofactura="
                + imontomaximofactura + ", iexcesotopemax=" + iexcesotopemax + ", iexcesotopemaxfinal="
                + iexcesotopemaxfinal + ", iflagpagocompleto=" + iflagpagocompleto + ", ipagocomparativotope="
                + ipagocomparativotope + ", irescma=" + irescma + ", iresfactorcmafinal=" + iresfactorcmafinal
                + ", iresabononuevacomprasimulado=" + iresabononuevacomprasimulado + ", iresabononuevacomprasimuladofinal="
                + iresabononuevacomprasimuladofinal + ", iresporcentajepicmasimulado=" + iresporcentajepicmasimulado
                + ", iresporcentajepicmasimuladofinal=" + iresporcentajepicmasimuladofinal + ", irespicmasimulado="
                + irespicmasimulado + ", irespicmasimuladofinal=" + irespicmasimuladofinal + ", iresporcentajepicmareal="
                + iresporcentajepicmareal + ", iresporcentajepicmarealfinal=" + iresporcentajepicmarealfinal
                + ", iresabonodisponibledelcliente=" + iresabonodisponibledelcliente + ", iresventatotalcreditocompleta="
                + iresventatotalcreditocompleta + ", iresabonodisponiblesimulado=" + iresabonodisponiblesimulado
                + ", iresporclimitesuperior=" + iresporclimitesuperior + ", iresporcsaturacioncmafinal="
                + iresporcsaturacioncmafinal + ", iresabonobasedespcomprafinal=" + iresabonobasedespcomprafinal
                + ", iresporcpicmafinalmaximofinal=" + iresporcpicmafinalmaximofinal + ", iresporccmafinalmaximofinal="
                + iresporccmafinalmaximofinal + ", iresporcpicmafinalminimofinal=" + iresporcpicmafinalminimofinal
                + ", iresporccmafinalminimofinal=" + iresporccmafinalminimofinal + ", irespicmafinalmaximofinal="
                + irespicmafinalmaximofinal + ", irespicmafinalminimofinal=" + irespicmafinalminimofinal
                + ", irespioperacionmaximofinal=" + irespioperacionmaximofinal + ", irespioperacionminimofinal="
                + irespioperacionminimofinal + ", irespicmarealfinal=" + irespicmarealfinal
                + ", iresconstanteabonobasemuebles=" + iresconstanteabonobasemuebles + ", iressobrepreciosimuladofinal="
                + iressobrepreciosimuladofinal + ", iresflaglineacreditofinal=" + iresflaglineacreditofinal
                + ", iresplazosalidapi=" + iresplazosalidapi + ", plazospagoinicial=" + plazospagoinicial
                + ", crlv_clientehit=" + crlv_clientehit + ", irnum_cuentasactivas=" + irnum_cuentasactivas
                + ", irnum_cuentassaldadasperiodo=" + irnum_cuentassaldadasperiodo + ", irnum_puntoscuentasactivas="
                + irnum_puntoscuentasactivas + ", irnum_puntoscuentasactivasysaldadas="
                + irnum_puntoscuentasactivasysaldadas + ", irnum_puntoscuentassaldadas=" + irnum_puntoscuentassaldadas
                + ", irmp_pago12meses=" + irmp_pago12meses + ", irnum_estado=" + irnum_estado + ", irnum_puntosestado="
                + irnum_puntosestado + ", crlv_categoria=" + crlv_categoria + ", irnum_puntoscategoria="
                + irnum_puntoscategoria + ", irnum_diasprimeracompra=" + irnum_diasprimeracompra
                + ", irnum_puntosdiasprimeracompra=" + irnum_puntosdiasprimeracompra + ", irnum_nuevosaldofactorizado="
                + irnum_nuevosaldofactorizado + ", irnum_nuevosaldo=" + irnum_nuevosaldo + ", irburo_scoresc01="
                + irburo_scoresc01 + ", irnum_puntosscore01=" + irnum_puntosscore01 + ", irburo_limcreditors22="
                + irburo_limcreditors22 + ", irnum_puntoslimcreditors22=" + irnum_puntoslimcreditors22
                + ", irburo_prccreditors26=" + irburo_prccreditors26 + ", irnum_puntosprccreditors26="
                + irnum_puntosprccreditors26 + ", irburo_cuentasmorosidadrs13=" + irburo_cuentasmorosidadrs13
                + ", irburo_totalvencidosrs24=" + irburo_totalvencidosrs24 + ", irburo_idtramautilizada="
                + irburo_idtramautilizada + ", drburo_fecconsultatramautilizada=" + drburo_fecconsultatramautilizada
                + ", irnum_puntossaldodespuesdelaventa=" + irnum_puntossaldodespuesdelaventa
                + ", irnum_puntostotalvencidosrs24ymorosidadrs13=" + irnum_puntostotalvencidosrs24ymorosidadrs13 + "]";
    }


}
