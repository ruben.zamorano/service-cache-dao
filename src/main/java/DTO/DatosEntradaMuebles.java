package DTO;

import Base.GlobalEnum;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Clase que representa el objeto de entrada de Muebles del C?lculo Pago Inicial
 * General
 *
 * @author Ramses Santos
 *
 */

@XmlRootElement(name = "DatosEntrada", namespace = "")
@XmlAccessorType(XmlAccessType.FIELD)
public class DatosEntradaMuebles {
    private String iPuntosParametricoArtAltoRiesgo;
    private String cPuntualidadCte;
    private String lPagoDineroE;
    private String iEnganchePropuestoClienteS50;
    private String iFlagEsClienteS50;
    private String iFlagLineaCreditoEspecial;
    private String iPagosUltimosDoceMeses;
    private String iSaldoTotalCteSinAjuste;
    private String dFechaAltaCte;
    private String iNuevoPrePuntajeAltoRiesgo;
    private String iCreditoInicial;
    private String lCliente;
    private String iFlagRecalibracion;
    private String dFechaPrimeraCompra;
    private String iSaldoCteFactorizado;
    private String iLineaCreditoRealPesos;
    private String iFlagClienteCreditoInicial;
    private String iFlag18pi12;
    private String iTotalContadoNvaCompraSinAjuste;
    private String dFechaTienda;
    private String iVtaAccesoriosCel;
    private String iNumDepartamento;
    private String iEngancheIlustrativo;
    private String iNum_PuntosParametricosCN;
    private String iNum_PuntosParametricosCNAux;
    private String cFactorCliente12;
    private String cFactorCliente18;
    private String cFactorCliente24;
    private String cFactorClienteEsp12;
    private String cFactorClienteEsp18;
    private String cFactorClienteEsp24;
    private String iFlagParametricoCN;
    private String iFlagParamCNParamVentas;
    private String SListaSku;
    private String SListaDcf;
    private String iTienda;
    private String iCaja;
    private String iFolio;
    private String iImporte;
    private String cImporteArt;
    private String iSalarioMinimoCoppel;
    private String iFlagVentaContado;
    private String iSumaAbonosBase;
    private String iSobrePrecioSimulado12;
    private String iSobrePrecioSimulado18;
    private String iSobrePrecioSimulado24;
    private String fFactorCMA;
    private String iSaldaCon;
    private String iFlagCMA;
    private String iFlagVentaAsistida;
    private String iTotalContadoRopa;
    private String cPlazoVenta;
    private String cMesesVenta;
    private String cFactorCliente;
    private String cFactorClienteEsp;
    private String cFactorClienteBasico;
    private String sSobrePrecioSimulado;
    private String icontadoplanlealtad;
    private String iFlagSanaTuDeuda;
    private String iOpcionSanaTuDeuda;
    private String iPrcPagoUltimaCompra;
    private String inum_infoburocredito;
    private String iclv_clientehit;
    private String inum_cuentasactivas;
    private String inum_cuentassaldadasperiodo;
    private String iclv_categoria;
    private String inum_estado;
    private String inum_valorscore;
    private String inum_rslimitecreditocuentasactivas;
    private String inum_rsporcentajelimitecreditocuentasactivas;
    private String inum_rstotalcuentasmopactual;
    private String inum_rssaldovencidocreditocuentasactivas;
    private String dfecconsulta;



    // Getter Methods

    public String getIclv_clientehit() {
        return iclv_clientehit;
    }

    public String getInum_estado() {
        return inum_estado;
    }

    public void setInum_estado(String inum_estado) {
        this.inum_estado = inum_estado;
    }

    public void setIclv_clientehit(String iclv_clientehit) {
        this.iclv_clientehit = iclv_clientehit;
    }

    public String getInum_cuentasactivas() {
        return inum_cuentasactivas;
    }

    public void setInum_cuentasactivas(String inum_cuentasactivas) {
        this.inum_cuentasactivas = inum_cuentasactivas;
    }

    public String getInum_cuentassaldadasperiodo() {
        return inum_cuentassaldadasperiodo;
    }

    public void setInum_cuentassaldadasperiodo(String inum_cuentassaldadasperiodo) {
        this.inum_cuentassaldadasperiodo = inum_cuentassaldadasperiodo;
    }

    public String getIclv_categoria() {
        return iclv_categoria;
    }

    public void setIclv_categoria(String iclv_categoria) {
        this.iclv_categoria = iclv_categoria;
    }

    public String getInum_valorscore() {
        return inum_valorscore;
    }

    public void setInum_valorscore(String inum_valorscore) {
        this.inum_valorscore = inum_valorscore;
    }

    public String getInum_rslimitecreditocuentasactivas() {
        return inum_rslimitecreditocuentasactivas;
    }

    public void setInum_rslimitecreditocuentasactivas(String inum_rslimitecreditocuentasactivas) {
        this.inum_rslimitecreditocuentasactivas = inum_rslimitecreditocuentasactivas;
    }

    public String getInum_rsporcentajelimitecreditocuentasactivas() {
        return inum_rsporcentajelimitecreditocuentasactivas;
    }

    public void setInum_rsporcentajelimitecreditocuentasactivas(String inum_rsporcentajelimitecreditocuentasactivas) {
        this.inum_rsporcentajelimitecreditocuentasactivas = inum_rsporcentajelimitecreditocuentasactivas;
    }

    public String getInum_rstotalcuentasmopactual() {
        return inum_rstotalcuentasmopactual;
    }

    public void setInum_rstotalcuentasmopactual(String inum_rstotalcuentasmopactual) {
        this.inum_rstotalcuentasmopactual = inum_rstotalcuentasmopactual;
    }

    public String getInum_rssaldovencidocreditocuentasactivas() {
        return inum_rssaldovencidocreditocuentasactivas;
    }

    public void setInum_rssaldovencidocreditocuentasactivas(String inum_rssaldovencidocreditocuentasactivas) {
        this.inum_rssaldovencidocreditocuentasactivas = inum_rssaldovencidocreditocuentasactivas;
    }

    public String getDfecconsulta() {
        return dfecconsulta;
    }

    public void setDfecconsulta(String dfecconsulta) {
        this.dfecconsulta = dfecconsulta;
    }

    public String getiFlagSanaTuDeuda() {
        return iFlagSanaTuDeuda;
    }

    public void setiFlagSanaTuDeuda(String iFlagSanaTuDeuda) {
        this.iFlagSanaTuDeuda = iFlagSanaTuDeuda;
    }

    public String getiOpcionSanaTuDeuda() {
        return iOpcionSanaTuDeuda;
    }

    public void setiOpcionSanaTuDeuda(String iOpcionSanaTuDeuda) {
        this.iOpcionSanaTuDeuda = iOpcionSanaTuDeuda;
    }

    public String getiPrcPagoUltimaCompra() {
        return iPrcPagoUltimaCompra;
    }

    public void setiPrcPagoUltimaCompra(String iPrcPagoUltimaCompra) {
        this.iPrcPagoUltimaCompra = iPrcPagoUltimaCompra;
    }

    public String getIcontadoplanlealtad() {
        return icontadoplanlealtad;
    }

    public void setIcontadoplanlealtad(String icontadoplanlealtad) {
        this.icontadoplanlealtad = icontadoplanlealtad;
    }

    public String getcPlazoVenta() {
        return cPlazoVenta;
    }

    public String getsSobrePrecioSimulado() {
        return sSobrePrecioSimulado;
    }

    public void setsSobrePrecioSimulado(String sSobrePrecioSimulado) {
        this.sSobrePrecioSimulado = sSobrePrecioSimulado;
    }

    public String getcFactorCliente() {
        return cFactorCliente;
    }

    public void setcFactorCliente(String cFactorCliente) {
        this.cFactorCliente = cFactorCliente;
    }

    public String getcFactorClienteEsp() {
        return cFactorClienteEsp;
    }

    public void setcFactorClienteEsp(String cFactorClienteEsp) {
        this.cFactorClienteEsp = cFactorClienteEsp;
    }

    public String getcFactorClienteBasico() {
        return cFactorClienteBasico;
    }

    public void setcFactorClienteBasico(String cFactorClienteBasico) {
        this.cFactorClienteBasico = cFactorClienteBasico;
    }

    public String getcMesesVenta() {
        return cMesesVenta;
    }

    public void setcMesesVenta(String cMesesVenta) {
        this.cMesesVenta = cMesesVenta;
    }

    public void setcPlazoVenta(String cPlazoVenta) {
        this.cPlazoVenta = cPlazoVenta;
    }

    public String getIPuntosParametricoArtAltoRiesgo() {
        return iPuntosParametricoArtAltoRiesgo;
    }

    public String getCPuntualidadCte() {
        return cPuntualidadCte;
    }

    public String getLPagoDineroE() {
        return lPagoDineroE;
    }

    public String getIEnganchePropuestoClienteS50() {
        return iEnganchePropuestoClienteS50;
    }

    public String getIFlagEsClienteS50() {
        return iFlagEsClienteS50;
    }

    public String getIFlagLineaCreditoEspecial() {
        return iFlagLineaCreditoEspecial;
    }

    public String getIPagosUltimosDoceMeses() {
        return iPagosUltimosDoceMeses;
    }

    public String getISaldoTotalCteSinAjuste() {
        return iSaldoTotalCteSinAjuste;
    }

    public String getDFechaAltaCte() {
        return dFechaAltaCte;
    }

    public String getINuevoPrePuntajeAltoRiesgo() {
        return iNuevoPrePuntajeAltoRiesgo;
    }

    public String getICreditoInicial() {
        return iCreditoInicial;
    }

    public String getLCliente() {
        return lCliente;
    }

    public String getIFlagRecalibracion() {
        return iFlagRecalibracion;
    }

    public String getDFechaPrimeraCompra() {
        return dFechaPrimeraCompra;
    }

    public String getISaldoCteFactorizado() {
        return iSaldoCteFactorizado;
    }

    public String getILineaCreditoRealPesos() {
        return iLineaCreditoRealPesos;
    }

    public String getIFlagClienteCreditoInicial() {
        return iFlagClienteCreditoInicial;
    }

    public String getIFlag18pi12() {
        return iFlag18pi12;
    }

    public String getITotalContadoNvaCompraSinAjuste() {
        return iTotalContadoNvaCompraSinAjuste;
    }

    public String getDFechaTienda() {
        return dFechaTienda;
    }

    public String getIVtaAccesoriosCel() {
        return iVtaAccesoriosCel;
    }

    public String getINumDepartamento() {
        return iNumDepartamento;
    }

    public String getIEngancheIlustrativo() {
        return iEngancheIlustrativo;
    }

    public String getINum_PuntosParametricosCN() {
        return iNum_PuntosParametricosCN;
    }

    public String getINum_PuntosParametricosCNAux() {
        return iNum_PuntosParametricosCNAux;
    }

    public String getCFactorCliente12() {
        return cFactorCliente12;
    }

    public String getCFactorCliente18() {
        return cFactorCliente18;
    }

    public String getCFactorCliente24() {
        return cFactorCliente24;
    }

    public String getCFactorClienteEsp12() {
        return cFactorClienteEsp12;
    }

    public String getCFactorClienteEsp18() {
        return cFactorClienteEsp18;
    }

    public String getCFactorClienteEsp24() {
        return cFactorClienteEsp24;
    }

    public String getIFlagParametricoCN() {
        return iFlagParametricoCN;
    }

    public String getIFlagParamCNParamVentas() {
        return iFlagParamCNParamVentas;
    }

    public String getSListaSku() {
        return SListaSku;
    }

    public String getSListaDcf() {
        return SListaDcf;
    }

    public String getITienda() {
        return iTienda;
    }

    public String getICaja() {
        return iCaja;
    }

    public String getIFolio() {
        return iFolio;
    }

    public String getIImporte() {
        return iImporte;
    }

    public String getCImporteArt() {
        return cImporteArt;
    }

    public String getISalarioMinimoCoppel() {
        return iSalarioMinimoCoppel;
    }

    public String getIFlagVentaContado() {
        return iFlagVentaContado;
    }

    public String getISumaAbonosBase() {
        return iSumaAbonosBase;
    }

    public String getISobrePrecioSimulado12() {
        return iSobrePrecioSimulado12;
    }

    public String getISobrePrecioSimulado18() {
        return iSobrePrecioSimulado18;
    }

    public String getISobrePrecioSimulado24() {
        return iSobrePrecioSimulado24;
    }

    public String getFFactorCMA() {
        return fFactorCMA;
    }

    public String getISaldaCon() {
        return iSaldaCon;
    }

    public String getIFlagCMA() {
        return iFlagCMA;
    }

    // Setter Methods

    public void setIPuntosParametricoArtAltoRiesgo(String iPuntosParametricoArtAltoRiesgo) {
        this.iPuntosParametricoArtAltoRiesgo = iPuntosParametricoArtAltoRiesgo;
    }

    public void setCPuntualidadCte(String cPuntualidadCte) {
        this.cPuntualidadCte = cPuntualidadCte;
    }

    public void setLPagoDineroE(String lPagoDineroE) {
        this.lPagoDineroE = lPagoDineroE;
    }



    public void setIEnganchePropuestoClienteS50(String iEnganchePropuestoClienteS50) {
        this.iEnganchePropuestoClienteS50 = iEnganchePropuestoClienteS50;
    }

    public void setIFlagEsClienteS50(String iFlagEsClienteS50) {
        this.iFlagEsClienteS50 = iFlagEsClienteS50;
    }

    public void setIFlagLineaCreditoEspecial(String iFlagLineaCreditoEspecial) {
        this.iFlagLineaCreditoEspecial = iFlagLineaCreditoEspecial;
    }

    public void setIPagosUltimosDoceMeses(String iPagosUltimosDoceMeses) {
        this.iPagosUltimosDoceMeses = iPagosUltimosDoceMeses;
    }

    public void setISaldoTotalCteSinAjuste(String iSaldoTotalCteSinAjuste) {
        this.iSaldoTotalCteSinAjuste = iSaldoTotalCteSinAjuste;
    }

    public void setDFechaAltaCte(String dFechaAltaCte) {
        this.dFechaAltaCte = dFechaAltaCte;
    }

    public void setINuevoPrePuntajeAltoRiesgo(String iNuevoPrePuntajeAltoRiesgo) {
        this.iNuevoPrePuntajeAltoRiesgo = iNuevoPrePuntajeAltoRiesgo;
    }

    public void setICreditoInicial(String iCreditoInicial) {
        this.iCreditoInicial = iCreditoInicial;
    }

    public void setLCliente(String lCliente) {
        this.lCliente = lCliente;
    }

    public void setIFlagRecalibracion(String iFlagRecalibracion) {
        this.iFlagRecalibracion = iFlagRecalibracion;
    }

    public void setDFechaPrimeraCompra(String dFechaPrimeraCompra) {
        this.dFechaPrimeraCompra = dFechaPrimeraCompra;
    }

    public void setISaldoCteFactorizado(String iSaldoCteFactorizado) {
        this.iSaldoCteFactorizado = iSaldoCteFactorizado;
    }

    public void setILineaCreditoRealPesos(String iLineaCreditoRealPesos) {
        this.iLineaCreditoRealPesos = iLineaCreditoRealPesos;
    }

    public void setIFlagClienteCreditoInicial(String iFlagClienteCreditoInicial) {
        this.iFlagClienteCreditoInicial = iFlagClienteCreditoInicial;
    }

    public void setIFlag18pi12(String iFlag18pi12) {
        this.iFlag18pi12 = iFlag18pi12;
    }

    public void setITotalContadoNvaCompraSinAjuste(String iTotalContadoNvaCompraSinAjuste) {
        this.iTotalContadoNvaCompraSinAjuste = iTotalContadoNvaCompraSinAjuste;
    }

    public void setDFechaTienda(String dFechaTienda) {
        this.dFechaTienda = dFechaTienda;
    }

    public void setIVtaAccesoriosCel(String iVtaAccesoriosCel) {
        this.iVtaAccesoriosCel = iVtaAccesoriosCel;
    }

    public void setINumDepartamento(String iNumDepartamento) {
        this.iNumDepartamento = iNumDepartamento;
    }

    public void setIEngancheIlustrativo(String iEngancheIlustrativo) {
        this.iEngancheIlustrativo = iEngancheIlustrativo;
    }

    public void setINum_PuntosParametricosCN(String iNum_PuntosParametricosCN) {
        this.iNum_PuntosParametricosCN = iNum_PuntosParametricosCN;
    }

    public void setINum_PuntosParametricosCNAux(String iNum_PuntosParametricosCNAux) {
        this.iNum_PuntosParametricosCNAux = iNum_PuntosParametricosCNAux;
    }

    public void setCFactorCliente12(String cFactorCliente12) {
        this.cFactorCliente12 = cFactorCliente12;
    }

    public void setCFactorCliente18(String cFactorCliente18) {
        this.cFactorCliente18 = cFactorCliente18;
    }

    public void setCFactorCliente24(String cFactorCliente24) {
        this.cFactorCliente24 = cFactorCliente24;
    }

    public void setCFactorClienteEsp12(String cFactorClienteEsp12) {
        this.cFactorClienteEsp12 = cFactorClienteEsp12;
    }

    public void setCFactorClienteEsp18(String cFactorClienteEsp18) {
        this.cFactorClienteEsp18 = cFactorClienteEsp18;
    }

    public void setCFactorClienteEsp24(String cFactorClienteEsp24) {
        this.cFactorClienteEsp24 = cFactorClienteEsp24;
    }

    public void setIFlagParametricoCN(String iFlagParametricoCN) {
        this.iFlagParametricoCN = iFlagParametricoCN;
    }

    public void setIFlagParamCNParamVentas(String iFlagParamCNParamVentas) {
        this.iFlagParamCNParamVentas = iFlagParamCNParamVentas;
    }

    public void setSListaSku(String SListaSku) {
        this.SListaSku = SListaSku;
    }

    public void setSListaDcf(String SListaDcf) {
        this.SListaDcf = SListaDcf;
    }

    public void setITienda(String iTienda) {
        this.iTienda = iTienda;
    }

    public void setICaja(String iCaja) {
        this.iCaja = iCaja;
    }

    public void setIFolio(String iFolio) {
        this.iFolio = iFolio;
    }

    public void setIImporte(String iImporte) {
        this.iImporte = iImporte;
    }

    public void setCImporteArt(String cImporteArt) {
        this.cImporteArt = cImporteArt;
    }

    public void setISalarioMinimoCoppel(String iSalarioMinimoCoppel) {
        this.iSalarioMinimoCoppel = iSalarioMinimoCoppel;
    }

    public void setIFlagVentaContado(String iFlagVentaContado) {
        this.iFlagVentaContado = iFlagVentaContado;
    }

    public void setISumaAbonosBase(String iSumaAbonosBase) {
        this.iSumaAbonosBase = iSumaAbonosBase;
    }

    public void setISobrePrecioSimulado12(String iSobrePrecioSimulado12) {
        this.iSobrePrecioSimulado12 = iSobrePrecioSimulado12;
    }

    public void setISobrePrecioSimulado18(String iSobrePrecioSimulado18) {
        this.iSobrePrecioSimulado18 = iSobrePrecioSimulado18;
    }

    public void setISobrePrecioSimulado24(String iSobrePrecioSimulado24) {
        this.iSobrePrecioSimulado24 = iSobrePrecioSimulado24;
    }

    public void setFFactorCMA(String fFactorCMA) {
        this.fFactorCMA = fFactorCMA;
    }

    public void setISaldaCon(String iSaldaCon) {
        this.iSaldaCon = iSaldaCon;
    }

    public void setIFlagCMA(String iFlagCMA) {
        this.iFlagCMA = iFlagCMA;
    }

    public String getiFlagVentaAsistida() {
        return iFlagVentaAsistida;
    }

    public void setiFlagVentaAsistida(String iFlagVentaAsistida) {
        this.iFlagVentaAsistida = iFlagVentaAsistida;
    }

    public String getiTotalContadoRopa() {
        return iTotalContadoRopa;
    }

    public void setiTotalContadoRopa(String iTotalContadoRopa) {
        this.iTotalContadoRopa = iTotalContadoRopa;
    }

    public String obtenerMesesVenta(){
        String res;
        String[] plazos = this.cPlazoVenta.split("|");

        try {
            res = "";

            for (int i = 0; i < plazos.length; i++) {
                if (plazos[i].equals("1")) {
                    res = res + "12|";
                } else if (plazos[i].equals("2")) {
                    res = res + "18|";
                } else if (plazos[i].equals("3")) {
                    res = res + "24|";
                } else if (plazos[i].equals("7")) {
                    res = res + "30|";
                } else if (plazos[i].equals("9")) {
                    res = res + "36|";
                }
            }

            if(res.contains("|")){
                res = res.substring(0, res.length()-1);
            }

        } finally {
            plazos = null;
        }

        return res;
    }

    /**
     * Setea los siguientes atributos
     * cPlazoVenta, cMesesVenta, cFactorCliente, cFactorClienteEsp, cFactorClienteBasico
     * @param iMaxPlazo Plazo maximo
     * @param objFactor Objeto Factor
     */
    public void setPlazoFactor(String iMaxPlazo, Factor objFactor,String sPuntualidad, int flag){

        if (iMaxPlazo.equals(GlobalEnum.enumPlazos.PLAZOS_12.getPlazo())) {
            this.setcPlazoVenta("1");
            this.setcMesesVenta("12");
            this.setcFactorCliente(objFactor.getsFactor12());
            this.setcFactorClienteEsp(objFactor.getFactorEspecial12());
            this.setcFactorClienteBasico(objFactor.getFactorBasico12());

        } else if (iMaxPlazo.equals(GlobalEnum.enumPlazos.PLAZOS_18.getPlazo())) {
            this.setcPlazoVenta("2");
            if((sPuntualidad.equals("N")|| sPuntualidad.equals("B")|| sPuntualidad.equals("C")|| sPuntualidad.equals("D")) && flag == 1 )
            {
                this.setcFactorCliente(objFactor.getsFactor12());
                this.setcFactorClienteEsp(objFactor.getFactorEspecial12());
                this.setcFactorClienteBasico(objFactor.getFactorBasico12());
                this.setcMesesVenta("12");
            }
            else
            {
                this.setcFactorCliente(objFactor.getsFactor18());
                this.setcFactorClienteEsp(objFactor.getFactorEspecial18());
                this.setcFactorClienteBasico(objFactor.getFactorBasico18());
                this.setcMesesVenta("18");
            }

        } else if (iMaxPlazo.equals(GlobalEnum.enumPlazos.PLAZOS_24.getPlazo())) {
            this.setcPlazoVenta("3");
            this.setcMesesVenta("24");
            this.setcFactorCliente(objFactor.getsFactor24());
            this.setcFactorClienteEsp(objFactor.getFactorEspecial24());
            this.setcFactorClienteBasico(objFactor.getFactorBasico24());

        } else if (iMaxPlazo.equals(GlobalEnum.enumPlazos.PLAZOS_30.getPlazo())) {
            this.setcPlazoVenta("7");
            this.setcMesesVenta("30");
            this.setcFactorCliente(objFactor.getsFactor30());
            this.setcFactorClienteEsp(objFactor.getFactorEspecial30());
            this.setcFactorClienteBasico(objFactor.getFactorBasico30());

        } else if (iMaxPlazo.equals(GlobalEnum.enumPlazos.PLAZOS_36.getPlazo())) {
            this.setcPlazoVenta("9");
            this.setcMesesVenta("36");
            this.setcFactorCliente(objFactor.getsFactor36());
            this.setcFactorClienteEsp(objFactor.getFactorEspecial36());
            this.setcFactorClienteBasico(objFactor.getFactorBasico36());
        }
    }

    public String getInum_infoburocredito() {
        return inum_infoburocredito;
    }

    public void setInum_infoburocredito(String inum_infoburocredito) {
        this.inum_infoburocredito = inum_infoburocredito;
    }

}














