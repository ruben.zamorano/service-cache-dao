package DTO;


public class ParametricoCartera {
    private int iNumPuntosParametricosCN = 0;
    private int iNumPuntosParametricosCNAux = 0;
    public short getIflagsanatudeuda() {
        return iFlagSanaTuDeuda;
    }

    public void setIflagsanatudeuda(short iflagsanatudeuda) {
        this.iFlagSanaTuDeuda = iflagsanatudeuda;
    }

    public int getiOpcionSanaTuDeuda() {
        return iOpcionSanaTuDeuda;
    }

    public void setiOpcionSanaTuDeuda(int iOpcionSanaTuDeuda) {
        this.iOpcionSanaTuDeuda = iOpcionSanaTuDeuda;
    }

    public int getiPrcPagoUltimaCompra() {
        return iPrcPagoUltimaCompra;
    }

    public void setiPrcPagoUltimaCompra(int iPrcPagoUltimaCompra) {
        this.iPrcPagoUltimaCompra = iPrcPagoUltimaCompra;
    }

    private double ffactorCMA = 0.0;
    private short iFlagSanaTuDeuda=0;
    private int iOpcionSanaTuDeuda=0;
    private int iPrcPagoUltimaCompra=0;

    public ParametricoCartera() {

    }

    /**
     * @param iNumPuntosParametricosCN
     * @param iNumPuntosParametricosCNAux
     * @param ffactorCMA
     */
    public ParametricoCartera(int iNumPuntosParametricosCN, int iNumPuntosParametricosCNAux, double ffactorCMA) {
        this.iNumPuntosParametricosCN = iNumPuntosParametricosCN;
        this.iNumPuntosParametricosCNAux = iNumPuntosParametricosCNAux;
        this.ffactorCMA = ffactorCMA;
    }

    /**
     * @return the iNumPuntosParametricosCN
     */
    public int getiNumPuntosParametricosCN() {
        return iNumPuntosParametricosCN;
    }

    /**
     * @param iNumPuntosParametricosCN
     *            the iNumPuntosParametricosCN to set
     */
    public void setiNumPuntosParametricosCN(int iNumPuntosParametricosCN) {
        this.iNumPuntosParametricosCN = iNumPuntosParametricosCN;
    }

    /**
     * @return the iNumPuntosParametricosCNAux
     */
    public int getiNumPuntosParametricosCNAux() {
        return iNumPuntosParametricosCNAux;
    }

    /**
     * @param iNumPuntosParametricosCNAux
     *            the iNumPuntosParametricosCNAux to set
     */
    public void setiNumPuntosParametricosCNAux(int iNumPuntosParametricosCNAux) {
        this.iNumPuntosParametricosCNAux = iNumPuntosParametricosCNAux;
    }

    /**
     * @return the ffactorCMA
     */
    public double getFfactorCMA() {
        return ffactorCMA;
    }

    /**
     * @param ffactorCMA the ffactorCMA to set
     */
    public void setFfactorCMA(double ffactorCMA) {
        this.ffactorCMA = ffactorCMA;
    }

    @Override
    public String toString() {
        return "ParametricoCartera [iNumPuntosParametricosCN=" + iNumPuntosParametricosCN
                + ", iNumPuntosParametricosCNAux=" + iNumPuntosParametricosCNAux + ", ffactorCMA=" + ffactorCMA
                + ", iFlagSanaTuDeuda=" + iFlagSanaTuDeuda + ", iOpcionSanaTuDeuda=" + iOpcionSanaTuDeuda
                + ", iPrcPagoUltimaCompra=" + iPrcPagoUltimaCompra + "]";
    }
}
