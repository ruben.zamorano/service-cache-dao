package DTO;


import Base.GlobalEnum;

public class Factor {
    public static final int CLIENTE_NORMAL=0;
    public static final int CLIENTE_ESPECIAL=1;
    public static final int CLIENTE_BASICO=2;

    protected String sFactor12;
    protected String sFactor18;
    protected String sFactor24;
    protected String sFactor30;
    protected String sFactor36;

    protected String factorEspecial12;
    protected String factorEspecial18;
    protected String factorEspecial24;
    protected String factorEspecial30;
    protected String factorEspecial36;

    protected String factorBasico12;
    protected String factorBasico18;
    protected String factorBasico24;
    protected String factorBasico30;
    protected String factorBasico36;

    public String getsFactor30() {
        return sFactor30;
    }
    public void setsFactor30(String sFactor30) {
        this.sFactor30 = sFactor30;
    }
    public String getsFactor36() {
        return sFactor36;
    }
    public void setsFactor36(String sFactor36) {
        this.sFactor36 = sFactor36;
    }
    public String getFactorEspecial30() {
        return factorEspecial30;
    }
    public void setFactorEspecial30(String factorEspecial30) {
        this.factorEspecial30 = factorEspecial30;
    }
    public String getFactorEspecial36() {
        return factorEspecial36;
    }
    public void setFactorEspecial36(String factorEspecial36) {
        this.factorEspecial36 = factorEspecial36;
    }
    public String getFactorBasico12() {
        return factorBasico12;
    }
    public void setFactorBasico12(String factorBasico12) {
        this.factorBasico12 = factorBasico12;
    }
    public String getFactorBasico18() {
        return factorBasico18;
    }
    public void setFactorBasico18(String factorBasico18) {
        this.factorBasico18 = factorBasico18;
    }
    public String getFactorBasico24() {
        return factorBasico24;
    }
    public void setFactorBasico24(String factorBasico24) {
        this.factorBasico24 = factorBasico24;
    }
    public String getFactorBasico30() {
        return factorBasico30;
    }
    public void setFactorBasico30(String factorBasico30) {
        this.factorBasico30 = factorBasico30;
    }
    public String getFactorBasico36() {
        return factorBasico36;
    }
    public void setFactorBasico36(String factorBasico36) {
        this.factorBasico36 = factorBasico36;
    }
    public String getsFactor12() {
        return sFactor12;
    }
    public void setsFactor12(String sFactor12) {
        this.sFactor12 = sFactor12;
    }
    public String getsFactor18() {
        return sFactor18;
    }
    public void setsFactor18(String sFactor18) {
        this.sFactor18 = sFactor18;
    }
    public String getsFactor24() {
        return sFactor24;
    }
    public void setsFactor24(String sFactor24) {
        this.sFactor24 = sFactor24;
    }
    public String getFactorEspecial12() {
        return factorEspecial12;
    }
    public void setFactorEspecial12(String factorEspecial12) {
        this.factorEspecial12 = factorEspecial12;
    }
    public String getFactorEspecial18() {
        return factorEspecial18;
    }
    public void setFactorEspecial18(String factorEspecial18) {
        this.factorEspecial18 = factorEspecial18;
    }
    public String getFactorEspecial24() {
        return factorEspecial24;
    }
    public void setFactorEspecial24(String factorEspecial24) {
        this.factorEspecial24 = factorEspecial24;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Factor [sFactor12=" + sFactor12 + ", sFactor18=" + sFactor18 + ", sFactor24=" + sFactor24
                + ", factorEspecial12=" + factorEspecial12 + ", factorEspecial18=" + factorEspecial18
                + ", factorEspecial24=" + factorEspecial24 + "]";
    }

    public String obtenerFactorCliente(){
        return this.sFactor12 + "|" + this.sFactor18 + "|" + this.sFactor24 + "|" + this.sFactor30 + "|" + this.sFactor36;
    }

    public String obtenerFactorClienteEsp(){
        return this.factorEspecial12 + "|" + this.factorEspecial18 + "|" + this.factorEspecial24 + "|" + this.factorEspecial30 + "|" + this.factorEspecial36;
    }

    public String obtenerFactorClienteBasico(){
        return this.factorBasico12 + "|" + this.factorBasico18 + "|" + this.factorBasico24 + "|" + this.factorBasico30 + "|" + this.factorBasico36;
    }


    /**
     * Regresa un arreglo con los factores del cliente [factorCte, factorCteEsp, factorCteBasico]
     * @param iPlazoVenta: El plazo de la venta
     * @return
     */
    public String[] getFactorCliente(String iPlazoVenta){
        String[] factores=new String[3];

        if(iPlazoVenta.equals(GlobalEnum.enumPlazos.PLAZOS_12.getPlazo())){
            factores[CLIENTE_NORMAL]=this.sFactor12;
            factores[CLIENTE_ESPECIAL]=this.getFactorEspecial12();
            factores[CLIENTE_BASICO]=this.getFactorBasico12();
        }else if(iPlazoVenta.equals(GlobalEnum.enumPlazos.PLAZOS_18.getPlazo())){
            factores[CLIENTE_NORMAL]=this.sFactor18;
            factores[CLIENTE_ESPECIAL]=this.getFactorEspecial18();
            factores[CLIENTE_BASICO]=this.getFactorBasico18();
        }else if(iPlazoVenta.equals(GlobalEnum.enumPlazos.PLAZOS_24.getPlazo())){
            factores[CLIENTE_NORMAL]=this.sFactor24;
            factores[CLIENTE_ESPECIAL]=this.getFactorEspecial24();
            factores[CLIENTE_BASICO]=this.getFactorBasico24();
        }else if(iPlazoVenta.equals(GlobalEnum.enumPlazos.PLAZOS_30.getPlazo())){
            factores[CLIENTE_NORMAL]=this.sFactor30;
            factores[CLIENTE_ESPECIAL]=this.getFactorEspecial30();
            factores[CLIENTE_BASICO]=this.getFactorBasico30();
        }else if(iPlazoVenta.equals(GlobalEnum.enumPlazos.PLAZOS_36.getPlazo())){
            factores[CLIENTE_NORMAL]=this.sFactor36;
            factores[CLIENTE_ESPECIAL]=this.getFactorEspecial36();
            factores[CLIENTE_BASICO]=this.getFactorBasico36();
        }

        return factores;
    }


}
