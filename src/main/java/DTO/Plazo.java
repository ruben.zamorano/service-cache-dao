package DTO;


import Base.GlobalEnum;

public class Plazo {
    public static final int ID_PLAZO_12=1;
    public static final int ID_PLAZO_18=2;
    public static final int ID_PLAZO_24=3;
    public static final int ID_PLAZO_30=7;
    public static final int ID_PLAZO_36=9;

    protected String iPlazoMensual;
    protected String iPlazoQuincenal;
    protected String iIdPlazo;

    public String getiPlazoMensual() {
        return iPlazoMensual;
    }
    public void setiPlazoMensual(String iPlazoMensual) {
        this.iPlazoMensual = iPlazoMensual;
    }
    public String getiPlazoQuincenal() {
        return iPlazoQuincenal;
    }
    public void setiPlazoQuincenal(String iPlazoQuincenal) {
        this.iPlazoQuincenal = iPlazoQuincenal;
    }
    public String getiIdPlazo() {
        return iIdPlazo;
    }
    public void setiIdPlazo(String iIdPlazo) {
        this.iIdPlazo = iIdPlazo;
    }

    public static String getPlazoMensual(String sPlazoMensual){
        String plazo="";

        switch (sPlazoMensual) {
            case "12":
                plazo = GlobalEnum.enumPlazos.PLAZOS_12.getPlazo();
                break;
            case "18":
                plazo = GlobalEnum.enumPlazos.PLAZOS_18.getPlazo();
                break;
            case "24":
                plazo = GlobalEnum.enumPlazos.PLAZOS_24.getPlazo();
                break;
            case "30":
                plazo = GlobalEnum.enumPlazos.PLAZOS_30.getPlazo();
                break;
            case "36":
                plazo = GlobalEnum.enumPlazos.PLAZOS_36.getPlazo();
            default:
                break;
        }

        return plazo;
    }


    /**
     * Convierte a plazo tienda para el grabado en funciones de tienda
     * @param iPlazoVenta
     * @return
     */
    public static String convertirPlazoTienda(int iPlazoVenta){
        int[] plazos={
                Plazo.ID_PLAZO_12
                ,Plazo.ID_PLAZO_18
                ,Plazo.ID_PLAZO_24
                ,Plazo.ID_PLAZO_30
                ,Plazo.ID_PLAZO_36
        };

        return Integer.toString(plazos[iPlazoVenta]);
    }

}
