package DTO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Objeto para grabar la informaci�n de los totales de la orden por area
 * @author Marco Antonio Olivas Olmeda
 * @version 1.0
 * @created 02-ene-2015 11:30:08 a.m.
 */
public class Totales {

    /**
     * Importe total de contado en los sku del �rea de Ropa
     */
    private int iTotalContadoRopa;
    /**
     * Importe total de contado en los sku del �rea de Muebles
     */
    private int iTotalContadoMuebles;
    /**
     * Importe total de contado en la orden
     */
    private int iTotalContado;
    /**
     * Valor del pago inicial que debe cubrir el cliente Coppel para realizar la compra
     */
    private int iPagoInicialTotal;
    /**
     * Valor del pago inicial que le corresponde al �rea de Ropa para que el cliente
     * Coppel pueda realizar la compra
     */
    private int iPagoInicialRopa;
    /**
     * Importe del pago inicial que deja el cliente Coppel para el �rea de Ropa
     */
    private int iPagoInicialRopaCliente;
    /**
     * Valor del pago inicial que le corresponde al �rea de Mubles para que el cliente
     * Coppel pueda realizar la compra
     */
    private int iPagoInicialMuebles;
    /**
     * Importe del pago inicial que deja el cliente Coppel para el �rea de Muebles
     */
    private int iPagoInicialMueblesCliente;
    /**
     * Importe del dinero electr�nico utilizado en el c�lculo del pago inicial
     */
    private int iDineroElectronicoUtilizado;
    /**
     * Importe del dinero electr�nico utilizado en el �rea de Ropa para el c�lculo del
     * pago inicial
     */
    private int iDineroElectronicoRopa;
    /**
     * Importe del dinero electr�nico utilizado en el �rea de Muebles para el c�lculo
     * del pago inicial
     */
    private int iDineroElectronicoMuebles;
    /**
     * Bandera para identificar si se trata del primer c�lculo o si se debe considerar
     * el Dinero Electr�nico y Pago inicial que proporciona el cliente Coppel
     */
    private int iPrimerCalculo;
    /**
     * Importe total de contado de los art�culos a los cuales aplica el dinero
     * electr�nico en el �rea de Muebles
     */
    private int iTotalContadoAplicaDEMuebles;
    /**
     * Fecha de la tienda en Coppel
     */
    private Date dFechaTienda;
    /**
     * Importe total de contado del �rea de Muebles para realizar validaciones sobre
     * los importes
     */
    private int iTotalContadoMueblesCalculo;
    /**
     * Importe total de contado del �rea de Ropa para realizar validaciones sobre los
     * importes
     */
    private int iTotalContadoRopaCalculo;
    /**
     * Importe del pago inicial excedente que deja el cliente
     */
    private int iPagoInicialExcedente;
    /**
     * Importe total de contado al cual se le puede aplicar descuento por dinero
     * electr�nico
     */
    private int iTotalContadoAplicaDE;
    /**
     * Bandera para indicar si se debe recalcular el pago inicial
     */
    private int iFlagRecalculo;

    private int iPagoRequeridoMuebles;

    private int iPagoRequeridoRopa;

    private String sIpCarteraCliente;

    private List<Plazo> plazo;

    private int iSaliarioMinimo;

    private int iNumMaximoPlazo;
    public int getiNumMaximoPlazo() {
        return iNumMaximoPlazo;
    }

    public void setiNumMaximoPlazo(int iNumMaximoPlazo) {
        this.iNumMaximoPlazo = iNumMaximoPlazo;
    }

    public int getiSaliarioMinimo() {
        return iSaliarioMinimo;
    }

    public void setiSaliarioMinimo(int iSaliarioMinimo) {
        this.iSaliarioMinimo = iSaliarioMinimo;
    }

    public List<Plazo> getPlazos() {
        if (plazo == null) {
            plazo = new ArrayList<Plazo>();
        }
        return this.plazo;
    }
    public Totales(){

    }

    /**
     * @return the dFechaTienda
     */
    public Date getdFechaTienda() {
        return dFechaTienda;
    }

    /**
     * @return the iDineroElectronicoMuebles
     */
    public int getiDineroElectronicoMuebles() {
        return iDineroElectronicoMuebles;
    }

    /**
     * @return the iDineroElectronicoRopa
     */
    public int getiDineroElectronicoRopa() {
        return iDineroElectronicoRopa;
    }

    /**
     * @return the iDineroElectronicoUtilizado
     */
    public int getiDineroElectronicoUtilizado() {
        return iDineroElectronicoUtilizado;
    }

    /**
     * @return the iFlagRecalculo
     */
    public int getiFlagRecalculo() {
        return iFlagRecalculo;
    }

    /**
     * @return the iPagoInicialExcedente
     */
    public int getiPagoInicialExcedente() {
        return iPagoInicialExcedente;
    }

    /**
     * @return the iPagoInicialMuebles
     */
    public int getiPagoInicialMuebles() {
        return iPagoInicialMuebles;
    }

    /**
     * @return the iPagoInicialMueblesCliente
     */
    public int getiPagoInicialMueblesCliente() {
        return iPagoInicialMueblesCliente;
    }

    /**
     * @return the iPagoInicialRopa
     */
    public int getiPagoInicialRopa() {
        return iPagoInicialRopa;
    }

    /**
     * @return the iPagoInicialRopaCliente
     */
    public int getiPagoInicialRopaCliente() {
        return iPagoInicialRopaCliente;
    }

    /**
     * @return the iPagoInicialTotal
     */
    public int getiPagoInicialTotal() {
        return iPagoInicialTotal;
    }

    /**
     * @return the iPrimerCalculo
     */
    public int getiPrimerCalculo() {
        return iPrimerCalculo;
    }

    /**
     * @return the iTotalContado
     */
    public int getiTotalContado() {
        return iTotalContado;
    }

    /**
     * @return the iTotalContadoAplicaDE
     */
    public int getiTotalContadoAplicaDE() {
        return iTotalContadoAplicaDE;
    }

    /**
     * @return the iTotalContadoAplicaDEMuebles
     */
    public int getiTotalContadoAplicaDEMuebles() {
        return iTotalContadoAplicaDEMuebles;
    }

    /**
     * @return the iTotalContadoMuebles
     */
    public int getiTotalContadoMuebles() {
        return iTotalContadoMuebles;
    }

    /**
     * @return the iTotalContadoMueblesCalculo
     */
    public int getiTotalContadoMueblesCalculo() {
        return iTotalContadoMueblesCalculo;
    }

    /**
     * @return the iTotalContadoRopa
     */
    public int getiTotalContadoRopa() {
        return iTotalContadoRopa;
    }

    /**
     * @return the iTotalContadoRopaCalculo
     */
    public int getiTotalContadoRopaCalculo() {
        return iTotalContadoRopaCalculo;
    }

    /**
     * @param dFechaTienda the dFechaTienda to set
     */
    public void setdFechaTienda(Date dFechaTienda) {
        this.dFechaTienda = dFechaTienda;
    }

    /**
     * @param iDineroElectronicoMuebles the iDineroElectronicoMuebles to set
     */
    public void setiDineroElectronicoMuebles(int iDineroElectronicoMuebles) {
        this.iDineroElectronicoMuebles = iDineroElectronicoMuebles;
    }

    /**
     * @param iDineroElectronicoRopa the iDineroElectronicoRopa to set
     */
    public void setiDineroElectronicoRopa(int iDineroElectronicoRopa) {
        this.iDineroElectronicoRopa = iDineroElectronicoRopa;
    }

    /**
     * @param iDineroElectronicoUtilizado the iDineroElectronicoUtilizado to set
     */
    public void setiDineroElectronicoUtilizado(int iDineroElectronicoUtilizado) {
        this.iDineroElectronicoUtilizado = iDineroElectronicoUtilizado;
    }

    /**
     * @param iFlagRecalculo the iFlagRecalculo to set
     */
    public void setiFlagRecalculo(int iFlagRecalculo) {
        this.iFlagRecalculo = iFlagRecalculo;
    }

    /**
     * @param iPagoInicialExcedente the iPagoInicialExcedente to set
     */
    public void setiPagoInicialExcedente(int iPagoInicialExcedente) {
        this.iPagoInicialExcedente = iPagoInicialExcedente;
    }

    /**
     * @param iPagoInicialMuebles the iPagoInicialMuebles to set
     */
    public void setiPagoInicialMuebles(int iPagoInicialMuebles) {
        this.iPagoInicialMuebles = iPagoInicialMuebles;
    }

    /**
     * @param iPagoInicialMueblesCliente the iPagoInicialMueblesCliente to set
     */
    public void setiPagoInicialMueblesCliente(int iPagoInicialMueblesCliente) {
        this.iPagoInicialMueblesCliente = iPagoInicialMueblesCliente;
    }

    /**
     * @param iPagoInicialRopa the iPagoInicialRopa to set
     */
    public void setiPagoInicialRopa(int iPagoInicialRopa) {
        this.iPagoInicialRopa = iPagoInicialRopa;
    }

    /**
     * @param iPagoInicialRopaCliente the iPagoInicialRopaCliente to set
     */
    public void setiPagoInicialRopaCliente(int iPagoInicialRopaCliente) {
        this.iPagoInicialRopaCliente = iPagoInicialRopaCliente;
    }

    /**
     * @param iPagoInicialTotal the iPagoInicialTotal to set
     */
    public void setiPagoInicialTotal(int iPagoInicialTotal) {
        this.iPagoInicialTotal = iPagoInicialTotal;
    }

    /**
     * @param iPrimerCalculo the iPrimerCalculo to set
     */
    public void setiPrimerCalculo(int iPrimerCalculo) {
        this.iPrimerCalculo = iPrimerCalculo;
    }

    /**
     * @param iTotalContado the iTotalContado to set
     */
    public void setiTotalContado(int iTotalContado) {
        this.iTotalContado = iTotalContado;
    }

    /**
     * @param iTotalContadoAplicaDE the iTotalContadoAplicaDE to set
     */
    public void setiTotalContadoAplicaDE(int iTotalContadoAplicaDE) {
        this.iTotalContadoAplicaDE = iTotalContadoAplicaDE;
    }

    /**
     * @param iTotalContadoAplicaDEMuebles the iTotalContadoAplicaDEMuebles to set
     */
    public void setiTotalContadoAplicaDEMuebles(int iTotalContadoAplicaDEMuebles) {
        this.iTotalContadoAplicaDEMuebles = iTotalContadoAplicaDEMuebles;
    }

    /**
     * @param iTotalContadoMuebles the iTotalContadoMuebles to set
     */
    public void setiTotalContadoMuebles(int iTotalContadoMuebles) {
        this.iTotalContadoMuebles = iTotalContadoMuebles;
    }

    /**
     * @param iTotalContadoMueblesCalculo the iTotalContadoMueblesCalculo to set
     */
    public void setiTotalContadoMueblesCalculo(int iTotalContadoMueblesCalculo) {
        this.iTotalContadoMueblesCalculo = iTotalContadoMueblesCalculo;
    }

    /**
     * @param iTotalContadoRopa the iTotalContadoRopa to set
     */
    public void setiTotalContadoRopa(int iTotalContadoRopa) {
        this.iTotalContadoRopa = iTotalContadoRopa;
    }

    /**
     * @param iTotalContadoRopaCalculo the iTotalContadoRopaCalculo to set
     */
    public void setiTotalContadoRopaCalculo(int iTotalContadoRopaCalculo) {
        this.iTotalContadoRopaCalculo = iTotalContadoRopaCalculo;
    }

    public int getiPagoRequeridoMuebles() {
        return iPagoRequeridoMuebles;
    }

    public void setiPagoRequeridoMuebles(int iPagoRequeridoMuebles) {
        this.iPagoRequeridoMuebles = iPagoRequeridoMuebles;
    }

    public int getiPagoRequeridoRopa() {
        return iPagoRequeridoRopa;
    }

    public void setiPagoRequeridoRopa(int iPagoRequeridoRopa) {
        this.iPagoRequeridoRopa = iPagoRequeridoRopa;
    }

    public String getsIpCarteraCliente() {
        return sIpCarteraCliente;
    }

    public void setsIpCarteraCliente(String sIpCarteraCliente) {
        this.sIpCarteraCliente = sIpCarteraCliente;
    }



}