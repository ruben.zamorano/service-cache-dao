package DTO;


/**
 *
 * @author jsanchezm
 */
public class type_fun_obtenerabonoropa {
    private int num_respuesta;
    private String des_mensaje;
    private int imp_abonoquincenal;
    private int imp_nuevoabonoquincenal;

    /**
     * @return the des_mensaje
     */
    public String getDes_mensaje() {
        return des_mensaje;
    }

    /**
     * @return the imp_abonoquincenal
     */
    public int getImp_abonoquincenal() {
        return imp_abonoquincenal;
    }

    /**
     * @return the imp_nuevoabonoquincenal
     */
    public int getImp_nuevoabonoquincenal() {
        return imp_nuevoabonoquincenal;
    }

    /**
     * @return the num_respuesta
     */
    public int getNum_respuesta() {
        return num_respuesta;
    }

    /**
     * @param des_mensaje the des_mensaje to set
     */
    public void setDes_mensaje(String des_mensaje) {
        this.des_mensaje = des_mensaje;
    }

    /**
     * @param imp_abonoquincenal the imp_abonoquincenal to set
     */
    public void setImp_abonoquincenal(int imp_abonoquincenal) {
        this.imp_abonoquincenal = imp_abonoquincenal;
    }

    /**
     * @param imp_nuevoabonoquincenal the imp_nuevoabonoquincenal to set
     */
    public void setImp_nuevoabonoquincenal(int imp_nuevoabonoquincenal) {
        this.imp_nuevoabonoquincenal = imp_nuevoabonoquincenal;
    }

    /**
     * @param num_respuesta the num_respuesta to set
     */
    public void setNum_respuesta(int num_respuesta) {
        this.num_respuesta = num_respuesta;
    }

    @Override
    public String toString()
    {
        return "type_fun_obtenerabonoropa [num_respuesta=" + num_respuesta + ", des_mensaje=" + des_mensaje + ", imp_abonoquincenal="
                + imp_abonoquincenal + ", imp_nuevoabonoquincenal=" + imp_nuevoabonoquincenal + "]";
    }
}
