package DTO;


public class SysFlags {

    private int flagCreditoInicial;
    private int flagEngancheIlustrativo;
    private int flag18pi12;
    private int flagParamCnParamVentas;
    private int flagParametricoCN;
    private int flagRecalibracion;
    private int flagRecalibracionRopa;
    private int flagCMA;

    private String ipPagoInicial;
    private String cIpServidor;
    private String iPuerto;

    public int getFlagCreditoInicial() {
        return flagCreditoInicial;
    }
    public void setFlagCreditoInicial(int flagCreditoInicial) {

        flagCreditoInicial = flagCreditoInicial == -1 ? 0: flagCreditoInicial;
        this.flagCreditoInicial = flagCreditoInicial;
    }
    public int getFlagEngancheIlustrativo() {
        return flagEngancheIlustrativo;
    }
    public void setFlagEngancheIlustrativo(int flagEngancheIlustrativo) {
        flagEngancheIlustrativo = flagEngancheIlustrativo == -1 ? 0: flagEngancheIlustrativo;
        this.flagEngancheIlustrativo = flagEngancheIlustrativo;
    }
    public int getFlag18pi12() {
        return flag18pi12;
    }
    public void setFlag18pi12(int flag18pi12) {
        flag18pi12 = flag18pi12 == -1 ? 0: flag18pi12;
        this.flag18pi12 = flag18pi12;
    }
    public int getFlagParamCnParamVentas() {
        return flagParamCnParamVentas;
    }
    public void setFlagParamCnParamVentas(int flagParamCnParamVentas) {
        flagParamCnParamVentas = flagParamCnParamVentas == -1 ? 0: flagParamCnParamVentas;
        this.flagParamCnParamVentas = flagParamCnParamVentas;
    }
    public int getFlagParametricoCN() {
        return flagParametricoCN;
    }
    public void setFlagParametricoCN(int flagParametricoCN) {
        flagParametricoCN = flagParametricoCN == -1 ? 0: flagParametricoCN;
        this.flagParametricoCN = flagParametricoCN;
    }
    public int getFlagRecalibracion() {
        return flagRecalibracion;
    }
    public void setFlagRecalibracion(int flagRecalibracion) {
        flagRecalibracion = flagRecalibracion == -1 ? 0: flagRecalibracion;
        this.flagRecalibracion = flagRecalibracion;
    }
    public int getFlagRecalibracionRopa() {
        return flagRecalibracionRopa;
    }
    public void setFlagRecalibracionRopa(int flagRecalibracionRopa) {
        flagRecalibracionRopa = flagRecalibracionRopa == -1 ? 0: flagRecalibracionRopa;
        this.flagRecalibracionRopa = flagRecalibracionRopa;
    }

    public String getIpPagoInicial() {
        return ipPagoInicial;
    }
    public void setIpPagoInicial(String ipPagoInicial) {
        this.ipPagoInicial = ipPagoInicial;
    }
    public String getcIpServidor() {
        return cIpServidor;
    }
    public void setcIpServidor(String cIpServidor) {
        this.cIpServidor = cIpServidor;
    }
    public String getiPuerto() {
        return iPuerto;
    }
    public void setiPuerto(String iPuerto) {
        this.iPuerto = iPuerto;
    }
    public int getFlagCMA() {
        return flagCMA;
    }
    public void setFlagCMA(int flagCMA) {
        this.flagCMA = flagCMA;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SysFlags [flagCreditoInicial=" + flagCreditoInicial + ", flagEngancheIlustrativo="
                + flagEngancheIlustrativo + ", flag18pi12=" + flag18pi12 + ", flagParamCnParamVentas="
                + flagParamCnParamVentas + ", flagParametricoCN=" + flagParametricoCN + ", flagRecalibracion="
                + flagRecalibracion + ", flagRecalibracionRopa=" + flagRecalibracionRopa + ", flagCMA=" + flagCMA
                + ", ipPagoInicial=" + ipPagoInicial + ", cIpServidor=" + cIpServidor + ", iPuerto=" + iPuerto + "]";
    }
}
