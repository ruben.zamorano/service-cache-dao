package DTO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//package com.coppel.ecommerce.calculopagoinicial.dto.type;

import java.util.Date;

/**
 *
 * @author jsanchezm
 */
public class typ_gndominio {
    private int ciudad;
    private String nombreciudad;
    private String domicilio;
    private String telefono;
    private int pesosunidadcredito;
    private int clientemaximo;
    private int numempgerenteropa;
    private int interesadicional;
    private int tasainteresropa;
    private int tasainteresprestamos;
    private int interesmoratorioprestamos;
    private int montominimoboletos;
    private int montoboletos;
    private int salariominimo;
    private Date fecha;
    private int paridad;
    private int iva;
    private int tasainteresbancoppel;
    private int interesmoratoriobancoppel;

    /**
     * @return the ciudad
     */
    public int getCiudad() {
        return ciudad;
    }

    /**
     * @return the clientemaximo
     */
    public int getClientemaximo() {
        return clientemaximo;
    }

    /**
     * @return the domicilio
     */
    public String getDomicilio() {
        return domicilio;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @return the interesadicional
     */
    public int getInteresadicional() {
        return interesadicional;
    }

    /**
     * @return the interesmoratoriobancoppel
     */
    public int getInteresmoratoriobancoppel() {
        return interesmoratoriobancoppel;
    }

    /**
     * @return the interesmoratorioprestamos
     */
    public int getInteresmoratorioprestamos() {
        return interesmoratorioprestamos;
    }

    /**
     * @return the iva
     */
    public int getIva() {
        return iva;
    }

    /**
     * @return the montoboletos
     */
    public int getMontoboletos() {
        return montoboletos;
    }

    /**
     * @return the montominimoboletos
     */
    public int getMontominimoboletos() {
        return montominimoboletos;
    }

    /**
     * @return the nombreciudad
     */
    public String getNombreciudad() {
        return nombreciudad;
    }

    /**
     * @return the numempgerenteropa
     */
    public int getNumempgerenteropa() {
        return numempgerenteropa;
    }

    /**
     * @return the paridad
     */
    public int getParidad() {
        return paridad;
    }

    /**
     * @return the pesosunidadcredito
     */
    public int getPesosunidadcredito() {
        return pesosunidadcredito;
    }

    /**
     * @return the salariominimo
     */
    public int getSalariominimo() {
        return salariominimo;
    }

    /**
     * @return the tasainteresbancoppel
     */
    public int getTasainteresbancoppel() {
        return tasainteresbancoppel;
    }

    /**
     * @return the tasainteresprestamos
     */
    public int getTasainteresprestamos() {
        return tasainteresprestamos;
    }

    /**
     * @return the tasainteresropa
     */
    public int getTasainteresropa() {
        return tasainteresropa;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(int ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @param clientemaximo the clientemaximo to set
     */
    public void setClientemaximo(int clientemaximo) {
        this.clientemaximo = clientemaximo;
    }

    /**
     * @param domicilio the domicilio to set
     */
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @param interesadicional the interesadicional to set
     */
    public void setInteresadicional(int interesadicional) {
        this.interesadicional = interesadicional;
    }

    /**
     * @param interesmoratoriobancoppel the interesmoratoriobancoppel to set
     */
    public void setInteresmoratoriobancoppel(int interesmoratoriobancoppel) {
        this.interesmoratoriobancoppel = interesmoratoriobancoppel;
    }

    /**
     * @param interesmoratorioprestamos the interesmoratorioprestamos to set
     */
    public void setInteresmoratorioprestamos(int interesmoratorioprestamos) {
        this.interesmoratorioprestamos = interesmoratorioprestamos;
    }

    /**
     * @param iva the iva to set
     */
    public void setIva(int iva) {
        this.iva = iva;
    }

    /**
     * @param montoboletos the montoboletos to set
     */
    public void setMontoboletos(int montoboletos) {
        this.montoboletos = montoboletos;
    }

    /**
     * @param montominimoboletos the montominimoboletos to set
     */
    public void setMontominimoboletos(int montominimoboletos) {
        this.montominimoboletos = montominimoboletos;
    }

    /**
     * @param nombreciudad the nombreciudad to set
     */
    public void setNombreciudad(String nombreciudad) {
        this.nombreciudad = nombreciudad;
    }

    /**
     * @param numempgerenteropa the numempgerenteropa to set
     */
    public void setNumempgerenteropa(int numempgerenteropa) {
        this.numempgerenteropa = numempgerenteropa;
    }

    /**
     * @param paridad the paridad to set
     */
    public void setParidad(int paridad) {
        this.paridad = paridad;
    }

    /**
     * @param pesosunidadcredito the pesosunidadcredito to set
     */
    public void setPesosunidadcredito(int pesosunidadcredito) {
        this.pesosunidadcredito = pesosunidadcredito;
    }

    /**
     * @param salariominimo the salariominimo to set
     */
    public void setSalariominimo(int salariominimo) {
        this.salariominimo = salariominimo;
    }

    /**
     * @param tasainteresbancoppel the tasainteresbancoppel to set
     */
    public void setTasainteresbancoppel(int tasainteresbancoppel) {
        this.tasainteresbancoppel = tasainteresbancoppel;
    }

    /**
     * @param tasainteresprestamos the tasainteresprestamos to set
     */
    public void setTasainteresprestamos(int tasainteresprestamos) {
        this.tasainteresprestamos = tasainteresprestamos;
    }

    /**
     * @param tasainteresropa the tasainteresropa to set
     */
    public void setTasainteresropa(int tasainteresropa) {
        this.tasainteresropa = tasainteresropa;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("ciudad: ").append(getCiudad()).append("\n\r");
        sb.append("nombreciudad " + getNombreciudad() +"\n\r");
        sb.append("domicilio "+ getDomicilio() +"\n\r");
        sb.append("telefono " + getTelefono() +"\n\r");
        sb.append("pesosunidadcredito "+ getPesosunidadcredito()+"\n\r");
        sb.append("clientemaximo " + clientemaximo +"\n\r");
        sb.append("numempgerenteropa "+ numempgerenteropa +"\n\r");
        sb.append("interesadicional " +interesadicional +"\n\r");
        sb.append("tasainteresropa " + tasainteresropa+"\n\r");
        sb.append("tasainteresprestamos "+tasainteresprestamos+"\n\r");
        sb.append("interesmoratorioprestamos " +interesmoratorioprestamos+"\n\r");
        sb.append("montominimoboletos " +montominimoboletos+"\n\r");
        sb.append("montoboletos "+montoboletos+"\n\r");
        sb.append("salariominimo "+ salariominimo+"\n\r");
        sb.append("Fecha "+ fecha +"\n\r");
        sb.append("paridad "+ paridad+"\n\r");
        sb.append("iva "+iva+"\n\r");
        sb.append("tasainteresbancoppel " +tasainteresbancoppel+"\n\r");
        sb.append("interesmoratoriobancoppel "+interesmoratoriobancoppel+"\n\r");
        return  sb.toString();
    }
}
