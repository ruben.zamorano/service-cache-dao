package DTO;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "plazospagoinicial", namespace = "")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlazoPagoInicial {
    private String idu_plazo;
    private String num_mesesplazo;
    private String isobrepreciosimulado;
    private String respagoinicialpropuesto;
    private String restipopagoinicial;
    private String respagoinicialminimo;
    private String respagoinicialoperacionmaximo;
    private String respagoinicialoperacionminimo;
    private String respuntajefinal;
    private String resporcentajepagoinicial;
    private String resporcentajepagominimo;
    private String iporcentajesaturacion;
    private String iporcentajesaturaciontopemaximo;

    public String getIduPlazo() {
        return idu_plazo;
    }
    public void setIduPlazo(String idu_plazo) {
        this.idu_plazo = idu_plazo;
    }
    public String getNum_mesesplazo() {
        return num_mesesplazo;
    }
    public void setNum_mesesplazo(String num_mesesplazo) {
        this.num_mesesplazo = num_mesesplazo;
    }
    public String getIsobrepreciosimulado() {
        return isobrepreciosimulado;
    }
    public void setIsobrepreciosimulado(String isobrepreciosimulado) {
        this.isobrepreciosimulado = isobrepreciosimulado;
    }
    public String getRespagoinicialpropuesto() {
        return respagoinicialpropuesto;
    }
    public void setRespagoinicialpropuesto(String respagoinicialpropuesto) {
        this.respagoinicialpropuesto = respagoinicialpropuesto;
    }
    public String getRestipopagoinicial() {
        return restipopagoinicial;
    }
    public void setRestipopagoinicial(String restipopagoinicial) {
        this.restipopagoinicial = restipopagoinicial;
    }
    public String getRespagoinicialminimo() {
        return respagoinicialminimo;
    }
    public void setRespagoinicialminimo(String respagoinicialminimo) {
        this.respagoinicialminimo = respagoinicialminimo;
    }
    public String getRespagoinicialoperacionmaximo() {
        return respagoinicialoperacionmaximo;
    }
    public void setRespagoinicialoperacionmaximo(String respagoinicialoperacionmaximo) {
        this.respagoinicialoperacionmaximo = respagoinicialoperacionmaximo;
    }
    public String getRespagoinicialoperacionminimo() {
        return respagoinicialoperacionminimo;
    }
    public void setRespagoinicialoperacionminimo(String respagoinicialoperacionminimo) {
        this.respagoinicialoperacionminimo = respagoinicialoperacionminimo;
    }
    public String getRespuntajefinal() {
        return respuntajefinal;
    }
    public void setRespuntajefinal(String respuntajefinal) {
        this.respuntajefinal = respuntajefinal;
    }
    public String getResporcentajepagoinicial() {
        return resporcentajepagoinicial;
    }
    public void setResporcentajepagoinicial(String resporcentajepagoinicial) {
        this.resporcentajepagoinicial = resporcentajepagoinicial;
    }
    public String getResporcentajepagominimo() {
        return resporcentajepagominimo;
    }
    public void setResporcentajepagominimo(String resporcentajepagominimo) {
        this.resporcentajepagominimo = resporcentajepagominimo;
    }
    public String getPorcentajesaturacion() {
        return iporcentajesaturacion;
    }
    public void setPorcentajesaturacion(String iporcentajesaturacion) {
        this.iporcentajesaturacion = iporcentajesaturacion;
    }
    public String getPorcentajesaturaciontopemaximo() {
        return iporcentajesaturaciontopemaximo;
    }
    public void setPorcentajesaturaciontopemaximo(String iporcentajesaturaciontopemaximo) {
        this.iporcentajesaturaciontopemaximo = iporcentajesaturaciontopemaximo;
    }

    @Override
    public String toString() {
        return "PlazoPagoInicial [idu_plazo=" + idu_plazo + ", num_mesesplazo=" + num_mesesplazo
                + ", isobrepreciosimulado=" + isobrepreciosimulado + ", respagoinicialpropuesto="
                + respagoinicialpropuesto + ", restipopagoinicial=" + restipopagoinicial + ", respagoinicialminimo="
                + respagoinicialminimo + ", respagoinicialoperacionmaximo=" + respagoinicialoperacionmaximo
                + ", respagoinicialoperacionminimo=" + respagoinicialoperacionminimo + ", respuntajefinal="
                + respuntajefinal + ", resporcentajepagoinicial=" + resporcentajepagoinicial
                + ", resporcentajepagominimo=" + resporcentajepagominimo + ", iporcentajesaturacion="
                + iporcentajesaturacion + ", iporcentajesaturaciontopemaximo=" + iporcentajesaturaciontopemaximo + "]";
    }



}
