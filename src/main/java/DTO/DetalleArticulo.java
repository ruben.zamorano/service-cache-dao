package DTO;


/**
 * Objeto para almacenar el detalle de los articulos que esten incluidos en la orden
 *
 * @author Marco Antonio Olivas Olmeda
 * @version 1.0
 * @updated 02-ene-2015 11:30:07 a.m.
 */
public class DetalleArticulo
{
    /**
     * Valor del Item (sku)
     */
    private String sItem;
    /**
     * Valor del codigo (sku) en Coppel
     */
    private int iCodigo;
    /**
     * Valor del area Coppel
     */
    private int iArea;
    /**
     * Valor de la talla Coppel
     */
    private int iTalla;
    /**
     * Valor del departamento en Coppel
     */
    private int iDepartamento;
    /**
     * Valor de la clase en Coppel
     */
    private int iClase;
    /**
     * Valor de la familia en Coppel
     */
    private int iFamilia;
    /**
     * Valor del departamento - clase - familia del sku en Coppel. <b>NOTA: </b>Este dato sirve para hacer comparaciones
     * a nivel familia en el area de Muebles (<b>3</b>)
     */
    private int iDcf;
    /**
     * Valor de la marca del codigo. <b>NOTA: </b>Este valor esta limitado a <b>10 caracteres</b>
     */
    private String sMarca;
    /**
     * Nombre del articulo (codigo) <b>NOTA: </b>Este dato esta limitado a <b>40 caracteres</b>
     */
    private String sNombre;
    /**
     * Bandera para indicar si el articlo puede aplicar para descontar dinero electronico
     */
    private int iAplicaDineroElectronico;

    private int esCelular;

    public DetalleArticulo()
    {

    }

    /**
     * @return the iAplicaDineroElectronico
     */
    public int getiAplicaDineroElectronico()
    {
        return iAplicaDineroElectronico;
    }

    /**
     * @return the iArea
     */
    public int getiArea()
    {
        return iArea;
    }

    /**
     * @return the iClase
     */
    public int getiClase()
    {
        return iClase;
    }

    /**
     * @return the iCodigo
     */
    public int getiCodigo()
    {
        return iCodigo;
    }

    /**
     * @return the iDcf
     */
    public int getiDcf()
    {
        return iDcf;
    }

    /**
     * @return the iDepartamento
     */
    public int getiDepartamento()
    {
        return iDepartamento;
    }

    /**
     * @return the iFamilia
     */
    public int getiFamilia()
    {
        return iFamilia;
    }

    /**
     * @return the iTalla
     */
    public int getiTalla()
    {
        return iTalla;
    }

    /**
     * @return the sItem
     */
    public String getsItem()
    {
        return sItem;
    }

    /**
     * @return the sMarca
     */
    public String getsMarca()
    {
        return sMarca;
    }

    /**
     * @return the sNombre
     */
    public String getsNombre()
    {
        return sNombre;
    }

    /**
     * @param iAplicaDineroElectronico
     *            the iAplicaDineroElectronico to set
     */
    public void setiAplicaDineroElectronico(int iAplicaDineroElectronico)
    {
        this.iAplicaDineroElectronico = iAplicaDineroElectronico;
    }

    /**
     * @param iArea
     *            the iArea to set
     */
    public void setiArea(int iArea)
    {
        this.iArea = iArea;
    }

    /**
     * @param iClase
     *            the iClase to set
     */
    public void setiClase(int iClase)
    {
        this.iClase = iClase;
    }

    /**
     * @param iCodigo
     *            the iCodigo to set
     */
    public void setiCodigo(int iCodigo)
    {
        this.iCodigo = iCodigo;
    }

    /**
     * @param iDcf
     *            the iDcf to set
     */
    public void setiDcf(int iDcf)
    {
        this.iDcf = iDcf;
    }

    /**
     * @param iDepartamento
     *            the iDepartamento to set
     */
    public void setiDepartamento(int iDepartamento)
    {
        this.iDepartamento = iDepartamento;
    }

    /**
     * @param iFamilia
     *            the iFamilia to set
     */
    public void setiFamilia(int iFamilia)
    {
        this.iFamilia = iFamilia;
    }

    /**
     * @param iTalla
     *            the iTalla to set
     */
    public void setiTalla(int iTalla)
    {
        this.iTalla = iTalla;
    }

    /**
     * @param sItem
     *            the sItem to set
     */
    public void setsItem(String sItem)
    {
        this.sItem = sItem;
    }

    /**
     * @param sMarca
     *            the sMarca to set
     */
    public void setsMarca(String sMarca)
    {
        this.sMarca = sMarca;
    }

    /**
     * @param sNombre
     *            the sNombre to set
     */
    public void setsNombre(String sNombre)
    {
        this.sNombre = sNombre;
    }

    public int getEsCelular()
    {
        return esCelular;
    }

    public void setEsCelular(int esCelular)
    {
        this.esCelular = esCelular;
    }

    @Override
    public String toString()
    {
        return "DetalleArticulo [sItem=" + sItem + ", iCodigo=" + iCodigo + ", iArea=" + iArea + ", iTalla=" + iTalla + ", iDepartamento="
                + iDepartamento + ", iClase=" + iClase + ", iFamilia=" + iFamilia + ", iDcf=" + iDcf + ", sMarca=" + sMarca + ", sNombre=" + sNombre
                + ", iAplicaDineroElectronico=" + iAplicaDineroElectronico + ", esCelular=" + esCelular + "]";
    }
}