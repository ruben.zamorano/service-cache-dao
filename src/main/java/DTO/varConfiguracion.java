package DTO;


public class varConfiguracion {
    private int num_respuesta;
    private String des_mensaje;
    private String nom_variable;
    private String des_valor;

    public int getNum_respuesta() {
        return num_respuesta;
    }
    public void setNum_respuesta(int num_respuesta) {
        this.num_respuesta = num_respuesta;
    }
    public String getDes_mensaje() {
        return des_mensaje;
    }
    public void setDes_mensaje(String des_mensaje) {
        this.des_mensaje = des_mensaje;
    }
    public String getNom_variable() {
        return nom_variable;
    }
    public void setNom_variable(String nom_variable) {
        this.nom_variable = nom_variable;
    }
    public String getDes_valor() {
        return des_valor;
    }
    public void setDes_valor(String des_valor) {
        this.des_valor = des_valor;
    }
}
