/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package DAO;

import Base.Base;
import Base.ConstantsBase;
import Base.General;
import Base.TxManager;
import Config.DataBaseEnum;
import DTO.DetalleArticulo;
import DTO.type_fun_obtenerabonoropa;
import DTO.varConfiguracion;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author jsanchezm
 */
public class TiendavirtualDAO extends Base {

  private static final Logger LOG = Logger.getLogger(TiendavirtualDAO.class);
  private ConstantsBase constantsBase = new ConstantsBase();

  public DetalleArticulo fun_obtenerdatoscodigos(String sCodigo) throws Exception {
    DetalleArticulo detalle = null;

    Connection conn = null;
    Statement statement = null;
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
      statement = conn.createStatement();

      String query = "select  nom_item, num_codigo, num_area, num_talla, num_departamento,num_clase,num_familia "
          + ", num_dcf , nom_marca, nom_articulo  from fun_obtenerdatoscodigos('{" + sCodigo
          + "}');";
      LOG.debug(query);
      statement.setQueryTimeout(10);
      LOG.debug("fun_obtenerdatoscodigos Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_obtenerdatoscodigos Termina");

      rSet = statement.getResultSet();
      if (rSet != null) {
        if (rSet.next()) {
          detalle = new DetalleArticulo();
          detalle.setiCodigo(rSet.getInt("num_codigo"));

          if (detalle.getiCodigo() == -1) {
            throw new General("El formato del Item es incorrecto. Item: articulo.sCodigo",
                "General", "fun_obtenerdatoscodigos");
          } else if (detalle.getiCodigo() == -2) {
            throw new General(" El formato de codigo - area es incorrecto en el Item", "General",
                "fun_obtenerdatoscodigos");
          } else if (detalle.getiCodigo() == -3) {
            throw new General("No viene talla en Item", "General", "fun_obtenerdatoscodigos");
          } else if (detalle.getiCodigo() == -4) {
            throw new General("No se obtuvo informacion de codigo coppel", "General",
                "fun_obtenerdatoscodigos");
          } else if (detalle.getiCodigo() == -5) {
            throw new General("No existe la relacion area - codigo - talla en Coppel", "General",
                "fun_obtenerdatoscodigos");
          }

          detalle.setsItem(sCodigo);
          detalle.setiArea(rSet.getInt("num_area"));
          detalle.setiTalla(rSet.getInt("num_talla"));
          detalle.setiDepartamento(rSet.getInt("num_departamento"));
          detalle.setiClase(rSet.getInt("num_clase"));
          detalle.setiFamilia(rSet.getInt("num_familia"));
          detalle.setiDcf(rSet.getInt("num_dcf"));
          detalle.setsMarca(rSet.getString("nom_marca"));
          detalle.setsNombre(rSet.getString("nom_articulo"));
          detalle.setiAplicaDineroElectronico(0);

          LOG.debug(String.format("Regresa: %s", detalle.toString()));

        }
      }
    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_obtenerdatoscodigos", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }

    }
    return detalle;
  }

  public type_fun_obtenerabonoropa fun_obtenerabonoropa(int iTotalCredito, int iNuevoSaldoRopa,
                                                        int iPlazoRopa) throws Exception {
    type_fun_obtenerabonoropa detalle = new type_fun_obtenerabonoropa();

    Connection conn = null;
    Statement statement = null;
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
      statement = conn.createStatement();
      String query = "select   num_respuesta, des_mensaje, imp_abonoquincenal, imp_nuevoabonoquincenal "
          + "FROM fun_obtenerabonoropa(" + iTotalCredito + " ," + iNuevoSaldoRopa + " ,"
          + iPlazoRopa + ");";
      LOG.debug(query);
      statement.setQueryTimeout(10);
      statement.executeQuery(query);
      LOG.debug("fun_obtenerabonoropa Inicia");
      rSet = statement.getResultSet();
      LOG.debug("fun_obtenerabonoropa Termina");

      if (rSet != null) {
        if (rSet.next()) {
          detalle = new type_fun_obtenerabonoropa();
          detalle.setNum_respuesta(rSet.getInt("num_respuesta"));
          detalle.setDes_mensaje(rSet.getString("des_mensaje"));
          detalle.setImp_abonoquincenal(rSet.getInt("imp_abonoquincenal"));
          detalle.setImp_nuevoabonoquincenal(rSet.getInt("imp_nuevoabonoquincenal"));

          LOG.debug(detalle.toString());
        }
      }
    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_obtenerabonoropa", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
    return detalle;
  }

  public void fun_grabardatoscredito(int iArea, int iFolioOrden, int iCliente,
      String sSituacionEspecial, int iCausaSituacionEspecial, int iPlazoRopa,
      int iEnganchePropuesto, int iTotalContado, int iTotalCredito, int iSobreprecioInicial,
      int iAbono, int iNuevoSaldo, int iNuevoAbono, int iPuntajeFinalParametricoAltoRiesgo,
      int iParametricoCelulares, int iPorcentajeSaturacion, int iNuevoSaldo0,
      int iPagoUltimosDoceMeses, int iPrepuntaje, int iSobreprecioFinal, int iEngancheCliente,
      int iTasaInteresRopa, int iPagoRequeridoRopa, int iPagoRequeridoMuebles) throws Exception {

    Statement statement = null;
    Connection conn = null;
    int inum_Respuesta = 0;
    String desMensaje = "";
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
      statement = conn.createStatement();
      String query = "select   num_respuesta, des_mensaje " + "FROM fun_grabardatoscredito(" + iArea
          + " ," + iFolioOrden + " ," + iCliente + " ," + "'" + sSituacionEspecial + "'::bpchar,"
          + iCausaSituacionEspecial + " ," + iPlazoRopa + " ," + iEnganchePropuesto + " ,"
          + iTotalContado + " ," + iTotalCredito + " ," + iSobreprecioInicial + " ," + iAbono + " ,"
          + iNuevoSaldo + " ," + iNuevoAbono + " ," + iPuntajeFinalParametricoAltoRiesgo + " ,"
          + iParametricoCelulares + " ," + iPorcentajeSaturacion + " ," + iNuevoSaldo0 + " ,"
          + iPagoUltimosDoceMeses + " ," + iPrepuntaje + " ," + iSobreprecioFinal + " ,"
          + iEngancheCliente + "," + iTasaInteresRopa + "," + iPagoRequeridoRopa + ","
          + iPagoRequeridoMuebles + ");";
      LOG.debug(query);
      statement.setQueryTimeout(20);
      LOG.debug("fun_grabardatoscredito Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_grabardatoscredito Termina");
      rSet = statement.getResultSet();

      if (rSet != null) {
        if (rSet.next()) {
          inum_Respuesta = rSet.getInt("num_respuesta");
          desMensaje = rSet.getString("des_mensaje");
          LOG.debug(String.format("fun_grabardatoscredito num_respuesta: %d | desMensaje: %s",
              inum_Respuesta, desMensaje));
        }
      }
      if (inum_Respuesta != 1) {
        throw new General(desMensaje, "FAILED", "fun_grabardatoscredito");
      }
    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_grabardatoscredito", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
  }

  public int fun_obtenerabonomuebles(int iAbono18) throws Exception {
    Connection conn = null;
    Statement statement = null;
    int inum_Respuesta = 0;
    int imp_abonoquincenal = 0;
    String desMensaje = "";
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
      statement = conn.createStatement();
      String query = "select   num_respuesta, des_mensaje , imp_abonoquincenal"
          + " FROM fun_obtenerabonomuebles(" + iAbono18 + ");";
      LOG.debug(query);
      statement.setQueryTimeout(10);
      LOG.debug("fun_obtenerabonomuebles Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_obtenerabonomuebles Termina");

      rSet = statement.getResultSet();

      if (rSet != null) {
        if (rSet.next()) {
          inum_Respuesta = rSet.getInt("num_respuesta");
          desMensaje = rSet.getString("des_mensaje");
          imp_abonoquincenal = rSet.getInt("imp_abonoquincenal");

          LOG.debug(String.format(
              "fun_obtenerabonomuebles num_respuesta: %d | des_mensaje: %s | imp_abonoquincenal: %d",
              inum_Respuesta, desMensaje, imp_abonoquincenal));

        }
      }

      if (inum_Respuesta != 1) {
        throw new General(desMensaje, "FAILED", "fun_obtenerabonomuebles");
      }

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_obtenerabonomuebles", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
    return imp_abonoquincenal;
  }

  public void fun_borrarrechazoscredito(int folioOrden) throws Exception {
    Connection conn = null;
    Statement statement = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
      statement = conn.createStatement();
      String query = "select * from " + " fun_borrarrechazoscredito(" + folioOrden + "::bigint"
          + ");";
      LOG.debug(query);
      statement.setQueryTimeout(20);
      LOG.debug("fun_borrarrechazoscredito Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_borrarrechazoscredito Termina");
    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_borrarrechazoscredito", e);
      else {
        throw e;
      }
    } finally {
      if (statement != null) {
        statement.close();
      }
    }
  }

  public void fun_borrardatoscredito(int iFolioOrden) throws Exception {
    Statement statement = null;
    Connection conn = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
      statement = conn.createStatement();
      String query = "select * from " + " fun_borrardatoscredito(" + iFolioOrden + "::bigint"
          + ");";
      LOG.debug(query);
      statement.setQueryTimeout(20);
      LOG.debug("fun_borrardatoscredito Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_borrardatoscredito Termina");

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_borrardatoscredito", e);
      else {
        throw e;
      }
    } finally {
      if (statement != null) {
        statement.close();
      }
    }
  }

  public String fun_obtenerproximopagoropa(String strDate) throws Exception {
    Connection conn = null;
    Statement statement = null;
    String fechaAbono = "";
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
      statement = conn.createStatement();

      String query = "select fun_obtenerproximopagoropa AS dFechaAbono  from "
          + " fun_obtenerproximopagoropa('" + strDate + "');";
      LOG.debug(query);
      statement.setQueryTimeout(10);
      LOG.debug("fun_obtenerproximopagoropa Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_obtenerproximopagoropa Termina");
      rSet = statement.getResultSet();

      if (rSet != null) {
        if (rSet.next()) {
          fechaAbono = rSet.getString("dFechaAbono");
        }
      }
    } catch (Exception e) {
      LOG.error("Error en fun_obtenerproximopagoropa ", e);
      throw e;
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
    return fechaAbono;
  }

  public List<varConfiguracion> fun_obtenervariablesconfiguracion(int iConfiguracion)
      throws Exception {
    Connection conn = null;
    Statement statement = null;
    List<varConfiguracion> lstVar = null;
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
      statement = conn.createStatement();

      String query = "SELECT num_respuesta, des_mensaje, nom_variable, des_valor FROM fun_obtenervariablesconfiguracion("
          + iConfiguracion + "::integer);";
      LOG.debug(query);
      statement.setQueryTimeout(10);
      LOG.debug("fun_obtenervariablesconfiguracion Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_obtenervariablesconfiguracion Termina");
      rSet = statement.getResultSet();

      if (rSet != null) {
        lstVar = new ArrayList<varConfiguracion>();
        while (rSet.next()) {
          varConfiguracion var = new varConfiguracion();

          var.setNum_respuesta(rSet.getInt("num_respuesta"));
          var.setNom_variable(rSet.getString("nom_variable"));
          var.setDes_mensaje(rSet.getString("des_mensaje"));
          var.setDes_valor(rSet.getString("des_valor"));

          lstVar.add(var);

        }
      }
    } catch (Exception e) {
      LOG.error("Error en fun_obtenervariablesconfiguracion ", e);
      throw e;
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
    return lstVar;
  }

  /**
   * Metodo para obtener el maximo plazo para ordenes de muebles
   * 
   * @param iNumFolio
   * @return
   * @throws Exception
   */
  public int fun_consultarultimoplazomuebles(int iNumFolio) throws Exception {
    Statement statement = null;
    ResultSet rSet = null;
    Connection conn = null;

    int iNumPlazo = 0;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
      statement = conn.createStatement();

      String query = "SELECT fun_consultarultimoplazomuebles(" + iNumFolio + "::integer) as valor;";
      LOG.debug(query);
      statement.setQueryTimeout(10);
      LOG.debug("fun_consultarultimoplazomuebles Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_consultarultimoplazomuebles Termina");
      rSet = statement.getResultSet();

      if (rSet != null) {
        if (rSet.next()) {
          iNumPlazo = rSet.getInt("valor");
        }
      }
    } catch (Exception e) {
      LOG.error("Error en fun_consultarultimoplazomuebles ", e);
      throw e;
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }

    return iNumPlazo;
  }

  /**
   * Metodo para actualizar el maximo plazo obtenido de muebles
   * 
   * @param iNumFolioOrden
   * @param iNumPlazo
   * @throws Exception
   */
  public void fun_actualizarplazomueblescredito(int iNumFolioOrden, int iNumPlazo)
      throws Exception {
    Statement statement = null;
    Connection conn = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
      statement = conn.createStatement();

      String query = "SELECT fun_actualizarplazomueblescredito(" + iNumFolioOrden + "::integer, "
          + iNumPlazo + "::smallint);";
      LOG.debug(query);
      statement.setQueryTimeout(20);
      LOG.debug("fun_actualizarplazomueblescredito Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_actualizarplazomueblescredito Termina");

    } catch (Exception e) {
      LOG.error("Error en fun_actualizarplazomueblescredito ", e);
      throw e;
    } finally {
      if (statement != null) {
        statement.close();
      }
    }
  }

  public void fun_eliminardatosdineroelectronico(int iFolioOrden) throws Exception {
    Connection conn = null;
    Statement statement = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
      statement = conn.createStatement();
      String query = "select * from fun_eliminardatosdineroelectronico01(" + iFolioOrden
          + "::bigint" + ");";
      LOG.debug(query);
      statement.setQueryTimeout(20);
      LOG.debug("fun_eliminardatosdineroelectronico01 Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_eliminardatosdineroelectronico01 Termina");

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED",
            "fun_eliminardatosdineroelectronico01", e);
      else {
        throw e;
      }
    } finally {
      if (statement != null) {
        statement.close();
      }
    }

  }

  public void fun_grabardatosdineroelectronico(int iFolioOrden, int gastoderopa, int gastodemuebles,
      Integer saldoDEalafecha, Integer cliente) throws Exception {

    Statement statement = null;
    Connection conn = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
      statement = conn.createStatement();
      String query = "select * from " + " fun_grabardatosdineroelectronico(" + iFolioOrden
          + "::bigint," + gastoderopa + "::integer," + gastodemuebles + "::integer,"
          + saldoDEalafecha + "::integer," + cliente + "::integer);";
      LOG.debug(query);
      statement.setQueryTimeout(20);
      LOG.debug("fun_grabardatosdineroelectronico Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_grabardatosdineroelectronico Termina");

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED",
            "fun_grabardatosdineroelectronico", e);
      else {
        throw e;
      }
    } finally {
      if (statement != null) {
        statement.close();
      }
    }
  }
  
  public int fun_obteneridestado( String sEstado) throws Exception {

	    Statement statement = null;
	    Connection conn = null;
	    int iEstado=0;
	    ResultSet rSet = null;

	    try {
	      conn = TxManager.getConnection(DataBaseEnum.TIENDA_VIRTUAL);
	      statement = conn.createStatement();
	      String query = "select fun_obteneridestado('" + sEstado+"'::character varying);";
	      LOG.debug(query);
	      statement.setQueryTimeout(20);
	      LOG.debug("fun_obteneridestado Inicia");
	      statement.executeQuery(query);
	      LOG.debug("fun_obteneridestado Termina");
	      rSet = statement.getResultSet();
	      
	      if (rSet != null) {
	          if (rSet.next()) {
	            iEstado = rSet.getInt("fun_obteneridestado");
	          }
	        }
	      } catch (Exception e) {
	        LOG.error("Error en fun_obtenerproximopagoropa ", e);
	        throw e;
	      } finally {
	        if (rSet != null) {
	          rSet.close();
	        }
	        if (statement != null) {
	          statement.close();
	        }
	      }

	    
	    return iEstado;
	  }

}