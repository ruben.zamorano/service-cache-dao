/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package DAO;

import Base.Base;
import Base.ConstantsBase;
import Base.General;
import Base.TxManager;
import Config.DataBaseEnum;
import DTO.Cuenta;
import DTO.Factor;
import DTO.ParametricoCartera;
import org.apache.log4j.Logger;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;


/**
 *
 * @author jsanchezm
 */
public class TiendaCarteraDAO extends Base {

  private static final Logger LOG = Logger.getLogger(TiendaCarteraDAO.class);
  private ConstantsBase constantsBase = new ConstantsBase();

  public void fun_crobtenerdatoscliente(int numCliente, Cuenta ObjCuenta) throws Exception {
    Connection conn = null;
    CallableStatement callableStatement = null;
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.CARTERA);

      String procedure = "{ call fun_crobtenerdatoscliente(?::integer) }";
      callableStatement = conn.prepareCall(procedure);
      callableStatement.setInt(1, numCliente);

      LOG.debug("fun_crobtenerdatoscliente: " + callableStatement);
      LOG.debug("fun_crobtenerdatoscliente Inicia");

      callableStatement.setQueryTimeout(10);
      callableStatement.execute();
      LOG.debug("fun_crobtenerdatoscliente Termina");
      rSet = callableStatement.getResultSet();

      if (rSet != null) {
        if (rSet.next()) {
          ObjCuenta.setiTasaInteresRopa(rSet.getInt("prc_interesropa"));
          ObjCuenta.setiPlazoRopa(rSet.getInt("plazoropa"));

          LOG.debug("cuenta.iTasaInteresRopa = " + ObjCuenta.getiTasaInteresRopa());
          LOG.debug("cuenta.iPlazoRopa = " + ObjCuenta.getiPlazoRopa());
        }
      }

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_crobtenerdatoscliente", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (callableStatement != null) {
        callableStatement.close();
      }
    }
  }

  public Factor fun_consultarFactorPorPlazo() throws Exception {
    Factor factor = new Factor();
    Connection conn = null;
    CallableStatement callableStatement = null;
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.CARTERA);

      String procedure = "{ call fun_consultarFactorPorPlazo(?::smallint) }";
      callableStatement = conn.prepareCall(procedure);
      callableStatement.setInt(1, 1);

      LOG.debug("fun_consultarFactorPorPlazo Inicia");
      callableStatement.setQueryTimeout(10);
      callableStatement.execute();
      LOG.debug("fun_consultarFactorPorPlazo Termina");
      rSet = callableStatement.getResultSet();

      if (rSet != null) {
        while (rSet.next()) {
          factor.setsFactor12(rSet.getString("cFactorCliente12"));
          factor.setsFactor18(rSet.getString("cFactorCliente18"));
          factor.setsFactor24(rSet.getString("cFactorCliente24"));
          factor.setsFactor30(rSet.getString("cFactorCliente30"));
          factor.setsFactor36(rSet.getString("cFactorCliente36"));

          factor.setFactorEspecial12(rSet.getString("cFactorClienteEsp12"));
          factor.setFactorEspecial18(rSet.getString("cFactorClienteEsp18"));
          factor.setFactorEspecial24(rSet.getString("cFactorClienteEsp24"));
          factor.setFactorEspecial30(rSet.getString("cFactorClienteEsp30"));
          factor.setFactorEspecial36(rSet.getString("cFactorClienteEsp36"));

          factor.setFactorBasico12(rSet.getString("cFactorClienteBasico12"));
          factor.setFactorBasico18(rSet.getString("cFactorClienteBasico18"));
          factor.setFactorBasico24(rSet.getString("cFactorClienteBasico24"));
          factor.setFactorBasico30(rSet.getString("cFactorClienteBasico30"));
          factor.setFactorBasico36(rSet.getString("cFactorClienteBasico36"));
        }
        LOG.debug(factor.toString());
      }
    } catch (Exception e) {
      throw e;
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (callableStatement != null) {
        callableStatement.close();
      }
    }
    return factor;
  }

  public int fun_validarpuedecomprarcreditoinicial(int numCliente) throws Exception {
    Connection conn = null;
    CallableStatement callableStatement = null;
    ResultSet rSet = null;

    int regresa = 0;
    try {
      conn = TxManager.getConnection(DataBaseEnum.CARTERA);
      String procedure = "{ call fun_validarpuedecomprarcreditoinicial(?::integer) }";
      callableStatement = conn.prepareCall(procedure);
      callableStatement.setInt(1, numCliente);
      LOG.debug("fun_validarpuedecomprarcreditoinicial Inicia");
      callableStatement.setQueryTimeout(10);
      callableStatement.execute();
      LOG.debug("fun_validarpuedecomprarcreditoinicial Termina");
      rSet = callableStatement.getResultSet();

      if (rSet != null) {
        if (rSet.next()) {
          regresa = rSet.getInt("result");

        }
      }

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED",
            "fun_validarpuedecomprarcreditoinicial", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (callableStatement != null) {
        callableStatement.close();
      }
    }
    return regresa;
  }

  public int fun_validarsituacionespecial(int numCliente, int area) throws Exception {
    Connection conn = null;
    CallableStatement callableStatement = null;
    ResultSet rSet = null;

    int regresa = 0;

    try {
      conn = TxManager.getConnection(DataBaseEnum.CARTERA);

      String procedure = "{ call fun_validarsituacionespecial(?::integer,?::integer,?::integer,?::integer) }";
      callableStatement = conn.prepareCall(procedure);
      callableStatement.setInt(1, area);
      callableStatement.setInt(2, 1);
      callableStatement.setInt(3, 1);
      callableStatement.setInt(4, numCliente);
      LOG.debug("fun_validarsituacionespecial Inicia");
      callableStatement.setQueryTimeout(10);
      callableStatement.execute();
      LOG.debug("fun_validarsituacionespecial Termina");
      rSet = callableStatement.getResultSet();

      if (rSet != null) {
        if (rSet.next()) {
          regresa = rSet.getInt("result");
          LOG.debug("fun_validarsituacionespecial = " + regresa);
        }
      }

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_validarsituacionespecial",
            e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (callableStatement != null) {
        callableStatement.close();
      }
    }
    return regresa;
  }

  public ParametricoCartera fun_consultarparametricocn(int numCliente,String sPuntualidad) throws Exception {

    Connection conn = null;
    ResultSet rSet = null;
    CallableStatement callableStatement = null;

    ParametricoCartera parametrico = new ParametricoCartera();

    try {
      conn = TxManager.getConnection(DataBaseEnum.CARTERA);
      String procedure = "{ call fun_consultarparametricocn02(?::integer,?::character varying) }";
      callableStatement = conn.prepareCall(procedure);
      callableStatement.setInt(1, numCliente);
      callableStatement.setString(2, sPuntualidad);
      LOG.debug("fun_consultarparametricocn02 Inicia");
      callableStatement.setQueryTimeout(10);
      callableStatement.execute();
      LOG.debug("fun_consultarparametricocn02 Termina");
      rSet = callableStatement.getResultSet();

      if (rSet != null) {
        if (rSet.next()) {
          if (rSet.getInt("estado") == 0) {
            parametrico.setiNumPuntosParametricosCN(rSet.getInt("inum_puntosparcn"));
            parametrico.setiNumPuntosParametricosCNAux(rSet.getInt("inum_puntosparcnaux"));
            parametrico.setFfactorCMA(rSet.getDouble("ffactorcma"));
            parametrico.setIflagsanatudeuda(rSet.getShort("iflagsanatudeuda"));
            parametrico.setiOpcionSanaTuDeuda(rSet.getInt("iopcsanatudeuda"));
            parametrico.setiPrcPagoUltimaCompra(rSet.getInt("iprcpagoultimacompra"));
            
          } else {
            LOG.error("Ocurri? un error interno en la funci?n: " + rSet.getString("mensaje"));
          }
        }

        LOG.debug(parametrico.toString());
      }
    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_consultarparametricocn01",
            e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (callableStatement != null) {
        callableStatement.close();
      }
    }

    return parametrico;
  }
  
  public String fun_consultarmaecrcomplementocliente(int numCliente) throws Exception {
	    Connection conn = null;
	    CallableStatement callableStatement = null;
	    ResultSet rSet = null;

	    String regresa = "0";

	    try {
	      conn = TxManager.getConnection(DataBaseEnum.CARTERA);

	      String procedure = "{ call FUN_CONSULTARMAECRCOMPLEMENTOCLIENTE(?::bigint,9::smallint) }";
	      callableStatement = conn.prepareCall(procedure);
	      callableStatement.setInt(1, numCliente);
	      
	      LOG.debug("fun_consultarmaecrcomplementocliente Inicia");
	      callableStatement.setQueryTimeout(10);
	      callableStatement.execute();
	      LOG.debug("fun_consultarmaecrcomplementocliente Termina");
	      rSet = callableStatement.getResultSet();

	      if (rSet != null) {
	        if (rSet.next()) {
	          regresa = rSet.getString("rdes_valor1");
	          LOG.debug("fun_consultarmaecrcomplementocliente = " + regresa);
	        }
	      }

	    } catch (Exception e) {
	      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
	        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_consultarmaecrcomplementocliente",
	            e);
	      else {
	        throw e;
	      }
	    } finally {
	      if (rSet != null) {
	        rSet.close();
	      }
	      if (callableStatement != null) {
	        callableStatement.close();
	      }
	    }
	    return regresa;
	  }
  



}