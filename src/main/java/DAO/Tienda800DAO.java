/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package DAO;

import Base.Base;
import Base.ConstantsBase;
import Base.General;
import Base.TxManager;
import Config.DataBaseEnum;
import DTO.*;
import helpers.HelperCache;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author jsanchezm
 */
public class Tienda800DAO extends Base {

  private static final Logger LOG = Logger.getLogger(Tienda800DAO.class);
  private ConstantsBase constantsBase = new ConstantsBase();
  Connection conn = null;
  public static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
  public static final String CACHE_COLLECTION_NAME = "Tienda800DAO";



  public Tienda800DAO(Connection connection)
  {
      this.conn = connection;
  }


  ///////////////////////////// fun_obtenerdatosgndominio /////////////////////////////
  public typ_gndominio fun_obtenerdatosgndominio() throws Exception {
    typ_gndominio type = null;
    String query = "select  ciudad,\n" + "nombreciudad,\n" + "domicilio,\n" + "telefono,\n"
            + "pesosunidadcredito,\n" + "clientemaximo,\n" + "numempgerenteropa,\n"
            + "interesadicional,\n" + "tasainteresropa,\n" + "tasainteresprestamos,\n"
            + "interesmoratorioprestamos,\n" + "montominimoboletos,\n" + "montoboletos,\n"
            + "salariominimo,\n" + "fecha,\n" + "paridad,\n" + "iva,\n" + "tasainteresbancoppel,\n"
            + "interesmoratoriobancoppel from func_gnconsultardatosgndominio();";


    type = gson.fromJson(HelperCache.getCacheService(CACHE_COLLECTION_NAME,query), typ_gndominio.class);

    if (type == null) {
      //Connection conn = null;
      Statement statement = null;
      ResultSet rSet = null;

      try {
        //conn = DriverManager.getConnection("jdbc:postgresql://10.27.113.4:5432/tienda.0800", "sysinternet", "L1bZrOVq+U027xiPMsZ2qIBbF8JenWnoxDq8jG3xNVAjObCJ8NC1QA==");
        statement = conn.createStatement();

        LOG.debug(query);
        //statement.setQueryTimeout(10);
        LOG.debug("fun_obtenerdatosgndominio Inicia");
        statement.executeQuery(query);
        LOG.debug("fun_obtenerdatosgndominio Termino");
        rSet = statement.getResultSet();

        if (rSet != null) {
          if (rSet.next()) {
            type = new typ_gndominio();

            type.setCiudad(rSet.getInt("ciudad"));
            type.setNombreciudad(rSet.getString("nombreciudad"));
            type.setDomicilio("");
            type.setTelefono(rSet.getString("telefono"));
            type.setPesosunidadcredito((rSet.getInt("pesosunidadcredito")));
            type.setClientemaximo(rSet.getInt("clientemaximo"));
            type.setNumempgerenteropa(rSet.getInt("numempgerenteropa"));
            type.setInteresadicional(rSet.getInt("interesadicional"));
            type.setTasainteresropa(rSet.getInt("tasainteresropa"));
            type.setTasainteresprestamos(rSet.getInt("tasainteresprestamos"));
            type.setInteresmoratorioprestamos(rSet.getInt("interesmoratorioprestamos"));
            type.setMontominimoboletos(rSet.getInt("montominimoboletos"));
            type.setMontoboletos(rSet.getInt("montoboletos"));
            type.setSalariominimo(rSet.getInt("salariominimo"));
            type.setFecha(rSet.getDate("fecha"));
            type.setParidad(rSet.getInt("paridad"));
            type.setIva(rSet.getInt("iva"));
            type.setTasainteresbancoppel(rSet.getInt("tasainteresbancoppel"));
            type.setInteresmoratoriobancoppel(rSet.getInt("interesmoratoriobancoppel"));

            LOG.debug("La func_gnconsultardatosgndominio regresa type.getNombreciudad(): "
                    + type.getNombreciudad());
          }
        }
      } catch (Exception e) {
        if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
          throw new General("Se agoto el tiempo de espera", "FAILED",
                  "func_gnconsultardatosgndominio", e);
        else {
          throw e;
        }
      } finally {
        if (rSet != null) {
          rSet.close();
        }
        if (statement != null) {
          statement.close();
        }
      }

      //set cache
      HelperCache.setCacheService(CACHE_COLLECTION_NAME, query, gson.toJson(type));
      System.out.println(CACHE_COLLECTION_NAME + "-fun_obtenerdatosgndominio: " + gson.toJson(type));
    }
    else
    {
      System.out.println(CACHE_COLLECTION_NAME + "-fun_obtenerdatosgndominio-cache: " + gson.toJson(type));
    }
    return type;

  }

  ///////////////////////////////////////////////////////////////////////////////////////


  ///////////////////////////// gnborrardetallefactura02 /////////////////////////////
  public int gnborrardetallefactura02(int iFolioOrden, int iCodigo) throws Exception {
    int num_respuesta = 0;
    Statement statement = null;
    ResultSet rSet = null;
    Connection conn = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();

      String query = "select gnborrardetallefactura02 from gnborrardetallefactura02 (" + iFolioOrden
          + "," + iCodigo + ");";
      LOG.debug(query);
      statement.setQueryTimeout(20);
      LOG.debug("gnborrardetallefactura02 Inicia");
      statement.executeQuery(query);
      LOG.debug("gnborrardetallefactura02 Termino");
      rSet = statement.getResultSet();

      if (rSet != null) {
        if (rSet.next()) {
          num_respuesta = rSet.getInt("gnborrardetallefactura02");
          LOG.debug("gnborrardetallefactura02: " + num_respuesta);
        }
      }
    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "gnborrardetallefactura02", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
    return num_respuesta;
  }
  ///////////////////////////////////////////////////////////////////////////////////////


  ///////////////////////////// fun_grabarfacturadetalleparametricos01 /////////////////////////////
  /**
   * @param iCaja
   * @param iFolio
   * @param iCodigo
   * @param iDcf
   * @param iCantidad
   * @param sDescripcion
   * @param sFormaEntrega ejem: 'B'
   * @param iImporte
   * @param iPrecio
   * @param iFlagVale
   * @param sListaPlazos  ejem: '1|2|3|7|9' son los plazos que devuelve el
   *                      servicio de tasas de interes
   * @param iCveSort
   * @param iTipoCiudad
   */
  public int fun_grabarfacturadetalleparametricos01(int iCaja, int iFolio, int iCodigo, int iDcf,
      int iCantidad, String sDescripcion, int iFlagRiesgo, String sFormaEntrega, int iImporte,
      int iPrecio, int iFlagVale, String sListaPlazos, int iCveSort, int iTipoCiudad, String jsonWS)
      throws Exception {

    Statement statement = null;
    Connection conn = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();

      String query = "select fun_grabarfacturadetalleparametricos01(" + iCaja + "::smallint,"
          + iFolio + "::integer," + iCodigo + "::integer," + "'" + sDescripcion
          + "'::character varying," + iPrecio + "::integer," + iCantidad + "::smallint," + iImporte
          + "::integer," + iFlagRiesgo + "::smallint," + "'" + sFormaEntrega + "'::character,"
          + iDcf + "::integer," + iFlagVale + "::smallint," + "'" + sListaPlazos
          + "'::character varying,"// ejem: 1|2|3|7|9
          + iCveSort + "::smallint," + iTipoCiudad + "::integer," + "'" + jsonWS + "'::json" + ");";

      LOG.debug(query);
      statement.setQueryTimeout(20);
      LOG.debug("fun_grabarfacturadetalleparametricos01 Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_grabarfacturadetalleparametricos01 Termina");

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED",
            "fun_grabarfacturadetalleparametricos01", e);
      else {
        throw e;
      }
    } finally {
      if (statement != null) {
        statement.close();
      }
    }
    return 1;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  public int fun_verificarrestriccionesde(int iCodigo) throws Exception {
    int num_respuesta = 0;

    Connection conn = null;
    Statement statement = null;
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();

      String query = "select  * from  fun_verificarrestriccionesde(" + iCodigo + "::integer);";
      LOG.debug(query);
      statement.setQueryTimeout(10);
      LOG.debug("fun_verificarrestriccionesde Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_verificarrestriccionesde Termina");
      rSet = statement.getResultSet();
      if (rSet != null) {
        if (rSet.next()) {
          num_respuesta = rSet.getInt("fun_verificarrestriccionesde");
          LOG.debug("fun_verificarrestriccionesde: " + num_respuesta);
        }
      }
    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_verificarrestriccionesde",
            e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
    return num_respuesta;
  }

  public String gncalculardiadepago(int i, int dia, int mes, int anio) throws Exception {
    Connection conn = null;
    Statement statement = null;
    String fechaAbono = "";
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();
      String query = "SELECT diapago, mespago, aniopago FROM gncalculardiadepago('" + i
          + "'::smallint,'" + dia + "'::smallint,'" + mes + "'::smallint,'" + anio
          + "'::smallint);";
      LOG.debug(query);
      statement.setQueryTimeout(10);
      LOG.debug("gncalculardiadepago Inicia");
      statement.executeQuery(query);
      LOG.debug("gncalculardiadepago Termina");

      rSet = statement.getResultSet();
      if (rSet != null) {
        if (rSet.next()) {
          int mes1 = rSet.getInt("mespago");
          int dia1 = rSet.getInt("diapago");

          fechaAbono = rSet.getString("aniopago");
          fechaAbono = fechaAbono + "-";
          if (mes1 < 10) {
            fechaAbono = fechaAbono + "0";
          }
          fechaAbono = fechaAbono + rSet.getString("mespago");
          fechaAbono = fechaAbono + "-";
          if (dia1 < 10) {
            fechaAbono = fechaAbono + "0";
          }
          fechaAbono = fechaAbono + rSet.getString("diapago");
          LOG.debug("gncalculardiadepago regresa = " + fechaAbono);
        }
      }
    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "gncalculardiadepago", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
    return fechaAbono;
  }

  public void fun_generardatosventa(int folioOrden, int iFlagPrimerCalculo, Cuenta objCuenta,
                                    Totales objTotales, CreditoMuebles objCreditoMuebles, Factor factor, String iFlagPlazo)
      throws Exception {
    Statement statement = null;
    ResultSet rSet = null;
    Connection conn = null;

    try {
      String[] factores = factor.getFactorCliente(Integer.toString(objCuenta.getiFlagPlazo()));

      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();

      String query = "" + "SELECT " + "totalcontado, totalcredito, "
          + "isobreprecio, pagoinicialpropuesto, "
          + "puntajefinalparcel, puntajefinalparaltoriesgo," + "isetramita, iflagctejoven, iabono,"
          + "isaturacion, nuevosaldo, pagoultimosdocemeses " + " FROM fun_generardatosventa03("
          + objTotales.getiTotalContadoMuebles() + "::integer, " + "1::integer, " + folioOrden
          + "::integer, " + iFlagPlazo + "::smallint, " + objCuenta.getiSaldoClienteFactorizado()
          + "::integer, " + objCuenta.getiLineaRealDeCredito() + "::integer, "
          + objCuenta.getiFlagLineaCreditoEspecial() + "::smallint, "
          + objCuenta.getiPagoUltimosDoceMeses() + "::integer, "
          + objCuenta.getiCausaSituacionEspecial() + "::smallint, " + "'"
          + objCuenta.getsSituacionEspecial() + "'::character varying, " + "'"
          + factores[Factor.CLIENTE_NORMAL] + "'::character varying, " + "'"
          + factores[Factor.CLIENTE_ESPECIAL] + "'::character varying, " + "'"
          + factores[Factor.CLIENTE_BASICO] + "'::character varying);";

      LOG.debug(query);
      statement.setQueryTimeout(10);
      LOG.debug("fun_generardatosventa03 Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_generardatosventa03 Termina");

      rSet = statement.getResultSet();

      if (rSet != null) {
        if (rSet.next()) {
          objCreditoMuebles.setiTotalContado(rSet.getInt("totalcontado"));
          objCreditoMuebles.setiTotalCredito(rSet.getInt("totalcredito"));
          objCreditoMuebles.setiSobrePrecio(rSet.getInt("isobreprecio"));
          objCreditoMuebles.setiEnganchePropuestoFinal(rSet.getInt("pagoinicialpropuesto"));
          objCreditoMuebles.setiPuntosFinalCelulares(rSet.getInt("puntajefinalparcel"));
          objCreditoMuebles.setiPuntosFinalAltoRiesgo(rSet.getInt("puntajefinalparaltoriesgo"));
          objCreditoMuebles.setiTramitarCredito(rSet.getInt("isetramita"));
          objCreditoMuebles.setiFlagClienteJoven(rSet.getInt("iflagctejoven"));
          objCreditoMuebles.setiAbonoMensual(rSet.getInt("iabono"));
          objCreditoMuebles.setiPorcentajeSaturacion(rSet.getInt("isaturacion"));
          objCreditoMuebles.setiNuevoSaldo(rSet.getInt("nuevosaldo"));
          objCreditoMuebles.setiPagoUltimosDoceMeses(rSet.getInt("pagoultimosdocemeses"));

          LOG.debug("fun_generardatosventa03: " + objCreditoMuebles.toString());
        }
      }
    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_generardatosventa03", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
  }

  public SysFlags obtenerSysFlags() throws Exception {

    Statement statement = null;
    SysFlags sysFlags = new SysFlags();
    Connection conn = null;
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      LOG.debug("obtenerSysFlags Inicia");
      statement = conn.createStatement();
      String query = "SELECT gnconsultarflag( '0', '152' );";
      LOG.debug(query);

      statement.executeQuery(query);

      rSet = statement.getResultSet();

      if (rSet != null && rSet.next())
        sysFlags.setFlagCreditoInicial(rSet.getInt("gnconsultarflag"));

      statement = conn.createStatement();
      query = "SELECT gnconsultarflag( '0', '46' );";
      LOG.debug(query);
      statement.executeQuery(query);

      rSet = statement.getResultSet();
      if (rSet != null && rSet.next())
        sysFlags.setFlagEngancheIlustrativo(rSet.getInt("gnconsultarflag"));

      statement = conn.createStatement();
      query = "SELECT gnconsultarflag( 'M', '135' );";
      LOG.debug(query);
      statement.executeQuery(query);

      rSet = statement.getResultSet();
      if (rSet != null && rSet.next())
        sysFlags.setFlag18pi12(rSet.getInt("gnconsultarflag"));

      statement = conn.createStatement();
      query = "SELECT gnconsultarflag( '0', '178' );";
      LOG.debug(query);
      statement.executeQuery(query);

      rSet = statement.getResultSet();
      if (rSet != null && rSet.next())
        sysFlags.setFlagParamCnParamVentas(rSet.getInt("gnconsultarflag"));

      statement = conn.createStatement();
      query = "SELECT gnconsultarflag( '0', '177' );";
      LOG.debug(query);
      statement.executeQuery(query);

      rSet = statement.getResultSet();

      if (rSet != null && rSet.next())
        sysFlags.setFlagParametricoCN(rSet.getInt("gnconsultarflag"));

      statement = conn.createStatement();
      query = "SELECT gnconsultarflag( '0', '169' );";
      LOG.debug(query);
      statement.executeQuery(query);

      rSet = statement.getResultSet();

      if (rSet != null && rSet.next())
        sysFlags.setFlagRecalibracion(rSet.getInt("gnconsultarflag"));

      statement = conn.createStatement();
      query = "SELECT gnconsultarflag( '0', '98' );";
      LOG.debug(query);
      statement.executeQuery(query);

      rSet = statement.getResultSet();

      if (rSet != null && rSet.next())
        sysFlags.setFlagRecalibracionRopa(rSet.getInt("gnconsultarflag"));

      statement = conn.createStatement();
      query = "SELECT cipservidor FROM fun_consultarsysservidores(229);";
      LOG.debug(query);
      statement.executeQuery(query);

      rSet = statement.getResultSet();

      if (rSet != null && rSet.next())
        sysFlags.setIpPagoInicial(rSet.getString("cipservidor"));

      statement = conn.createStatement();
      query = "SELECT cipservidor, ipuerto FROM fun_consultarsysservidores(230);";
      LOG.debug(query);
      statement.executeQuery(query);

      rSet = statement.getResultSet();

      if (rSet != null && rSet.next()) {
        sysFlags.setcIpServidor(rSet.getString("cipservidor"));
        sysFlags.setiPuerto(rSet.getString("ipuerto"));
      }

      statement = conn.createStatement();
      query = "SELECT gnconsultarflag( '0', '197' );";
      LOG.debug(query);
      statement.executeQuery(query);

      rSet = statement.getResultSet();

      if (rSet != null && rSet.next())
        sysFlags.setFlagCMA(rSet.getInt("gnconsultarflag"));

      LOG.debug("obtenerSysFlags Termina");

      LOG.debug(sysFlags.toString());

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "obtenerSysFlags", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }

    return sysFlags;
  }

  public void obtenerUrlReglasCredito(SysFlags sysFlags) throws Exception {

    Statement statement = null;
    Connection conn = null;
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();
      String query = "SELECT cipservidor, ipuerto FROM fun_consultarsysservidores(170);";
      LOG.debug(query);
      LOG.debug("obtenerUrlReglasCredito Inicia");
      statement.executeQuery(query);
      LOG.debug("obtenerUrlReglasCredito Termina");
      rSet = statement.getResultSet();

      if (rSet != null && rSet.next()) {
        sysFlags.setcIpServidor(rSet.getString("cipservidor"));
        sysFlags.setiPuerto(rSet.getString("ipuerto"));
      }

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "obtenerSysFlags", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
  }

  public RespuestaCalculoTotalesPagoInicial fun_CalcularTotalesPagoInicialCentralizada(int iCaja,
      int iFolioOrden, int iPagoDineroElectronico, int iTienda, int iPagoInicial, int plazoVenta,
      int iFlag18pi12) throws Exception {

    Statement statement = null;
    ResultSet rSet = null;
    Connection conn = null;

    RespuestaCalculoTotalesPagoInicial respuestaTotales = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();

      String query = "SELECT lPagoDineroeTotal, TotalContadoSinAjuste, iVtaAccesoriosCel, "
          + "iEngancheIlustrativo, iNumDepartamento, iimporte, SListaSku, SListaDcf, sImporteArt, "
          + "isobrepreciosimulado, " + "iFlagCMA, iresConstantePagoInicialSimuladoCMA "
          + "FROM fun_CalcularTotalesPagoInicialCentralizada02(" + iCaja + "::smallint, "
          + iFolioOrden + "::integer, " + iPagoDineroElectronico + "::bigint, " + "800::smallint, "
          + iPagoInicial + "::integer, " + "1::smallint, " + plazoVenta + "::smallint,"
          + iFlag18pi12 + "::smallint" + ");";

      LOG.debug(query);
      LOG.debug("fun_CalcularTotalesPagoInicialCentralizada Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_CalcularTotalesPagoInicialCentralizada Termina");
      rSet = statement.getResultSet();

      if (rSet != null && rSet.next()) {
        respuestaTotales = new RespuestaCalculoTotalesPagoInicial(rSet.getLong("lPagoDineroETotal"),
            rSet.getInt("TotalContadoSinAjuste"), rSet.getInt("iVtaAccesoriosCel"),
            rSet.getInt("iEngancheIlustrativo"), rSet.getInt("iNumDepartamento"),
            rSet.getInt("iimporte"), rSet.getString("SListaSku"), rSet.getString("SListaDcf"),
            rSet.getString("sImporteArt"), rSet.getInt("iSobrePrecioSimulado"), 0, 0, 0,
            rSet.getInt("iFlagCMA"), rSet.getInt("iresConstantePagoInicialSimuladoCMA"));
      }

      LOG.debug(respuestaTotales.toString());

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED",
            "fun_CalcularTotalesPagoInicialCentralizada01", e);
      else {
        throw new General("Ocurrio un error al calcular el pago inicial, #15001", "FAILED",
            "fun_CalcularTotalesPagoInicialCentralizada01", e);
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }

    return respuestaTotales;
  }

  public int fun_obtenerdepskumayorvalor01(int iCaja, int iFolio, String iPlazoVenta)
      throws Exception {
    Statement statement = null;
    ResultSet rSet = null;
    Connection conn = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();
      String query = "SELECT COALESCE(MAX(departamento), 0) AS iNumDepartamento FROM fun_obtenerdepskumayorvalor01("
          + iCaja + "::smallint, " + iFolio + "::integer, " + iPlazoVenta + "::smallint)";
      LOG.debug(query);
      LOG.debug("fun_obtenerdepskumayorvalor01 Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_obtenerdepskumayorvalor01 Termina");
      rSet = statement.getResultSet();

      if (rSet != null && rSet.next())
        return rSet.getInt("iNumDepartamento");

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "obtenerSysFlags", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }

    return 0;
  }

  public RespuestaCalculoPiArticuloCentralizada fun_GrabarCalculoPIArticuloCentralizada02(
      DatosEntradaMuebles solicitudPi, RowMuebles respuestaPi, int iPagoDejaCliente,
      int iflag18pi12) throws Exception {

    Statement statement = null;
    ResultSet rSet = null;
    Connection conn = null;

    try {

      RespuestaCalculoPiArticuloCentralizada respuestaCalculo = null;
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();
      String query = "SELECT iTotalContado, iPrecioCredito, iSobrePrecio, Vporcentajeinteres FROM "
          + "fun_grabarcalculopiarticulocentralizada02 " + "(1::smallint,"// caja
          + solicitudPi.getIFolio() + "::integer, " + "800::smallint, "
          + solicitudPi.getcPlazoVenta() + "::integer, "
          + respuestaPi.getPlazospagoinicial().getRespagoinicialpropuesto() + "::integer, "
          + respuestaPi.getPlazospagoinicial().getResporcentajepagoinicial() + "::integer, "
          + respuestaPi.getPlazospagoinicial().getResporcentajepagominimo() + "::integer, "
          + iPagoDejaCliente + "::integer, " // pago deja cliente
          + "0::integer, " // flag primer calculo
          + solicitudPi.getLPagoDineroE() + "::integer, " + "2::smallint, " // flag
          // sobreprecio
          + iflag18pi12 + "::smallint" + ");";

      LOG.debug(query);
      LOG.debug("fun_GrabarCalculoPIArticuloCentralizada02 Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_GrabarCalculoPIArticuloCentralizada02 Termina");
      rSet = statement.getResultSet();

      if (rSet != null && rSet.next()) {
        respuestaCalculo = new RespuestaCalculoPiArticuloCentralizada(rSet.getInt("iTotalContado"),
            rSet.getInt("iPrecioCredito"), rSet.getInt("iSobrePrecio"),
            rSet.getInt("Vporcentajeinteres"));
      }

      LOG.debug(respuestaCalculo.toString());

      return respuestaCalculo;

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED",
            "fun_grabarcalculopiarticulocentralizada02", e);
      else {
        throw new General("Ocurri? un error al calcular el pago inicial. #15002", "FAILED",
            "fun_grabarcalculopiarticulocentralizada02", e);
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
  }

  public void fun_insertarCertificacionPICentralizada(int folio, String ResTipoPagoInicial,
      int flagCreditoInicial, int iNumPuntosParametricosCN, int iNumPuntosParametricosCNAux,
      Cuenta objCuenta, Factor factor) throws Exception {

    Statement statement = null;
    Connection conn = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();
      String query = "SELECT estado, mensaje FROM "
          + "fun_insertarcertificacionpicentralizada('800'::integer, '1'::integer, '1'::integer, '"
          + folio + "'::integer, '1'::integer, '" + ResTipoPagoInicial
          + "'::integer, '2'::integer, '" + objCuenta.getiPuntosAltoRiesgo()
          + "'::text, 'ENTRADA'::text )";

      LOG.debug(query);
      statement.executeQuery(query);

      // 18
      statement = conn.createStatement();
      query = "SELECT estado, mensaje FROM "
          + "fun_insertarcertificacionpicentralizada('800'::integer, '1'::integer, '1'::integer, '"
          + folio + "'::integer, '1'::integer, '" + ResTipoPagoInicial
          + "'::integer, '18'::integer, '" + objCuenta.getiPrepuntaje()
          + "'::text, 'ENTRADA'::text )";
      LOG.debug(query);
      statement.executeQuery(query);

      // 20
      statement = conn.createStatement();
      query = "SELECT estado, mensaje FROM "
          + "fun_insertarcertificacionpicentralizada('800'::integer, '1'::integer, '1'::integer, '"
          + folio + "'::integer, '1'::integer, '" + ResTipoPagoInicial
          + "'::integer, '20'::integer, '" + flagCreditoInicial + "'::text, 'ENTRADA'::text )";
      LOG.debug(query);
      statement.executeQuery(query);

      // 29
      statement = conn.createStatement();
      query = "SELECT estado, mensaje FROM "
          + "fun_insertarcertificacionpicentralizada('800'::integer, '1'::integer, '1'::integer, '"
          + folio + "'::integer, '1'::integer, '" + ResTipoPagoInicial
          + "'::integer, '29'::integer, '" + objCuenta.getsFechaPrimeraCompra()
          + "'::text, 'ENTRADA'::text )";
      LOG.debug(query);
      statement.executeQuery(query);

      // 236
      statement = conn.createStatement();
      query = "SELECT estado, mensaje FROM "
          + "fun_insertarcertificacionpicentralizada('800'::integer, '1'::integer, '1'::integer, '"
          + folio + "'::integer, '1'::integer, '" + ResTipoPagoInicial
          + "'::integer, '236'::integer, '" + iNumPuntosParametricosCN
          + "'::text, 'ENTRADA'::text )";
      LOG.debug(query);
      statement.executeQuery(query);

      // 237
      statement = conn.createStatement();
      query = "SELECT estado, mensaje FROM "
          + "fun_insertarcertificacionpicentralizada('800'::integer, '1'::integer, '1'::integer, '"
          + folio + "'::integer, '1'::integer, '" + ResTipoPagoInicial
          + "'::integer, '237'::integer, '" + iNumPuntosParametricosCNAux
          + "'::text, 'ENTRADA'::text )";
      LOG.debug(query);
      statement.executeQuery(query);

      // 241
      statement = conn.createStatement();
      query = "SELECT estado, mensaje FROM "
          + "fun_insertarcertificacionpicentralizada('800'::integer, '1'::integer, '1'::integer, '"
          + folio + "'::integer, '1'::integer, '" + ResTipoPagoInicial
          + "'::integer, '241'::integer, '" + factor.getsFactor12() + "'::text, 'ENTRADA'::text )";
      LOG.debug(query);
      statement.executeQuery(query);

      // 242
      statement = conn.createStatement();
      query = "SELECT estado, mensaje FROM "
          + "fun_insertarcertificacionpicentralizada('800'::integer, '1'::integer, '1'::integer, '"
          + folio + "'::integer, '1'::integer, '" + ResTipoPagoInicial
          + "'::integer, '242'::integer, '" + factor.getsFactor18() + "'::text, 'ENTRADA'::text )";
      LOG.debug(query);
      statement.executeQuery(query);

      // 243
      statement = conn.createStatement();
      query = "SELECT estado, mensaje FROM "
          + "fun_insertarcertificacionpicentralizada('800'::integer, '1'::integer, '1'::integer, '"
          + folio + "'::integer, '1', '" + ResTipoPagoInicial + "'::integer, '243'::integer, '"
          + factor.getsFactor24() + "'::text, 'ENTRADA'::text )";
      LOG.debug(query);
      statement.executeQuery(query);

      // 244
      statement = conn.createStatement();
      query = "SELECT estado, mensaje FROM "
          + "fun_insertarcertificacionpicentralizada('800'::integer, '1'::integer, '1'::integer, '"
          + folio + "'::integer, '1'::integer, '" + ResTipoPagoInicial
          + "'::integer, '244'::integer, '" + factor.getFactorEspecial12()
          + "'::text, 'ENTRADA'::text )";
      LOG.debug(query);
      statement.executeQuery(query);

      // 245
      statement = conn.createStatement();
      query = "SELECT estado, mensaje FROM "
          + "fun_insertarcertificacionpicentralizada('800'::integer, '1'::integer, '1'::integer, '"
          + folio + "'::integer, '1'::integer, '" + ResTipoPagoInicial
          + "'::integer, '245'::integer, '" + factor.getFactorEspecial18()
          + "'::text, 'ENTRADA'::text )";
      LOG.debug(query);
      statement.executeQuery(query);

      // 246
      statement = conn.createStatement();
      query = "SELECT estado, mensaje FROM "
          + "fun_insertarcertificacionpicentralizada('800'::integer, '1'::integer, '1'::integer, '"
          + folio + "'::integer, '1'::integer, '" + ResTipoPagoInicial
          + "'::integer, '246'::integer, '" + factor.getFactorEspecial24()
          + "'::text, 'ENTRADA'::text )";
      LOG.debug(query);
      statement.executeQuery(query);

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "obtenerSysFlags", e);
      else {
        throw e;
      }
    } finally {
      if (statement != null) {
        statement.close();
      }
    }
  }

  /**
   * Funcion de tienda numero
   * 
   * @param
   * @param xmlEntrada
   * @throws General
   * @throws SQLException
   */
  public void fun_grabarcertificacionfinalpicentralizada(String xmlEntrada)
      throws General, SQLException {
    Statement statement = null;
    ResultSet rSet = null;
    Connection conn = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();
      String query = String.format(
          "SELECT estado, mensaje FROM fun_grabarcertificacionfinalpicentralizada07('%s');",
          xmlEntrada);

      LOG.debug(query);
      LOG.debug("fun_grabarcertificacionfinalpicentralizada06 Inicia");
      statement.executeQuery(query);
      LOG.debug("fun_grabarcertificacionfinalpicentralizada06 Termina");

      rSet = statement.getResultSet();

      if (rSet != null && rSet.next()) {
        if (rSet.getInt("estado") != 0) {
          LOG.error("No se pudo insertar la certificacion de los datos: #15004 "
              + rSet.getString("mensaje"));
        }
      }

    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED",
            "fun_grabarcertificacionfinalpicentralizada06", e);
      else {
        throw new General("Ocurri? un error al calcular el pago inicial. #15003", "FAILED",
            "fun_grabarcertificacionfinalpicentralizada06", e);
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
  }



  /**
   * M?todo para obtener la ip del servidor de tramites.
   * 
   * @param  - Conexion a la tienda 800 inicializada anteriormente
   * @return String - Cadena con la Ip del servidor de tramite.
   * @throws Exception
   */
  /////////////////////////////  obtenerIpTramite  /////////////////////////////
  public String obtenerIpTramite() throws Exception {
    Statement statement = null;
    //Connection conn = null;
    ResultSet rSet = null;
    String query = "SELECT cipservidor, ipuerto FROM fun_consultarsysservidores(248);";


    String responseIp = HelperCache.getCacheService(CACHE_COLLECTION_NAME, query);

    if (responseIp != null)
    {
      System.out.println(CACHE_COLLECTION_NAME + "-obtenerIpTramite-cache: " +responseIp);
      return responseIp;
    }
    else
    {
      try {
        //conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
        statement = conn.createStatement();

        LOG.debug("Obtener el IpTramite = " + query);
        LOG.debug("obtenerIpTramite Inicia");
        statement.executeQuery(query);
        LOG.debug("obtenerIpTramite Termina");
        rSet = statement.getResultSet();

        if (rSet != null && rSet.next()) {
          if (rSet.getString("cipservidor") != null) {
            LOG.debug("IpTramite: " + rSet.getString("cipservidor"));
            System.out.println(CACHE_COLLECTION_NAME + "-obtenerIpTramite: " + rSet.getString("cipservidor"));
            HelperCache.setCacheService(CACHE_COLLECTION_NAME,query, rSet.getString("cipservidor"));
            return rSet.getString("cipservidor");
          } else {
            throw new General("No se encontro el IP de Tr?mite", "FAILED",
                    "fun_consultarsysservidores");
          }
        }

      } catch (Exception e) {
        if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
          throw new General("Se agoto el tiempo de espera", "FAILED", "fun_consultarsysservidores",
                  e);
        else {
          throw e;
        }
      } finally {
        if (rSet != null) {
          rSet.close();
        }
        if (statement != null) {
          statement.close();
        }
      }

      return "";
    }
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  public int[] obtenermuDcf(int dcf) throws Exception {
    Statement statement = null;
    int[] res = new int[2];
    Connection conn = null;
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();
      String query = "SELECT plazo18 as flagplazo18 ,riesgo as flagriesgo FROM muobtenermudcf("
          + dcf + "::integer" + ");";

      LOG.debug(query);
      LOG.debug("obtenermuDcf Inicia");
      statement.executeQuery(query);
      LOG.debug("obtenermuDcf Termina");

      rSet = statement.getResultSet();

      if (rSet != null && rSet.next()) {
        do {
          res[0] = rSet.getInt("flagplazo18");
          res[1] = rSet.getInt("flagriesgo");
        } while (rSet.next());
      }

      return res;
    } catch (Exception e) {
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "muobtenermudcf", e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
  }

  public int fun_validararticulocelular(String dcfList) throws Exception {
    Statement statement = null;
    int res = 0;
    Connection conn = null;
    ResultSet rSet = null;

    try {
      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
      statement = conn.createStatement();
      String query = "select fun_validararticulocelular from fun_validararticulocelular('" + dcfList
          + "');";

      LOG.debug(query);
      LOG.debug("fun_validararticulocelular Inicia");
      statement.executeQuery(query);

      rSet = statement.getResultSet();

      if (rSet != null && rSet.next()) {
        do {
          res = rSet.getInt("fun_validararticulocelular");
        } while (rSet.next());
      }

      LOG.debug("respuesta: " + res);
      LOG.debug("fun_validararticulocelular Termina");

    } catch (Exception e) {
      res = 0;
      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
        throw new General("Se agoto el tiempo de espera", "FAILED", "fun_validararticulocelular",
            e);
      else {
        throw e;
      }
    } finally {
      if (rSet != null) {
        rSet.close();
      }
      if (statement != null) {
        statement.close();
      }
    }
    return res;
  }
  
  public int fun_obtenercategoriaporsku(int skuIndice, int catIndice)
	      throws Exception {

	    Statement statement = null;
	    Connection conn = null;
	    ResultSet rSet = null;
	    int res = 0;

	    try {
	      conn = TxManager.getConnection(DataBaseEnum.TIENDA_800);
	      statement = conn.createStatement();

	      String query = "select icategoria from FUN_OBTENERCATEGORIAPORSKU(" + skuIndice + "::integer," + catIndice + "::smallint);";
	      LOG.debug(query);
	      statement.setQueryTimeout(20);
	      LOG.debug("fun_obtenercategoriaporsku Inicia");
	      statement.executeQuery(query);
	      LOG.debug("fun_obtenercategoriaporsku Termina");
	      

	      rSet = statement.getResultSet();

	      if (rSet != null && rSet.next()) {
	        do {
	          res = rSet.getInt("icategoria");
	        } while (rSet.next());
	      }

	    } catch (Exception e) {
	      if (e.getMessage().toUpperCase().contains(constantsBase.CANCEL_TIMEOUT_REQUEST))
	        throw new General("Se agoto el tiempo de espera", "FAILED",
	            "fun_obtenercategoriaporsku", e);
	      else {
	        throw e;
	      }
	    } finally {
	      if (statement != null) {
	        statement.close();
	      }
	    }
	    return res;
	  }




}
