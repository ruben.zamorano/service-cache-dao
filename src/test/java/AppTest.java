import helpers.BodyGrabarCache;
import helpers.CacheDTO;
import helpers.HelperCache;
import helpers.ResponseHelperCache;

import okhttp3.*;
import okhttp3.mock.Behavior;
import okhttp3.mock.MockInterceptor;
import org.junit.Before;
import org.junit.Test;


import static helpers.HelperCache.*;
import static okhttp3.mock.MediaTypes.MEDIATYPE_JSON;
import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

public class AppTest {

    private static String QUERY = "SELECT * FROM TABLA";
    private static String COLLECTION_NAME = "COLLECTION_TEST";

    private MockInterceptor interceptor;
    private OkHttpClient client;

    @Before
    public void setup() {
        client = new OkHttpClient.Builder()
                .addInterceptor(interceptor = new MockInterceptor(Behavior.UNORDERED))
                .build();
    }

    @Test
    public void testSetCacheService() {

        try {
            interceptor.addRule()
                    .post(CACHE_HOST +  "/grabar")
                    .respond("{succes:ok}");


            BodyGrabarCache bodyGrabarCache = new BodyGrabarCache();
            bodyGrabarCache.setKeyName(QUERY.hashCode() + "");
            bodyGrabarCache.setKeyValue("123");
            bodyGrabarCache.setCollection(COLLECTION_NAME);
            bodyGrabarCache.setTtl(CACHE_TTL);

            client.newCall(new Request.Builder()
                    .url(CACHE_HOST +  "/grabar")
                    .post(getRequestBody(bodyGrabarCache))
                    .build())
                    .execute();
            HelperCache.setCacheService(COLLECTION_NAME,QUERY, "123");

            assertTrue(true);
        }
        catch (Exception e)
        {

        }

    }

    @Test
    public void testGetCacheServiceOK() {

        final String url = CACHE_HOST  + "/" + COLLECTION_NAME + "/" + QUERY.hashCode();

        try {
            interceptor.addRule()
                    .get(url)
                    .respond("{\"data\":{\"keyName\":\"fun_obtenerdatosgndominio\",\"keyValue\":\"123\",\"collection\":\"Tienda800DAO\",\"ttl\":\"2021-07-1203:43:39.374\"}}");

            client.newCall(new Request.Builder()
                    .url(url)
                    .get()
                    .build())
                    .execute();



            String result = HelperCache.getCacheService(COLLECTION_NAME,QUERY);
            assertNull(result);
        }
        catch (Exception e)
        {

        }


    }

    @Test
    public void testGetCacheServiceNull() {
        assertNull( HelperCache.getCacheService(null,null));
    }


    @Test
    public void testGetBodyRequestCacheOK() {

        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .readTimeout(CACHE_TIME_OUT_MS_GET, TimeUnit.MILLISECONDS)
                .connectTimeout(CACHE_TIME_OUT_MS_GET, TimeUnit.MILLISECONDS)
                .build();
        Request request = new Request.Builder()
                .url(CACHE_HOST  + "/" + COLLECTION_NAME + "/" + QUERY.hashCode())
                .method("GET", null)
                .build();

        ResponseBody responseBody = HelperCache.getBodyRequestCache(client, request);
        assertNull(responseBody);
    }
    @Test
    public void testGetBodyRequestCacheNull() {
        assertNull( HelperCache.getBodyRequestCache(null, null));
    }

    @Test
    public void tesGetResponseCacheOK() {


        try
        {
            String key = QUERY.hashCode() + "";

            MockInterceptor interceptor = new MockInterceptor();
            interceptor.addRule()
                    .get(CACHE_HOST  + "/" + COLLECTION_NAME + "/" + key)
                    .respond("{succeed:true}", MEDIATYPE_JSON);



            OkHttpClient client = new OkHttpClient()
                    .newBuilder()
                    .addInterceptor(interceptor)
                    .readTimeout(CACHE_TIME_OUT_MS_GET, TimeUnit.MILLISECONDS)
                    .connectTimeout(CACHE_TIME_OUT_MS_GET, TimeUnit.MILLISECONDS)
                    .build();


            Request request = new Request.Builder()
                    .url(CACHE_HOST  + "/" + COLLECTION_NAME + "/" + key)
                    .method("GET", null)
                    .build();




            ResponseBody responseBodyCache = HelperCache.getBodyRequestCache(client, request);

            HelperCache.getResponseCache(responseBodyCache);

            assertNotNull(responseBodyCache);
        }
        catch (Exception e)
        {

        }

    }
    @Test
    public void testGetResponseCacheNull() {
        assertNull( HelperCache.getResponseCache(null));
    }




    @Test
    public void testDTO_Cache() {
        CacheDTO dto_cache = new CacheDTO(
                null);
        assertNotNull( dto_cache);
    }


    @Test
    public void testResponseHelperCache() {
        ResponseHelperCache responseHelperCache = new ResponseHelperCache(
                null);
        assertNotNull( responseHelperCache);
    }


    @Test
    public void testGetDataCacheResponseOK() {

        CacheDTO data = new CacheDTO("123");
        ResponseHelperCache responseCache = new ResponseHelperCache(data);
        String response = getDataCacheResponse(responseCache);
        assertNotNull( response);
    }

    @Test
    public void testGetDataCacheResponseError() {

        String response = getDataCacheResponse(null);
        assertNull( response);
    }



    @Test
    public void tesBodyGrabarCache() {

        BodyGrabarCache bodyGrabarCache = new BodyGrabarCache();
        bodyGrabarCache.setKeyName(QUERY.hashCode() + "");
        bodyGrabarCache.setKeyValue("123");
        bodyGrabarCache.setCollection(COLLECTION_NAME);
        bodyGrabarCache.setTtl(CACHE_TTL);
        assertNotNull( bodyGrabarCache);
    }

    @Test
    public void tesgetRequestBodyOK() {

        BodyGrabarCache bodyGrabarCache = new BodyGrabarCache();
        bodyGrabarCache.setKeyName(QUERY.hashCode() + "");
        bodyGrabarCache.setKeyValue("123");
        bodyGrabarCache.setCollection(COLLECTION_NAME);
        bodyGrabarCache.setTtl(CACHE_TTL);

        RequestBody requestBody = HelperCache.getRequestBody(bodyGrabarCache);
        assertNotNull( requestBody);
    }

    @Test
    public void tesgetRequestBodyError() {
        RequestBody requestBody = HelperCache.getRequestBody(null);
        assertNull( requestBody);
    }





















}
